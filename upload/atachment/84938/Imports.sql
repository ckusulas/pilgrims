/*
 Navicat PostgreSQL Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 90510
 Source Host           : localhost:5432
 Source Catalog        : pilgrim
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90510
 File Encoding         : 65001

 Date: 10/10/2017 15:13:44
*/


-- ----------------------------
-- Table structure for Imports
-- ----------------------------
DROP TABLE IF EXISTS "public"."Imports";
CREATE TABLE "public"."Imports" (
  "id" int4 NOT NULL DEFAULT nextval('import_id_seq'::regclass),
  "fk_user" varchar(255) COLLATE "pg_catalog"."default",
  "action" varchar(255) COLLATE "pg_catalog"."default",
  "updated" varchar(32) COLLATE "pg_catalog"."default" DEFAULT now()
)
;

-- ----------------------------
-- Records of Imports
-- ----------------------------
INSERT INTO "public"."Imports" VALUES (78, '3089', 'Importacion Cursos Manuales', '2018-01-22 14:14:59.866921-06');
INSERT INTO "public"."Imports" VALUES (79, '3089', 'Importacion Cursos Manuales', '2018-01-22 14:16:53.587477-06');
INSERT INTO "public"."Imports" VALUES (80, '3089', 'Importacion Cursos Manuales', '2018-01-22 14:33:39.026858-06');
INSERT INTO "public"."Imports" VALUES (81, '3089', 'Importacion Cursos Manuales', '2018-01-22 14:34:04.466929-06');
INSERT INTO "public"."Imports" VALUES (82, '3089', 'Importacion Cursos Manuales', '2018-01-22 14:34:25.293303-06');
INSERT INTO "public"."Imports" VALUES (83, '3089', 'Importacion Cursos Manuales', '2018-01-22 14:39:43.951979-06');
INSERT INTO "public"."Imports" VALUES (84, '3089', 'Importacion Cursos Manuales', '2018-01-22 14:57:56.949522-06');
INSERT INTO "public"."Imports" VALUES (85, '3089', 'Importacion Cursos Manuales', '2018-01-22 14:59:59.246521-06');
INSERT INTO "public"."Imports" VALUES (86, '3089', 'Importacion Cursos Manuales', '2018-01-22 15:05:19.26209-06');
INSERT INTO "public"."Imports" VALUES (87, '3089', 'Importacion Cursos Manuales', '2018-01-22 15:05:50.157867-06');
INSERT INTO "public"."Imports" VALUES (88, '3089', 'Importacion Cursos Manuales', '2018-01-22 15:06:37.82984-06');
INSERT INTO "public"."Imports" VALUES (89, '3089', 'Importacion Cursos Manuales', '2018-01-22 15:07:21.193604-06');
