/*
 Navicat PostgreSQL Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 90510
 Source Host           : localhost:5432
 Source Catalog        : pilgrim
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90510
 File Encoding         : 65001

 Date: 10/10/2017 18:21:47
*/


-- ----------------------------
-- Table structure for Atachments
-- ----------------------------
DROP TABLE IF EXISTS "public"."Imports";
CREATE TABLE "public"."Imports" (
  "id" int4 NOT NULL DEFAULT nextval('atachment_id_seq'::regclass),
  "fk_user" varchar(255) COLLATE "pg_catalog"."default", 
  "action" varchar(255) COLLATE "pg_catalog"."default", 
  "updated" varchar(32) COLLATE "pg_catalog"."default" DEFAULT now()
);

-- ----------------------------
-- Records of Atachments
-- ----------------------------
INSERT INTO "public"."Atachments" VALUES (25, 'RefactoringToPatterns.pdf', 'upload/atachment/84599/', 84599, '2017-11-25 10:07:05.5429-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (26, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/84599/', 84599, '2017-11-25 10:09:53.904249-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (27, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/84599/', 84599, '2017-11-25 10:20:46.013473-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (28, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84571/', 84571, '2017-11-25 10:40:27.202248-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (29, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84600/', 84600, '2017-11-25 10:50:42.960149-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (30, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84594/', 84594, '2017-11-25 10:56:41.162955-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (31, 'RefactoringToPatterns.pdf', 'upload/atachment/84594/', 84594, '2017-11-25 10:57:31.729656-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (32, 'libroRojofr.pdf', 'upload/atachment/84571/', 84571, '2017-11-25 10:58:05.35802-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (34, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84579/', 84579, '2017-11-25 10:59:45.854623-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (35, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/68970/', 68970, '2017-11-25 11:02:23.315956-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (36, 'libroRojofr.pdf', 'upload/atachment/68970/', 68970, '2017-11-25 11:02:53.933313-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (38, 'libroRojofr.pdf', 'upload/atachment/84572/', 84572, '2017-11-25 11:23:08.209181-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (39, 'RefactoringToPatterns.pdf', 'upload/atachment/84572/', 84572, '2017-11-25 11:23:50.269799-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (40, 'php.ini', 'upload/atachment/84572/', 84572, '2017-11-25 11:23:56.356446-06', 'ini', '<i class=''fa fa-file-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (41, 'libroRojofr.pdf', 'upload/atachment/84598/', 84598, '2017-11-25 22:55:36.89774-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (42, 'Formacion_curso.php', 'upload/atachment/84571/', 84571, '2017-11-27 11:51:30.051458-06', 'php', '<i class=''fa fa-file-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (44, 'libroRojofr.pdf', 'upload/atachment/84395/', 84395, '2017-11-27 17:17:55.68742-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (45, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84395/', 84395, '2017-11-27 17:18:51.564834-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (54, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/68973/', 68973, '2017-11-28 15:48:03.241643-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (66, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/68981/', 68981, '2017-11-28 16:50:32.153285-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (70, 'Addison.Wesley.Test.Driven.Development.By.Example.Nov.2002.ISBN.0321146530.chm', 'upload/atachment/68969/', 68969, '2017-11-28 17:00:59.883796-06', 'wesley', '<i class=''fa fa-file-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (71, 'postgresql.pdf', 'upload/atachment/68969/', 68969, '2017-11-28 17:01:54.749934-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (73, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/68971/', 68971, '2017-11-28 17:10:33.96564-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (76, 'Participants.sql.zip', 'upload/atachment/68980/', 68980, '2017-11-28 17:13:37.230742-06', 'sql', '<i class=''fa fa-file-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (77, 'Awicons-Vista-Artistic-Delete.ico', 'upload/atachment/68980/', 68980, '2017-11-28 17:13:55.148347-06', 'ico', '<i class=''fa fa-file-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (79, 'jxlrwtest.xls', 'upload/atachment/68980/', 68980, '2017-11-28 17:14:25.197688-06', 'xls', '<i class=''fa fa-table'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (80, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/68980/', 68980, '2017-11-28 17:15:25.278812-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (82, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/84578/', 84578, '2017-11-28 17:50:54.948324-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (83, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84578/', 84578, '2017-11-28 17:51:03.025641-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (86, 'file format.txt', 'upload/atachment/84579/', 84579, '2017-11-28 18:05:50.301937-06', 'txt', '<i class=''fa fa-file-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (87, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/68973/', 68973, '2017-11-29 13:27:12.481607-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (88, 'libroRojofr.pdf', 'upload/atachment/68973/', 68973, '2017-11-29 13:27:36.853977-06', 'pdf', '<i class=''fa fa-file-pdf-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (93, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/98765432/', 98765432, '2018-01-02 12:47:56.744923-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (94, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/98765432/', 98765432, '2018-01-02 12:48:10.319388-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (95, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/98765432/', 98765432, '2018-01-02 12:51:43.494709-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (97, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/98765431/', 98765431, '2018-01-02 13:28:37.180684-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (98, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/98765433/', 98765433, '2018-01-02 17:25:52.783504-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (99, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/98765433/', 98765433, '2018-01-02 17:28:31.092824-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (101, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/98765430/', 98765430, '2018-01-02 18:03:39.743695-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (102, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/98765430/', 98765430, '2018-01-02 18:05:21.511145-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (105, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/98765430/', 98765430, '2018-01-03 14:31:47.402596-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (106, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/83448144/', 83448144, '2018-01-04 12:29:53.388395-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (107, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/84048869/', 84048869, '2018-01-04 12:35:38.045104-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (108, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/89956876/', 89956876, '2018-01-04 12:44:46.975056-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (109, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/88701888/', 88701888, '2018-01-04 12:45:12.24029-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (110, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/86288627/', 86288627, '2018-01-04 12:46:19.302366-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (111, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/87054188/', 87054188, '2018-01-04 12:47:25.775933-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (112, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/89447448/', 89447448, '2018-01-04 12:49:51.689011-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (113, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/85196107/', 85196107, '2018-01-04 12:49:58.210553-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (114, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/87974678/', 87974678, '2018-01-04 12:54:35.416576-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (115, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/80491175/', 80491175, '2018-01-04 12:54:42.00731-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (116, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/89484525/', 89484525, '2018-01-04 12:55:09.772857-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (117, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/86310889/', 86310889, '2018-01-04 13:09:18.554788-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (118, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/86046701/', 86046701, '2018-01-04 13:52:48.749997-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (119, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/85902035/', 85902035, '2018-01-04 13:52:58.482141-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (120, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/89528291/', 89528291, '2018-01-04 13:53:59.892476-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (121, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/82843846/', 82843846, '2018-01-04 13:54:11.941726-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (122, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/86103710/', 86103710, '2018-01-04 13:54:53.031821-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (123, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/82127821/', 82127821, '2018-01-04 13:55:00.352644-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (124, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/87191495/', 87191495, '2018-01-04 13:55:03.53749-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (125, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/84904750/', 84904750, '2018-01-04 13:55:19.35006-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (126, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/87440969/', 87440969, '2018-01-04 14:03:27.957589-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (127, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/85583161/', 85583161, '2018-01-04 14:04:28.642693-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (128, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/84118513/', 84118513, '2018-01-04 14:04:36.213352-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (146, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/82121875/', 82121875, '2018-01-04 17:29:11.875516-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (130, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/3321/', 3321, '2018-01-04 14:07:02.493788-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (131, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/3321/', 3321, '2018-01-04 14:07:45.471402-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (132, 'Untitled.sql', 'upload/atachment/3321/', 3321, '2018-01-04 14:11:06.124042-06', 'sql', '<i class=''fa fa-file-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (133, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/80370964/', 80370964, '2018-01-04 14:48:46.350182-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (134, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/87446173/', 87446173, '2018-01-04 14:48:54.563036-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (135, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/84739509/', 84739509, '2018-01-04 14:48:55.910952-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (136, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/80057686/', 80057686, '2018-01-04 14:48:56.964675-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (137, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/89573559/', 89573559, '2018-01-04 14:48:57.222587-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (138, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/84160305/', 84160305, '2018-01-04 14:48:57.766351-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (140, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/80192850/', 80192850, '2018-01-04 15:10:26.605647-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (142, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/80192850/', 80192850, '2018-01-04 15:11:08.932349-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (152, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/86796860/', 86796860, '2018-01-04 18:07:53.917772-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (153, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/86796860/', 86796860, '2018-01-04 18:08:31.069195-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (155, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/83062645/', 83062645, '2018-01-04 18:12:06.438796-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (162, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/82534081/', 82534081, '2018-01-05 07:32:33.294458-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (163, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/82534081/', 82534081, '2018-01-05 11:23:09.087112-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (164, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/88789470/', 88789470, '2018-01-05 11:25:43.23252-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (165, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/80277473/', 80277473, '2018-01-05 11:25:48.598249-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (166, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/87744955/', 87744955, '2018-01-05 11:26:21.650451-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (167, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/80807188/', 80807188, '2018-01-05 11:26:27.078387-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (168, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/85783581/', 85783581, '2018-01-05 11:27:08.966429-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (169, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/89151733/', 89151733, '2018-01-05 11:27:21.122528-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (170, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/83133757/', 83133757, '2018-01-05 11:27:27.76189-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (171, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/89586851/', 89586851, '2018-01-05 11:27:47.968622-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (172, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/87255239/', 87255239, '2018-01-05 11:27:54.314493-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (173, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/87973559/', 87973559, '2018-01-05 11:30:12.971351-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (174, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/80133584/', 80133584, '2018-01-05 11:30:20.756371-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (175, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/88766924/', 88766924, '2018-01-05 11:49:44.4041-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (176, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/80348502/', 80348502, '2018-01-05 11:49:49.214868-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (177, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/87424246/', 87424246, '2018-01-05 11:54:28.13342-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (178, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/87897699/', 87897699, '2018-01-05 11:54:36.814633-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (179, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/87262837/', 87262837, '2018-01-05 12:15:03.462238-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (180, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/80119306/', 80119306, '2018-01-05 12:17:09.022787-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (181, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/80905236/', 80905236, '2018-01-05 12:17:44.160002-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (182, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/87093855/', 87093855, '2018-01-05 12:36:15.29128-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (183, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/87163811/', 87163811, '2018-01-05 12:36:19.59899-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (184, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/84535361/', 84535361, '2018-01-05 12:38:50.065449-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (185, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/87441257/', 87441257, '2018-01-05 12:40:24.907583-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (186, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/87441257/', 87441257, '2018-01-05 12:41:11.971006-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (188, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/89868559/', 89868559, '2018-01-05 12:43:33.763642-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (189, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/89868559/', 89868559, '2018-01-05 12:43:38.537419-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (190, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/89868559/', 89868559, '2018-01-05 12:47:44.911545-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (191, 'Untitled.sql', 'upload/atachment/88846598/', 88846598, '2018-01-05 13:08:06.660438-06', 'sql', '<i class=''fa fa-file-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (192, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/88846598/', 88846598, '2018-01-05 13:08:14.103827-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (193, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/88846598/', 88846598, '2018-01-05 13:08:19.283295-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (194, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/81203086/', 81203086, '2018-01-05 13:16:22.281917-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (195, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/81203086/', 81203086, '2018-01-05 13:16:29.381568-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (196, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/88842675/', 88842675, '2018-01-05 13:18:30.64065-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (197, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/88842675/', 88842675, '2018-01-05 13:18:36.814546-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (198, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/88842675/', 88842675, '2018-01-05 13:18:49.045399-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (199, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/86036744/', 86036744, '2018-01-05 13:37:03.13063-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (200, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/86036744/', 86036744, '2018-01-05 13:37:08.954532-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (201, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/86036744/', 86036744, '2018-01-05 13:37:13.992299-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (202, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/80648596/', 80648596, '2018-01-05 13:40:35.502907-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (203, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/85576264/', 85576264, '2018-01-05 13:54:13.247111-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (204, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/85576264/', 85576264, '2018-01-05 13:54:17.839063-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (205, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/88960839/', 88960839, '2018-01-05 13:55:12.224154-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (206, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/88960839/', 88960839, '2018-01-05 13:55:16.977454-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (207, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/81895074/', 81895074, '2018-01-05 13:56:31.595811-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (208, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/81895074/', 81895074, '2018-01-05 13:56:35.915649-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (209, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/81150276/', 81150276, '2018-01-05 14:05:42.587716-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (210, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/81150276/', 81150276, '2018-01-05 14:05:46.334701-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (212, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/82849831/', 82849831, '2018-01-05 14:06:50.22081-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (213, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/82849831/', 82849831, '2018-01-05 14:06:54.877398-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (214, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/84657/', 84657, '2018-01-05 14:08:36.737083-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (215, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/84657/', 84657, '2018-01-05 14:08:40.580895-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (216, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84657/', 84657, '2018-01-05 14:08:48.534273-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (217, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/82059652/', 82059652, '2018-01-05 15:01:46.39338-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (218, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/82059652/', 82059652, '2018-01-05 15:01:51.207208-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (219, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/84664/', 84664, '2018-01-05 15:04:23.693482-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (220, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/84666/', 84666, '2018-01-05 15:17:06.634096-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (221, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84666/', 84666, '2018-01-05 15:17:11.100033-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (222, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/84667/', 84667, '2018-01-05 15:21:58.181467-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (223, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/84667/', 84667, '2018-01-05 15:22:02.685157-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (224, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/84668/', 84668, '2018-01-05 15:23:36.569296-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (232, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/68982/', 68982, '2018-01-05 17:33:19.200228-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (233, 'Screenshot from 2017-11-24 14-26-50.png', 'upload/atachment/84395/', 84395, '2018-01-11 13:05:58.159802-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (226, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84671/', 84671, '2018-01-05 15:24:46.345401-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (227, 'Screenshot from 2017-11-03 14-26-33.png', 'upload/atachment/84671/', 84671, '2018-01-05 15:51:19.744972-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (234, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/68967/', 68967, '2018-01-11 13:35:22.694576-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (229, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84672/', 84672, '2018-01-05 16:09:05.893221-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (235, 'Screenshot from 2017-11-03 14-25-51.png', 'upload/atachment/84673/', 84673, '2018-01-11 13:35:45.762707-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (236, 'Screenshot from 2017-10-12 10-29-11.png', 'upload/atachment/84674/', 84674, '2018-01-12 18:42:58.259295-06', 'png', '<i class=''fa fa-file-image-o'' aria-hidden=''true''></i>');
INSERT INTO "public"."Atachments" VALUES (237, 'file format.txt', 'upload/atachment/84683/', 84683, '2018-01-16 11:58:11.492364-06', 'txt', '<i class=''fa fa-file-o'' aria-hidden=''true''></i>');

-- ----------------------------
-- Primary Key structure for table Atachments
-- ----------------------------
ALTER TABLE "public"."Atachments" ADD CONSTRAINT "Anexos_pkey" PRIMARY KEY ("id");
