<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

interface CrudModelInterface
{
    public function get($idElement);
    public function create($newElement);
    public function update($idElement,$title);
    public function delete($idElement);
    public function getlist($findElementId);

   

}