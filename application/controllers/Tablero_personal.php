<?php

/**
 * Controlador para mostrar Panel de Participante.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Tablero_personal extends CI_Controller {
 
    public function __construct() {
        parent::__construct();
        $this->load->model('tprincipal_model');
        $this->load->model('tpersonal_model');

        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');
 
        //if(!$this->session->userdata('is_logued_in')==TRUE || $_GET['srt'] != $token){
        if(!$this->session->userdata('is_logued_in')==TRUE ){
            redirect(base_url().'index.php');
        }


    }

    public function index() {


        $listWorksites = $this->tpersonal_model->getWorksite_EventView();


        $data = array(
            'catalogWorksites' => $listWorksites,          
        );

        if($this->session->userdata('profile')==3){
             $this->load->view('tpersonal_user', $data);

        }else{
            $this->load->view('tpersonal', $data);
        }
    }

    public function populate_locations($findWorksite) {


        $findWorksite = urldecode($findWorksite);
        $listLocations = $this->tpersonal_model->getLocation_FilterbyWorksite($findWorksite);


        echo json_encode($listLocations);
    }

    public function populate_participants($findLocation, $worsiteRef = "") {

        $findLocation = urldecode($findLocation);
        $worsiteRef = urldecode($worsiteRef);
        $listParticipants = $this->tpersonal_model->getParticipants_FilterbyLocation($findLocation, $worsiteRef);

        echo json_encode($listParticipants);
    }

    public function getHoursParticipant($idUser) {

        $qtyHours = $this->tpersonal_model->getHoursParticipant($idUser);
        $nameParticipant = $this->tpersonal_model->getNameParticipant($idUser);
        $listCourses = $this->tpersonal_model->getListCoursesByUser($idUser);



        $complete = 0;
        $progress = 0;
        foreach ($listCourses as $row) {
            if ($row['status'] == "Complete") {
                $complete++;
            } else {
                $progress++;
            }
        }

        $name = ucwords(strtolower($nameParticipant[0]['firstName']));
        $lastName = ucwords(strtolower($nameParticipant[0]['lastName']));
        $secondName = ucwords(strtolower($nameParticipant[0]['secondName']));




        $data = array(
            'nameParticipant' => $name . " " . $lastName . " " . $secondName,
            'IDparticipant' => $idUser,
            'qtyHours' => $qtyHours,
            'qtyCompleted' => $complete,
            'qtyProgress' => $progress,
            'listCourses' => $listCourses
        );


        echo json_encode($data);
    }

}
