<?php

/**
 * Crud para mostrar Admon de Instructores.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Admon_localidades extends CI_Controller {

    public $list;

    public function __construct() {
        parent::__construct();
        $this->load->model('admon_locations_model');

        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }
    }

    public function index() {



        $listLocations = $this->admon_locations_model->getlist(0);


        $data = array();



        foreach ($listLocations->result() as $locations) {

            //$complex->ID despues un ahref para editar.
            $data[] = array(
                'id' => $locations->id,
                'name' => $locations->name,
            );
        }


        $output = array(
            "dataTables" => $data,
            "subTitle" => "Localidades",
            "subElementSingular" => "Localidad",
            "name_controller" => "admon_localidades/",
        );

        $this->load->view('admon_localidades', $output);
    }

    public function ajax_deleteElement($idElement) {


        $result = $this->admon_locations_model->delete($idElement);


        $msg = "";
        $status = $result;
        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getElement($find_elem = "") {

        $elem_area = $this->admon_locations_model->get($find_elem);

        foreach ($elem_area->result() as $row) {
            $id = $row->id;
            $name_location = $row->name;
        }




        $data = array(
            'id' => $id,
            'name_location' => $name_location
        );

        echo json_encode($data);
    }

    public function saveElement() {


        $nameLocalidad = $this->input->post('nameLocalidadTXT');
        $idLocalidad = $this->input->post('idElement');





        $result = $this->admon_locations_model->update($idLocalidad, $nameLocalidad);
        if ($result) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function createElement() {


        $nameLocalidad = $this->input->post('nameLocalidadTXT');


        $result = $this->admon_locations_model->create($nameLocalidad);

        if ($result == "insertado exitosamente") {
            $status = true;
            $msg = '';
        }

        if ($result == "ya existia localidad") {
            $status = false;
            $msg = $result;
        }

        if ($result == "error insertado") {
            $status = false;
            $msg = "Error al insertar, favor consultar con Soporte.";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getList($find_elem = "") {

        $findCourse = urldecode($find_elem);
        $listComplex = $this->crud_course_model->getlist($findCourse);

        $data = array(
            'listComplex' => $listComplex
        );



        echo json_encode($data);
    }

}
