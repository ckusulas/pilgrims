<?php

/**
 * Importador para Cursos Manuales
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_import_course_manual extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('tprincipal_model');
        $this->load->model('admon_course_model');


        $this->load->library('form_validation');
        $this->load->helper('url');
        //$this->load->library('PHPExcel');
    }

    public function index() {

        $this->load->view('excel_manual_course_import');
    }

    public function import_data() {
        $config = array(
            'upload_path' => FCPATH . 'upload/',
            'allowed_types' => 'xls'
        );



        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file')) {
            $data = $this->upload->data();
            @chmod($data['full_path'], 0777);



            $this->load->library('Spreadsheet_Excel_Reader');
            $this->spreadsheet_excel_reader->setOutputEncoding('CP1251');


            $this->spreadsheet_excel_reader->read($data['full_path']);


            //Procesando Sheet: "Sheet1"
            $this->processCourseManualImport($this->spreadsheet_excel_reader->sheets[0]);






            //redirect('excel-import-sap');
        }
    }

    public function processCourseManualImport($dataInput) {


        /*
          1.Nombre del curso
          2.Complejo
          3.Area
          4.Subarea
          5.Localidad
          6.Tipo
          7.Categoria
          8.Modalidad
          9.Fecha Inicio
          10.Fecha Fin
          11.Duracion Min
          12.Nombre del grupo
          13.Nombre del Instructor
          14.Ap.Paterno del Instructor
          15.Ap.Materno del Instructor

         */

        $sheets = $dataInput;

        //error_reporting(0);
        error_reporting(E_ALL);




        $data_excel = array();
        $cantidadUpdates = 0;
        $cantidadInsert = 0;
        $totalCounter = 0;


        //Proximamente:
        //  Primera columna debemos poner i=1, para validar las columnas

        $heading = "Nombre del Curso|Complejo|Area|Subarea|Localidad|Tipo|Categoria|Modalidad|Fecha Inicio|Fecha Fin|Duracion Min|Nombre del grupo|Nombre del Instructor|Ap.Paterno del Instructor|Ap.Materno del Instructor";




        $dataResult = $this->validanteContent($sheets);

        // echo "respuesta:" . print_r($dataResult);


        $msgError = $dataResult['msgError'];

        if ($msgError == "") {


            //Registrando importacion
            $this->db->set('action', "Importacion Cursos Manuales");
            $this->db->set('fk_user', $this->session->userdata('id'));
            $this->db->insert('Imports');

            $fk_imports = $this->db->insert_id();





            $noParticipants = $listParticipantsForm = $instructor = "";


            foreach ($dataResult as $data) {



                //Si tenemos datos del Curso, y no de msgError entonces procesamos...
                if (count($data) > 1) {

                    //Course
                    $this->db->like('courseCode', 'others_');
                    $this->db->order_by('courseCode', 'DESC');
                    $this->db->limit(1);
                    $query = $this->db->get('Courses');
                    $ret = $query->row();
                    $last_courseCode = $ret->courseCode;


                    $aElem = explode("_", $last_courseCode);
                    $elemento = intval($aElem[1]);
                    $elemento++;
                    $subNumeric = str_pad($elemento, 5, "0", STR_PAD_LEFT);
                    $codeCourse = "others_" . $subNumeric;

                    //Preparando nuevo terreno
                    //Insertando nuevo curso
                    $this->db->set('fk_import', $fk_imports);
                    $this->db->set('source', "Otros");
                    $this->db->set('courseCode', $codeCourse);
                    $this->db->insert('Courses');

                    //Insertando nuevo trainingRecord
                    $this->db->set('fk_course', $codeCourse);
                    $this->db->insert('TrainingRecords');

                    $idRelationCourse = $this->db->insert_id();



                    //TrainingRecords  

                    $Type = $data['Type'];
                    $Modality = $data['Modality'];
                    $nameCourse = $data['nameCourse'];
                    $fk_complex = $data['fk_complex'];
                    $fk_area = $data['fk_area'];
                    $fk_location = $data['fk_location'];
                    $fk_category = $data['fk_category'];
                    $Duration = $data['Duration'];
                    $dateStart = $data['dateStart'];
                    $dateEnd = $data['dateEnd'];
                    $teamName = $data['teamName'];
                    $name_Instructor = $data['name_Instructor'];
                    $lastname_Instructor = $data['lastname_Instructor'];
                    $secondName_Instructor = $data['secondName_Instructor'];

                    //insert Course 
                    $this->updateTraininCourse($idRelationCourse, $codeCourse, $Type, $Modality, $nameCourse, $fk_complex, $fk_area, $fk_location, $fk_category, $noParticipants, $listParticipantsForm, $Duration, $instructor, $dateStart, $dateEnd, $teamName, $name_Instructor, $lastname_Instructor, $secondName_Instructor);
                }
            }
        } else {
            echo "No se ha validado el contenido, debe corregir los siguientes campos para poder cargar los cursos: <br>";
            echo $msgError;
        }





        echo "<br><a href='javascript:history.back()'>Regresar.</a>";
    }

    public function validanteContent($sheets) {

        $msgError = "";
        $dataResult = array();

        echo "<h2>Validando campos en cada registro... </h2><br>";


        for ($i = 2; $i <= $sheets['numRows']; $i++) {

            $nameCourse = $Complex = $Area = $Location = $Type = "";
            $Category = $Modality = $dateStart = $dateEnd = $Duration = $teamName = "";



            if (isset($sheets['cells'][$i][1])) {
                $nameCourse = utf8_encode($sheets['cells'][$i][1]);
                echo "<b>Nombre del curso: " . $nameCourse . "</b><br>";
            }



            if (isset($sheets['cells'][$i][2]))
                $Complex = utf8_encode($sheets['cells'][$i][2]);

            echo "Complejo: " . $Complex . "<br>";



            if (isset($sheets['cells'][$i][3]))
                $Area = utf8_encode($sheets['cells'][$i][3]);

            echo "Area:" . $Area . "<br>";

            if (isset($sheets['cells'][$i][4])) {
                $Subarea = utf8_encode($sheets['cells'][$i][4]);
                echo "Subarea: " . $Subarea . "<br>";
            }



            if (isset($sheets['cells'][$i][5]))
                $Location = utf8_encode($sheets['cells'][$i][5]);
            echo "Localidad: " . $Location . "<br>";


            if (isset($sheets['cells'][$i][6]))
                $Type = utf8_encode($sheets['cells'][$i][6]);
            echo "Tipo: " . $Type . "<br>";


            if (isset($sheets['cells'][$i][7]))
                $Category = utf8_encode($sheets['cells'][$i][7]);
            echo "categoria: " . $Category . "<br>";


            if (isset($sheets['cells'][$i][8]))
                $Modality = utf8_encode($sheets['cells'][$i][8]);
            echo "Modalidad: " . $Modality . "<br>";

            if (isset($sheets['cells'][$i][9]))
                $dateStart = utf8_encode($sheets['cells'][$i][9]);
            echo "Fecha inicio: " . $dateStart . "<br>";

            if (isset($sheets['cells'][$i][10]))
                $dateEnd = utf8_encode($sheets['cells'][$i][10]);
            echo "Fecha final: " . $dateEnd . "<br>";

            if (isset($sheets['cells'][$i][11]))
                $Duration = intval(utf8_encode($sheets['cells'][$i][11]));
            echo "Duracion: " . $Duration . "<br>";

            if (isset($sheets['cells'][$i][12]))
                $teamName = intval(utf8_encode($sheets['cells'][$i][12]));
            echo "Nombre Equipo: " . $teamName . "<br>";


            if (isset($sheets['cells'][$i][13]))
                $name_Instructor = utf8_encode($sheets['cells'][$i][13]);
            echo "Nombre Instructor: " . $name_Instructor . "<br>";

            if (isset($sheets['cells'][$i][14]))
                $lastname_Instructor = utf8_encode($sheets['cells'][$i][14]);
            echo "Ap.Paterno Instructor: " . $lastname_Instructor . "<br>";

            if (isset($sheets['cells'][$i][15]))
                $secondName_Instructor = utf8_encode($sheets['cells'][$i][15]);
            echo "Ap.Materno Instructor: " . $secondName_Instructor . "<br>";


            echo "<br><br><br>";



            //Validacion de catalogos


            if ($nameCourse == "") {
                $msgError .= "<br>Favor revise la columna <b>Nombre Del Curso</b> del registro <b>no.{$i}</b>";
            }


            //Complejos       

            $listComplexes = $this->tprincipal_model->getCatalog_Complexes();

            $bandComplex = 0;

            $fk_complex = "";

            foreach ($listComplexes as $row) {

                if (strtoupper($row['name']) == strtoupper($Complex)) {

                    $fk_complex = $row['id'];
                    $bandComplex = 1;
                }
            }
            if (0 == $bandComplex) {
                $msgError .= "<br>Favor revise la columna <b>Complejo</b> del registro <b>no.{$i}</b>";
            }




            //Areas

            $listProcesses = $this->tprincipal_model->getCatalog_Processes();

            $bandArea = 0;

            foreach ($listProcesses as $row) {
                //echo $row['name'] . "<br>";
                if (strtoupper($row['name']) == strtoupper($Area)) {

                    $fk_area = $row['id'];
                    $bandArea = 1;
                }
            }
            if (0 == $bandArea) {
                $msgError .= "<br>Favor revise la columna <b>Area</b> del registro <b>no.{$i}</b>";
            }



            //Localidad

            $listLocations = $this->tprincipal_model->getCatalog_Locations();

            $bandLocation = 0;

            foreach ($listLocations as $row) {
                //echo $row['name'] . "<br>";
                if (strtoupper($row['name']) == strtoupper($Location)) {

                    $fk_location = $row['id'];
                    $bandLocation = 1;
                }
            }
            if (0 == $bandLocation) {
                $msgError .= "<br>Favor revise la columna <b>Localidad</b> del registro <b>no.{$i}</b>";
            }



            //Tipo

            $listType = array("interno", "externo");

            $bandType = 0;

            foreach ($listType as $row) {
                //echo $row['name'] . "<br>";
                if (strtolower($row) == strtolower($Type)) {

                    $Type = $row;
                    $bandType = 1;
                }
            }
            if (0 == $bandType) {
                $msgError .= "<br>Favor revise la columna <b>Tipo</b> del registro <b>no.{$i}</b>";
            }




            //Categoria

            $listCategory = $this->admon_course_model->getCatalog_Category("1,2,3,4,5");

            $bandCategory = 0;

            foreach ($listCategory as $row) {
                //echo $row['name'] . "<br>";
                if (strtoupper($row['name']) == strtoupper($Category)) {

                    $fk_category = $row['id'];
                    $bandCategory = 1;
                }
            }

            if (0 == $bandArea) {
                $msgError .= "<br>Favor revise la columna <b>Categoria</b> del registro <b>no.{$i}</b>";
            }




            //Modalidad

            $listModality = array("Presencial", "Virtual", "Mixto");

            $bandModality = 0;

            foreach ($listModality as $row) {
                //echo $row['name'] . "<br>";
                if (strtolower($row) == strtolower($Modality)) {

                    $Modality = $row;
                    $bandModality = 1;
                }
            }
            if (0 == $bandModality) {
                $msgError .= "<br>Favor revise la columna <b>Modalidad</b> del registro <b>no.{$i}</b>";
            }






            //Fecha Inicio

            $bandDateStart = 0;

            $a_dateStart = explode("/", $dateStart);
            if (count($a_dateStart) == 3) {
                if (checkdate($a_dateStart[1], $a_dateStart[0], $a_dateStart[2])) {
                    
                } else {

                    $bandDateStart = 1;
                }
            } else {
                $bandDateStart = 1;
            }

            if (1 == $bandDateStart) {
                $msgError .= "<br>Favor revise el formato de fecha de <b>Fecha Inicial</b> del registro <b>no.{$i}</b>, Fecha Invalida: Debe ser  'dd/mm/YYYY', y poner formato 'texto' a la columna de fecha.";
            }





            //Fecha Fin

            $bandDateEnd = 0;

            $a_dateEnd = explode("/", $dateEnd);
            if (count($a_dateEnd) == 3) {
                if (checkdate($a_dateEnd[1], $a_dateEnd[0], $a_dateEnd[2])) {
                    
                } else {

                    $bandDateEnd = 1;
                }
            } else {
                $bandDateEnd = 1;
            }

            if (1 == $bandDateEnd) {
                $msgError .= "<br>Favor revise el formato de fecha de <b>Fecha Final</b> del registro <b>no.{$i}</b>, Fecha Invalida: Debe ser  'dd/mm/YYYY', y poner formato 'texto' a la columna de fecha.";
            }



            $noParticipants = $listParticipantsForm = $instructor = "";



            $dataResult[] = array(
                "Type" => $Type,
                "Modality" => $Modality,
                "nameCourse" => $nameCourse,
                "fk_complex" => $fk_complex,
                "fk_area" => $fk_area,
                'fk_location' => $fk_location,
                'fk_category' => $fk_category,
                'noParticipants' => $noParticipants,
                'listParticipantsForm' => $listParticipantsForm,
                'Duration' => $Duration,
                'instructor' => $instructor,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
                'teamName' => $teamName,
                'name_Instructor' => $name_Instructor,
                'lastname_Instructor' => $lastname_Instructor,
                'secondName_Instructor' => $secondName_Instructor,
            );
        }

        $dataResult["msgError"] = $msgError;

        return($dataResult);
    }

    public function updateTraininCourse($idRelationCourse, $codeCourse, $typeSelect, $modalitySelect, $courseName, $complexSelect, $areaSelect, $locationSelect, $categorySelect, $noParticipants = "", $listParticipants = "", $duration = "", $instructor = "", $dateStart, $dateEnd, $teamName = "", $name_Instructor = "", $lastname_Instructor = "", $secondName_Instructor = "") {

        //Updating CourseName
        $this->db->set('courseName', $courseName);
        $this->db->set('type', strtolower($typeSelect));
        $this->db->set('duration', $duration);
        $this->db->where('courseCode', $codeCourse);
        $this->db->update('Courses');

        $name_Instructor = ucwords(strtolower(trim($name_Instructor)));
        $lastname_Instructor = ucwords(strtolower(trim($lastname_Instructor)));
        $secondName_Instructor = ucwords(strtolower(trim($secondName_Instructor)));





        $this->db->select('id');
        $this->db->where('firstName', $name_Instructor);
        $this->db->where('lastName', $lastname_Instructor);
        $this->db->where('secondName', $secondName_Instructor);

        $fk_instructor = "";

        $query = $this->db->get('Instructors');

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                $fk_instructor = $row->id;
            }

            echo "ya estaba insertado el instructor <br>";
        } else {
            $this->db->set('firstName', $name_Instructor);
            $this->db->set('lastName', $lastname_Instructor);
            $this->db->set('secondName', $secondName_Instructor);
            $this->db->insert('Instructors');

            $fk_instructor = $this->db->insert_id();
            echo "se inserta nuevo instructor <br>";
        }


        echo "fk_instructor:" . $fk_instructor . "<br>";



        $this->db->set('fk_instructor', $fk_instructor);
        $this->db->set('fk_complex', $complexSelect);
        $this->db->set('fk_area', $areaSelect);
        $this->db->set('fk_location', $locationSelect);
        $this->db->set('fk_category', $categorySelect);
        //$this->db->set('fk_instructor', $instructor);
        $this->db->set('teamName', $teamName);

        $formatDataStart = "";
        if ($dateStart != "") {
            $aDataStart = explode("/", $dateStart);
            $formatDataStart = $aDataStart[2] . "-" . $aDataStart[1] . "-" . $aDataStart[0];
        }

        $this->db->set('trainingStart', $formatDataStart);



        $formatDataEnd = "";
        if ($dateEnd != "") {
            $aDataEnd = explode("/", $dateEnd);
            $formatDataEnd = $aDataEnd[2] . "-" . $aDataEnd[1] . "-" . $aDataEnd[0];
        }

        $this->db->set('trainingEnd', $formatDataEnd);


        $this->db->set('delivery', strtolower($modalitySelect));


        $this->db->set('listAsistants', $listParticipants);
        $this->db->set('quantityParticipants', intval($noParticipants));
        $this->db->where('id', $idRelationCourse);
        $this->db->update('TrainingRecords');



        if ($this->db->affected_rows() > 0) {
            echo "<h2>Se inserto correctamente, el curso:<b>" . $courseName . "</b></h2><br>";
        } else {
            echo "<h2><font color='red'>Hubo error al insertar, el curso:<b>" . $courseName . "</b></font>>/h2><br>";
        }
    }

}