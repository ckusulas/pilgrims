<?php

/**
 * Crud para mostrar Admon de Worksites.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Admon_worksites extends CI_Controller {

    public $list;

    public function __construct() {
        parent::__construct();
        $this->load->model('admon_worksites_model');


        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }
    }

    public function index() {



        $listworksites = $this->admon_worksites_model->getlist(0);


        $data = array();



        foreach ($listworksites->result() as $worksites) {

            //$complex->ID despues un ahref para editar.
            $data[] = array(
                'id' => $worksites->id,
                'name' => $worksites->name,
            );
        }


        $output = array(
            "dataTables" => $data,
            "subTitle" => "Worksites",
            "subElementSingular" => "Worksite",
            "name_controller" => "admon_worksites/",
        );

        $this->load->view('admon_worksites', $output);
    }

    public function ajax_deleteElement($idElement) {


        $result = $this->admon_worksites_model->delete($idElement);


        $msg = "";
        $status = $result;
        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getElement($find_elem = "") {

        $this->load->model('tprincipal_model');

        $elem_area = $this->admon_worksites_model->get($find_elem);

        $listComplexes = $this->tprincipal_model->getCatalog_Complexes();
        $listProcesses = $this->tprincipal_model->getCatalog_Processes();


        foreach ($elem_area->result() as $row) {
            $id = $row->id;
            $name_worksite = $row->name;
            $rel_complex = $row->fk_complex;
            $rel_area = $row->fk_area;
        }




        $data = array(
            'id' => $id,
            'name_worksite' => $name_worksite,
            'fk_area' => $rel_area,
            'fk_complex' => $rel_complex,
            'catalogComplexes' => $listComplexes,
            'catalogAreas' => $listProcesses,
        );

        echo json_encode($data);
    }

    public function saveElement() {


        $nameWorksite = $this->input->post('nameWorksiteTXT');
        $idWorksite = $this->input->post('idElement');
        $fk_complex = $this->input->post('complexSelectTMP');
        $fk_area = $this->input->post('selectArea');


        $data = array(
            'nameWorksite' => $nameWorksite,
            'fk_complex' => $fk_complex,
            'fk_area' => $fk_area
        );



        $result = $this->admon_worksites_model->update($idWorksite, $data);
        if ($result) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function createElement() {


        $nameWorksite = $this->input->post('nameWorksiteTXT');


        $result = $this->admon_worksites_model->create($nameWorksite);

        if ($result == "insertado exitosamente") {
            $status = true;
            $msg = '';
        }

        if ($result == "ya existia worksite") {
            $status = false;
            $msg = $result;
        }

        if ($result == "error insertado") {
            $status = false;
            $msg = "Error al insertar, favor consultar con Soporte.";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getList($find_elem = "") {

        $findCourse = urldecode($find_elem);
        $listComplex = $this->crud_course_model->getlist($findCourse);

        $data = array(
            'listComplex' => $listComplex
        );



        echo json_encode($data);
    }

}
