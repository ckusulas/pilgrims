<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Author : Constantino Kusulas V.
 */

class Import extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('PHPExcel');
    }

    function index() {
        $msg = $this->uri->segment(3);
        $alert = '';
        if ($msg == 'success') {
            $alert = 'Success!!';
        }
        $data['_alert'] = $alert;
        $this->load->view('import-index', $data);
    }

    function upload() {
        $fileName = time() . $_FILES['fileImport']['name'];
        $config['upload_path'] = './fileExcel/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
        $this->load->library('upload');
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('fileImport'))
            $this->upload->display_errors();
        $media = $this->upload->data('fileImport');
        $inputFileName = './fileExcel/' . $media['file_name'];
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        for ($row = 2; $row <= $highestRow; $row++) {                           // Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            $data = array(
                "nama" => $rowData[0][1],
                "alamat" => $rowData[0][2],
                "kontak" => $rowData[0][3]
            );

            $insert = $this->db->insert("data_orang", $data);
            delete_files($media['file_path']);
        }

        redirect(base_url('import/index/success'));
    }

}
