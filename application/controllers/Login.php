<?php

/**
 * Login
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('login_model');
        $this->load->helper('security');
    }

    public function index() {


        $login = $this->input->post('login');
        $password = $this->input->post('password');


        $data = array(
            'username' => xss_clean($login),
            'password' => xss_clean($password),
        );

        $check_user = $this->login_model->login_success($data);

        if ($check_user == TRUE) {

            $token = md5(uniqid(rand(), true));
            $dataName = explode(" ", $check_user->firstName);
            $lastName = $check_user->lastName;
            if (strlen($lastName) > 11) {
                $dataLastname = explode(" ", $check_user->lastName);
                $lastName = $dataLastname[0];
            }

            $name = ucwords(strtolower($dataName[0])) . " " . ucwords(strtolower($lastName));


            $path_origin = "/tablero_principal";
            if (3 == $check_user->fk_profiles) {
                $path_origin = "/tablero_personal";
            }


            $data = array(
                'is_logued_in' => TRUE,
                'id' => $check_user->id,
                'matricula' => $check_user->idParticipant,
                'worksite' => $check_user->worksite,
                'profile' => $check_user->fk_profiles,
                'fullname' => strtoupper($check_user->firstName) . ' ' . strtoupper($check_user->lastName),
                'name' => $name,
                'token' => session_id(),
                'url_origin' => site_url() . $path_origin . '?srt=' . session_id()
            );


            $this->session->set_userdata($data);



            if (3 == $check_user->fk_profiles) {
                redirect(site_url() . $path_origin . '?srt=' . $this->session->userdata('token'));
            } else {
                redirect(site_url() . $path_origin . '?srt=' . $this->session->userdata('token'));
            }
        } else {
            redirect(base_url() . 'index.php');
        }
    }

    public function token() {
        $token = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $token);
        return $token;
    }

    public function logout() {
        $this->session->sess_destroy();
        $data = array(
            'status' => TRUE,
        );
        echo json_encode($data);
    }

}
