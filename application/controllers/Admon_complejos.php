<?php

/**
 * Controlador para mostrar Admon de Complejos.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Admon_complejos extends CI_Controller {

    public $list;

    public function __construct() {
        parent::__construct();
        $this->load->model('admon_complexes_model');

        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }



        //Category idioma
        $this->list = "5";
    }

    public function index() {


        //'listParticipantes' => $listParticipantesPlane,
        $listComplex = $this->admon_complexes_model->getlist(0);


        $data = array();



        foreach ($listComplex->result() as $complex) {

            //$complex->ID despues un ahref para editar.
            $data[] = array(
                'id' => $complex->id,
                'name' => $complex->name,
            );
        }


        $output = array(
            "dataTables" => $data,
        );

        $this->load->view('admon_complejos', $output);
    }

    public function ajax_deleteElement($idElement) {


        $result = $this->admon_complexes_model->delete($idElement);


        $msg = "";
        $status = $result;
        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getElement($find_elem = "") {

        $elem_complex = $this->admon_complexes_model->get($find_elem);

        foreach ($elem_complex->result() as $row) {
            $id = $row->id;
            $name_complex = $row->name;
        }




        $data = array(
            'id' => $id,
            'name_complex' => $name_complex
        );

        echo json_encode($data);
    }

    public function saveElement() {


        $nameComplex = $this->input->post('nameComplexTXT');
        $idComplex = $this->input->post('idElement');





        $result = $this->admon_complexes_model->update($idComplex, $nameComplex);
        if ($result) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function createElement() {


        $nameComplex = $this->input->post('nameComplexTXT');


        $result = $this->admon_complexes_model->create($nameComplex);

        if ($result == "insertado exitosamente") {
            $status = true;
            $msg = '';
        }

        if ($result == "ya existia complejo") {
            $status = false;
            $msg = $result;
        }

        if ($result == "error insertado") {
            $status = false;
            $msg = "Error al insertar, favor consultar con Soporte.";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getList($find_elem = "") {

        $findCourse = urldecode($find_elem);
        $listComplex = $this->crud_course_model->getlist($findCourse);

        $data = array(
            'listComplex' => $listComplex
        );



        echo json_encode($data);
    }

}
