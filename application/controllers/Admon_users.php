<?php

/**
 * Crud para mostrar Admon de Usuarios.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Admon_users extends CI_Controller {

    public $list;

    public function __construct() {
        parent::__construct();
        $this->load->model('admon_users_model');

        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }
    }

    public function index() {


        $output = array(
            "subTitle" => "Localidades",
            "subElementSingular" => "Localidad",
            "name_controller" => "admon_localidades/",
        );

        $this->load->view('admon_users', $output);
    }

    function datatables_ajax() {
        /** AJAX Handle */
        if (
                isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
                !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {



            $datatables = $_POST;
            //echo "content:" . print_r($_POST);
            $datatables['table'] = 'Participants';
            $datatables['id-table'] = 'id';
            /**
             * Kolom yang ditampilkan
             */
            $datatables['col-display'] = array(
                'idParticipant',
                'firstName',
                'lastName',
                'worksite',
                'nameLocation',
                'type',
                'id'
            );
            /**
             * menggunakan table join
             */
            $datatables['join'] = ' LEFT JOIN "Profiles" ON ("fk_profiles" = "Profiles"."idProfiles") ';
            //select * from "Participants" LEFT JOIN "Profiles" ON("fk_profiles" = "Profiles"."id")
            $this->admon_users_model->Datatables($datatables);
        }
        return;
    }

    public function ajax_deleteElement($idElement) {


        $result = $this->admon_locations_model->delete($idElement);


        $msg = "";
        $status = $result;
        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getElement($find_user = "") {

        $elem_participant = $this->admon_users_model->get($find_user);

        foreach ($elem_participant->result() as $row) {
            $idUser = $row->id;
            $matricula = $row->idParticipant;
            $name = $row->firstName;
            $lastname = $row->lastName;
            $password = $row->password;
            $profile = $row->fk_profiles;
            $worksite = $row->worksite;
        }

        $listProfiles = $this->admon_users_model->getCatalog_Profiles();

        //print_r($listProfiles);

        if ($password == "") {
            $password = $this->random_password(10);
        }


        $data = array(
            'idUser' => $idUser,
            'matricula' => $matricula,
            'name' => $name . " " . $lastname,
            'password' => $password,
            'worksite' => $worksite,
            'profile' => $profile,
            'catalogProfiles' => $listProfiles,
        );

        echo json_encode($data);
    }

    public function saveElement() {

        $type_profile = $this->input->post('profiSelect');
        $fk_user = $this->input->post('idUser');
       
        $worksiteList = $this->input->post('worksiteLists');
        $password = $this->input->post('passwordTXT');

       /*  echo "fk_user:" . $fk_user . "<br>";
        echo "worksiteList:" . $worksiteList;*/


        if (2 == $type_profile) {


            $this->db->where('fk_participant', $fk_user);
            $this->db->delete('ProfilesGestorWorksites');

            $arrayListWorksite = explode(",", $worksiteList);

            foreach ($arrayListWorksite as $elem) {

                $this->db->set('fk_worksite',trim($elem));
                $this->db->set('fk_participant',trim($fk_user));

                $this->db->insert('ProfilesGestorWorksites');

                //echo $this->db->last_query() . "<br>";
            }
        }


        if ($this->db->affected_rows() > 0) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $this->db->set('password', $password);
        $this->db->where('id', $fk_user);

        $this->db->update('Participants');







        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function createElement() {


        $nameLocalidad = $this->input->post('nameLocalidadTXT');


        $result = $this->admon_locations_model->create($nameLocalidad);

        if ($result == "insertado exitosamente") {
            $status = true;
            $msg = '';
        }

        if ($result == "ya existia localidad") {
            $status = false;
            $msg = $result;
        }

        if ($result == "error insertado") {
            $status = false;
            $msg = "Error al insertar, favor consultar con Soporte.";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getList($find_elem = "") {

        $findCourse = urldecode($find_elem);
        $listUsers = $this->admon_users_model->getlist($findCourse);

        $data = array(
            'listUsers' => $listUsers
        );



        echo json_encode($data);
    }

    function random_password($length = 8) {
        $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    function setPasswordParticipant($matricula = "") {
        if ($matricula == "") {

            $query = $this->db->query('select  "idParticipant" from "Participants"');
            $result = $query->result_array();

            foreach ($result as $row) {


                $password = $this->random_password(10);
                $matricula = $row['idParticipant'];

                $this->db->set('password', $password);
                $this->db->where('idParticipant', $matricula);
                $this->db->update("Participants");

                echo "matricula:" . $matricula . ", password:" . $password . "<br>";
            }
        }
    }

    public function getListWorksites_specific($idUser) {


        $listWorksiteGestor = $this->admon_users_model->getListWorksitesGestorSpecific($idUser);


        $listWorksites = array();

        foreach ($listWorksiteGestor as $row) {

            $participant = array(
                "id" => $row['id'],
                "name" => $row['name']
            );

            $listWorksites[] = $participant;
        }




        echo json_encode($listWorksites);
    }

    public function getListWorksites() {


        $listWorksiteGestor = $this->admon_users_model->getListWorksites();


        $listWorksites = array();

        foreach ($listWorksiteGestor as $row) {

            $participant = array(
                "id" => $row['id'],
                "name" => $row['name']
            );

            $listWorksites[] = $participant;
        }




        echo json_encode($listWorksites);
    }

}
