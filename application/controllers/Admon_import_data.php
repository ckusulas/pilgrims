<?php

/**
 * Controlador para Import Data .xls
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Admon_import_data extends CI_Controller {

    public $list;

    public function __construct() {
        parent::__construct();
        $this->load->model('admon_instructors_model');

        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }
    }

    public function index() {

        $listinstructors = $this->admon_instructors_model->getlist(0);


        $data = array();



        foreach ($listinstructors->result() as $instructor) {

            //$complex->ID despues un ahref para editar.
            $data[] = array(
                'id' => $instructor->id,
                'firstName' => $instructor->firstName,
                'lastNames' => $instructor->lastName . " " . $instructor->secondName,
            );
        }


        $output = array(
            "dataTables" => $data,
            "subTitle" => "Instructores",
            "subElementSingular" => "Instructor",
            "name_controller" => "admon_instructors/",
        );

        $this->load->view('admon_import_data', $output);
    }

    public function ajax_deleteElement($idElement) {


        $result = $this->admon_instructors_model->delete($idElement);


        $msg = "";
        $status = $result;
        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getElement($find_elem = "") {

        $elem_area = $this->admon_instructors_model->get($find_elem);

        foreach ($elem_area->result() as $row) {
            $id = $row->id;
            $firstName = $row->firstName;
            $lastName = $row->lastName;
            $secondName = $row->secondName;
        }




        $data = array(
            'id' => $id,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'secondName' => $secondName
        );

        echo json_encode($data);
    }

    public function saveElement() {


        $firstName = $this->input->post('firstNameTXT');
        $lastName = $this->input->post('lastNameTXT');
        $secondName = $this->input->post('secondNameTXT');
        $idInstructor = $this->input->post('idElement');

        $data = array(
            'firstName' => $firstName,
            'lastName' => $lastName,
            'secondName' => $secondName
        );


        $result = $this->admon_instructors_model->update($idInstructor, $data);
        if ($result) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function createElement() {


        $firstName = $this->input->post('firstNameTXT');
        $lastName = $this->input->post('lastNameTXT');
        $secondName = $this->input->post('secondNameTXT');

        $data = array(
            'firstName' => $firstName,
            'lastName' => $lastName,
            'secondName' => $secondName
        );


        $result = $this->admon_instructors_model->create($data);

        if ($result == "insertado exitosamente") {
            $status = true;
            $msg = '';
        }

        if ($result == "ya existia instructor") {
            $status = false;
            $msg = $result;
        }

        if ($result == "error insertado") {
            $status = false;
            $msg = "Error al insertar, favor consultar con Soporte.";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getList($find_elem = "") {

        $findCourse = urldecode($find_elem);
        $listComplex = $this->admon_course_model->getlist($findCourse);

        $data = array(
            'listComplex' => $listComplex
        );



        echo json_encode($data);
    }

}
