<?php

/**
 * Importador para Alchemy
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_import_alchemy_sap extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        //$this->load->library('PHPExcel');
    }

    public function index() {

        $this->load->view('excel_import_alchemy_sap');
    }

    public function import_data() {
        $config = array(
            'upload_path' => FCPATH . 'upload/',
            'allowed_types' => 'xls'
        );


        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file')) {
            $data = $this->upload->data();
            @chmod($data['full_path'], 0777);

            $this->load->library('Spreadsheet_Excel_Reader');
            $this->spreadsheet_excel_reader->setOutputEncoding('CP1251');
            //  $this->spreadsheet_excel_reader->setOutputEncoding('UTF-8');

            $this->spreadsheet_excel_reader->read($data['full_path']);


            //Procesando Sheet: "Plantilla SAP"
            $this->processPlantillaSAP($this->spreadsheet_excel_reader->sheets[3]);


            //Procesando Sheet: "Base de datos"
            $this->processWorksiteSAP($this->spreadsheet_excel_reader->sheets[1]);


            echo "<br><a href='javascript:history.back()'>Regresar.</a>";


            //redirect('admon_import_data');
        } else {
            echo "Error: No se pudo cargar el archivo..";
        }
    }

    public function processPlantillaSAP($dataInput) {


        /*
          1.Estado  (default = activo)
          2.Matricula
          3.Puesto
          4.Name of Employee or Applicant
          5.Last name
          6.Second name
          7.First name
          8.Entry Date (Documented Employment)
          9.Short Text of Organizational Unit
          10.Personnel area
          11.Personnel Area Text
          12.Name Localidad
          13.WORKSITE */

        $sheets = $dataInput;

        //error_reporting(0);
        error_reporting(E_ALL);




        $data_excel = array();
        $cantidadUpdates = 0;
        $cantidadInsert = 0;
        $totalCounter = 0;


        //Registrando importacion
        $this->db->set('action', "Importacion Cursos SAP");
        $this->db->set('fk_user', $this->session->userdata('id'));
        $this->db->insert('Imports');

        $fk_imports = $this->db->insert_id();



        //Primera columna debemos poner i=1, para validar las columnas
        //for ($i = 1; $i <= $sheets['numRows']; $i++) {
        for ($i = 2; $i <= $sheets['numRows']; $i++) {

            $matricula = $position = $lastName = $secondName = $firstName = "";
            $ingreso = $costCenter = $costText = $organizational = $organizationalText = "";
            $personelArea = $PersonnelAreasText = $localidad = $status = "";


            $matricula = utf8_encode($sheets['cells'][$i][2]);






            if (isset($sheets['cells'][$i][3]))
                $position = utf8_encode($sheets['cells'][$i][3]);

            if (isset($sheets['cells'][$i][5]))
                $lastName = utf8_encode($sheets['cells'][$i][5]);

            if (isset($sheets['cells'][$i][6]))
                $secondName = utf8_encode($sheets['cells'][$i][6]);

            if (isset($sheets['cells'][$i][7]))
                $firstName = utf8_encode($sheets['cells'][$i][7]);

            if (isset($sheets['cells'][$i][8]))
                $ingreso = ""; // utf8_encode($sheets['cells'][$i][8]);

            if (isset($sheets['cells'][$i][9]))
                $costCenter = utf8_encode($sheets['cells'][$i][9]);

            if (isset($sheets['cells'][$i][10]))
                $costText = utf8_encode($sheets['cells'][$i][10]);


            //OrganizationalUnits
            if (isset($sheets['cells'][$i][11])) {
                $organizational = utf8_encode($sheets['cells'][$i][11]);
                $this->db->where('id', $organizational);
                $q = $this->db->get('OrganizationalUnits');

                if (isset($sheets['cells'][$i][12]))
                    $organizationalText = utf8_encode($sheets['cells'][$i][12]);

                if ($q->num_rows() > 0) {

                    $this->db->set('name', $organizationalText);
                    $this->db->where('id', $organizational);
                    $this->db->update('OrganizationalUnits');
                } else {


                    $this->db->set('id', $organizational);
                    $this->db->set('name', $organizationalText);
                    $this->db->insert('OrganizationalUnits');
                }
            }


            //personelArea
            if (isset($sheets['cells'][$i][13])) {
                $personelArea = utf8_encode($sheets['cells'][$i][13]);
                $this->db->where('id', $personelArea);
                $q = $this->db->get('PersonnelAreas');

                if (isset($sheets['cells'][$i][14]))
                    $PersonnelAreasText = utf8_encode($sheets['cells'][$i][14]);

                if ($q->num_rows() > 0) {

                    $this->db->set('name', $PersonnelAreasText);
                    $this->db->where('id', $personelArea);

                    $this->db->update('PersonnelAreas');
                } else {

                    $this->db->set('id', $personelArea);
                    $this->db->set('name', $PersonnelAreasText);

                    $this->db->insert('PersonnelAreas');
                }
            }






            if (isset($sheets['cells'][$i][15]))
                $localidad = utf8_encode($sheets['cells'][$i][15]);

            if (isset($sheets['cells'][$i][1]))
                $status = utf8_encode($sheets['cells'][$i][1]);




            $this->db->where('idParticipant', $matricula);
            $q = $this->db->get('Participants');


            $this->db->set('position', $position);

            $this->db->set('secondName', $secondName);
            $this->db->set('lastName', $lastName);
            $this->db->set('firstName', $firstName);

            $this->db->set('entryDate', $ingreso);

            $this->db->set('costCenter', $costCenter);
            $this->db->set('costCentertext', $costText);

            $this->db->set('organizationalUnit', $organizational);
            $this->db->set('shortTextOrganizUnit', $organizationalText);

            $this->db->set('fk_import', $fk_imports);


            $this->db->set('personnelArea', $personelArea);
            $this->db->set('personnelAreaText', $PersonnelAreasText);

            $this->db->set('nameLocation', $localidad);

            $this->db->set('status', $status);



            $totalCounter++;

            if ($q->num_rows() > 0) {
                //UPDATE Particpante

                $this->db->where('idParticipant', $matricula);
                $this->db->update('Participants');

                $cantidadUpdates++;
            } else {
                //INSERT Participante

                $this->db->set('idParticipant', $matricula);
                $this->db->insert('Participants');

                $cantidadInsert++;
            }
        }


        echo "cantidad de renglones:" . $totalCounter . "<br>";
        echo "cantidad de updates:" . $cantidadUpdates . "<br>";
        echo "cantidad de inserts:" . $cantidadInsert . "<br>";
    }

    public function processWorksiteSAP($dataInput) {

        /*
          1.Estado
          2.Personnel number
          3.Puesto
          4.Name of Employee or Applicant
          5.Last name
          6.Second name
          7.First name
          8.Entry Date (Documented Employment)
          9.Short Text of Organizational Unit
          10.Personnel area
          11.Personnel Area Text
          12.Name Localidad
          13.WORKSITE */

        $sheets = $dataInput;

        //error_reporting(0);
        error_reporting(E_ALL);




        $data_excel = array();
        $cantidadUpdates = 0;
        $cantidadInsert = 0;
        $totalCounter = 0;


        //Registrando log
        $this->db->set('action', "Importacion Cursos SAP");
        $this->db->set('fk_user', $this->session->userdata('id'));
        $this->db->insert('Imports');

        $fk_imports = $this->db->insert_id();


        //Primera columna debemos poner i=1, para validar las columnas
        //for ($i = 1; $i <= $sheets['numRows']; $i++) {
        for ($i = 2; $i <= $sheets['numRows']; $i++) {

            $matricula = "";
            $worksite = "";



            if (isset($sheets['cells'][$i][2]))
                $matricula = utf8_encode($sheets['cells'][$i][2]);

            if (isset($sheets['cells'][$i][13]))
                $worksite = utf8_encode($sheets['cells'][$i][13]);




            //$q = $this->db->get('Participants');
            $this->db->set('worksite', $worksite);

            $this->db->where('idParticipant', $matricula);
            $this->db->update('Participants');




            //Worksite Catalog
            if (isset($sheets['cells'][$i][13])) {

                $worksite = utf8_encode($sheets['cells'][$i][13]);
                $this->db->like('LOWER(name)', strtolower($worksite));

                $q = $this->db->get('Worksites');


                if ($q->num_rows() == 0) {

                    $this->db->set('name', $worksite);
                    $this->db->set('fk_import', $fk_imports);
                    $this->db->insert('Worksites');
                }



                //Workiste = Procseso + Complexes + Localidad
                $descompWorksite = explode(" ", $worksite);

                //Processes/Area
                $this->db->like('LOWER(name)', strtolower($descompWorksite[0]));
                $q = $this->db->get('Processes');


                if ($q->num_rows() == 0) {
                    $this->db->set('name', $descompWorksite[0]);
                    $this->db->set('fk_import', $fk_imports);
                    $this->db->insert('Processes');
                }


                //Complexes
                if (isset($descompWorksite[1])) {
                    $this->db->like('LOWER(name)', strtolower($descompWorksite[1]));
                    $q = $this->db->get('Complexes');


                    if ($q->num_rows() == 0) {
                        $this->db->set('name', $descompWorksite[1]);
                        $this->db->set('fk_import', $fk_imports);
                        $this->db->insert('Complexes');
                    }
                }
            }//endIf
        }//for
    }

}
