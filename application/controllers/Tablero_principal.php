<?php

/**
 * Controlador para mostrar Panel General - Dasboard.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Tablero_principal extends CI_Controller {

    /**
     * Controlador para mostrar Panel Principal de Pilgrim.
     *
     * @author Constantino Kusulas V
     */
    public function __construct() {
        parent::__construct();

        $this->load->model('tprincipal_model');
        $this->load->model('admon_users_model');



        if (!$this->session->userdata('is_logued_in') == TRUE) {
            redirect(base_url() . 'index.php');
        }

        $profile = $this->session->userdata('profile');
        if ($profile == 3) {
            redirect(base_url() . 'index.php');
        }
    }

    public function index() {
        $this->output->delete_cache();

        $searchWorksite = "";
        $searchComplex = "";
        $searchProcess = "";
        $searchLocation = "";
        $searchDateFinal = "";
        $searchWorkSideSede = "";

        $flagWorkSideSede = 0;

        $listWorksites = "";

        $filterWorksite = "";
        $filterComplex = "";
        $filterProcess = "";
        $filterLocation = "";
        $filterFinitial = "";
        $filterFfinal = "";

        if ($this->input->post('f_submited')) {

            $filterWorksite = $this->input->post('f_worksite');
            $filterComplex = $this->input->post('f_complex');
            $filterProcess = $this->input->post('f_process');
            $filterLocation = $this->input->post('f_location');

            $filterFinitial = $this->input->post('f_finitial');
            $filterFfinal = $this->input->post('f_ffinal');




            if ($filterWorksite != "Todos" && $filterWorksite != "Seleccione Worksite") {
                $searchWorksite = " and worksite='$filterWorksite'";

                if ($filterWorksite == "CORPORATIVO QUERETARO") {
                    $flagWorkSideSede = 1;
                }
            }

            if ($filterComplex != "Todas" && $filterComplex != "Seleccione Complejo") {
                $searchComplex = " and worksite like '%" . $filterComplex . "%'";
            }

            if ($filterProcess != "Todas" && $filterProcess != "Seleccione Area") {
                $searchProcess = " and worksite like '%" . $filterProcess . "%'";

                if ($filterProcess == "CORPORATIVO") {
                    $flagWorkSideSede = 1;
                }
            }

            if ($filterLocation != "Seleccione Localidad" && $filterLocation != "Todas") {
                $searchLocation = ' and "nameLocation"=\'' . $filterLocation . '\'';
            }

            //subquery Especial para cuando Worksite="CORPORATIVO QUERETARO" OR Proceso = "CORPORATIVO"
            if ($flagWorkSideSede == 1) {

                $searchWorkSideSede = " OR ( LOWER('worksiteSede') like '%corporativo%' AND  LOWER('worksite') not like '%corporativo%' )";
            }





            if ($filterFinitial != "" && $filterFfinal != "") {

                $dateInitial = explode("/", $filterFinitial);
                $filterFinitial = $dateInitial[2] . "/" . $dateInitial[1] . "/" . $dateInitial[0];

                $dateFinal = explode("/", $filterFfinal);
                $filterFfinal = $dateFinal[2] . "/" . $dateFinal[1] . "/" . $dateFinal[0];


                $searchDateFinal = ' and "status"=\'Complete\' and "trainingEnd" BETWEEN \'' . $filterFinitial . '\' AND \'' . $filterFfinal . '\' ';
            }

            //Reacomodando fechas
            if ($filterFinitial != "") {

                $l_date = explode("/", $filterFinitial);
                $filterFinitial = $l_date[2] . "/" . $l_date[1] . "/" . $l_date[0];
                $l_date = explode("/", $filterFfinal);
                $filterFfinal = $l_date[2] . "/" . $l_date[1] . "/" . $l_date[0];
            }
        }

        $subQueryGeneral = "$searchWorksite $searchComplex $searchProcess $searchLocation $searchDateFinal $searchWorkSideSede";




        //Internos
        //$qty_hours = $this->tprincipal_model->get_qtyHours(" delivery='interno' $subQueryGeneral");
        $qty_hours = $this->tprincipal_model->get_qtyHours(" source='Alchemy' or source='UP' $subQueryGeneral");
        $qty_hours = number_format($qty_hours, 2);
        //$qty_participants = number_format($this->tprincipal_model->get_qtyPaticipants(" delivery='interno' $subQueryGeneral"));
        $qty_participants = number_format($this->tprincipal_model->get_qtyPaticipants(" source='Alchemy' or source='UP' $subQueryGeneral"));
        $qty_courses = number_format($this->tprincipal_model->get_qtyCourses(" source='Alchemy' or source='UP' $subQueryGeneral"));
        $qty_english = number_format($this->tprincipal_model->get_qtyEnglish(" 1=1  $subQueryGeneral"));
        $qty_hours_english = round($this->tprincipal_model->get_qtyHoursEnglish(" 1=1 $subQueryGeneral"), 2);

        //Enternos
        //$row = $this->tprincipal_model->get_qtyHours(" delivery='externo' $subQueryGeneral");
        $row = $this->tprincipal_model->get_qtyHours(" source='Otros' and \"qtyAtachments\">0 $subQueryGeneral");
        $qty_hoursExt = round($row, 2);
        //$qty_participantsExt = number_format($this->tprincipal_model->get_qtyPaticipants(" delivery='externo' $subQueryGeneral "));
        $qty_participantsExt = number_format($this->tprincipal_model->get_qtyPaticipants("  source='Otros' and \"qtyAtachments\">0   $subQueryGeneral "));
        //$qty_coursesExt = number_format($this->tprincipal_model->get_qtyCourses(" delivery='externo' $subQueryGeneral "));
        $qty_coursesExt = number_format($this->tprincipal_model->get_qtyCourses(" source='Otros' and \"qtyAtachments\">0  $subQueryGeneral "));
        //$qty_englishExt = number_format($this->tprincipal_model->get_qtyEnglish(" delivery='externo' $subQueryGeneral "));
        //QtyHours by Sources
        //$qty_HoursAlchemy = $this->tprincipal_model->get_QtyHourSource("Alchemy"," delivery='interno' $subQueryGeneral");
        $qty_HoursAlchemy = round($this->tprincipal_model->get_QtyHourSource("Alchemy", " 1=1 $subQueryGeneral"), 2);
        //$qty_HoursUP = $this->tprincipal_model->get_QtyHourSource("UP"," delivery='interno' $subQueryGeneral");
        $qty_HoursUP = round($this->tprincipal_model->get_QtyHourSource("UP", " 1=1 $subQueryGeneral"), 2);

        $qty_HoursOthers = $this->tprincipal_model->get_QtyHourSource("Otros", " \"qtyAtachments\">0  $subQueryGeneral");

        $qty_HoursAlchemyExt = $this->tprincipal_model->get_QtyHourSource("Alchemy", " delivery='externo' $subQueryGeneral");
        $qty_HoursUPExt = $this->tprincipal_model->get_QtyHourSource("UP", " delivery='externo' $subQueryGeneral");

        //$qty_HoursOthersExt = $this->tprincipal_model->get_QtyHourSource("Otros"," delivery='externo' $subQueryGeneral");
        $qty_HoursOthersExt = round($this->tprincipal_model->get_QtyHourSource("Otros", " \"qtyAtachments\">0 $subQueryGeneral"), 2);



        //QtyParticipants by source
        //$qty_ParticipantAlchemy = $this->tprincipal_model->get_QtyParticipantSource("Alchemy"," delivery='interno' $subQueryGeneral");
        $qty_ParticipantAlchemy = $this->tprincipal_model->get_QtyParticipantSource("Alchemy", " 1=1 $subQueryGeneral");
        //$qty_ParticipantUP = $this->tprincipal_model->get_QtyParticipantSource("UP"," delivery='interno' $subQueryGeneral");
        $qty_ParticipantUP = $this->tprincipal_model->get_QtyParticipantSource("UP", " 1=1 $subQueryGeneral");

        $qty_ParticipantOthers = $this->tprincipal_model->get_QtyParticipantSource("Otros", "  \"qtyAtachments\">0 $subQueryGeneral");

        $qty_ParticipantAlchemyExt = $this->tprincipal_model->get_QtyParticipantSource("Alchemy", " delivery='externo' $subQueryGeneral");
        $qty_ParticipantUPExt = $this->tprincipal_model->get_QtyParticipantSource("UP", " delivery='externo' $subQueryGeneral");
        $qty_ParticipantOthersExt = $this->tprincipal_model->get_QtyParticipantSource("Otros", " \"qtyAtachments\">0  $subQueryGeneral");


        //QtyCourses by source
        //$qty_CourseAlchemy = $this->tprincipal_model->get_QtyCourseSource("Alchemy"," delivery='interno' $subQueryGeneral");
        $qty_CourseAlchemy = $this->tprincipal_model->get_QtyCourseSource("Alchemy", " 1=1 $subQueryGeneral");
        //$qty_CourseUP = $this->tprincipal_model->get_QtyCourseSource("UP"," delivery='interno' $subQueryGeneral");
        $qty_CourseUP = $this->tprincipal_model->get_QtyCourseSource("UP", " 1=1  $subQueryGeneral");
        //$qty_CourseOthers = $this->tprincipal_model->get_QtyCourseSource("Otros"," delivery='interno' $subQueryGeneral");
        $qty_CourseOthers = $this->tprincipal_model->get_QtyCourseSource("Otros", " \"qtyAtachments\">0  $subQueryGeneral");

        $qty_CourseAlchemyExt = $this->tprincipal_model->get_QtyCourseSource("Alchemy", " delivery='externo' $subQueryGeneral");
        $qty_CourseUPExt = $this->tprincipal_model->get_QtyCourseSource("UP", " delivery='externo' $subQueryGeneral");
        $qty_CourseOthersExt = $this->tprincipal_model->get_QtyCourseSource("Otros", " \"qtyAtachments\">0 $subQueryGeneral");

        //$qty_HoursEnglish = $this->tprincipal_model->get_HourEnglish($subQueryGeneral);

        $listWorksites = $this->tprincipal_model->getCatalog_Worksites();

        $findComplex = "";
        $findProcesses = "";
        $findWorksites = "";
        $findLocations = "";

        if (2 == $this->session->userdata('profile')) {
            $matricula = $this->session->userdata('matricula');
            $listWorksites = $this->admon_users_model->getListWorksitesGestorSpecific($matricula);


            $a_complex = array();
            $a_area = array();
            $a_worksite = array();
            $a_rellocation = array();
            $a_location = array();

            foreach ($listWorksites as $row) {

                $a_complex[] = $row['fk_complex'];
                $a_area[] = $row['fk_area'];
                $a_worksite[] = "'" . $row['name'] . "'";
            }

            $a_complex = array_unique($a_complex);
            $findComplex = implode(",", $a_complex);

            $a_area = array_unique($a_area);
            $findProcesses = implode(",", $a_area);

            $a_worksite = array_unique($a_worksite);
            $findWorksites = implode(",", $a_worksite);

            $relationLoctations_Worksites = $this->tprincipal_model->getCatalog_LocationsFromWorksites($findWorksites);


            foreach ($relationLoctations_Worksites as $row) {

                $a_rellocation[] = "'" . $row['location'] . "'";
            }


            $a_location = array_unique($a_rellocation);
            $findLocations = implode(",", $a_location);
        } else {
            $listWorksites = $this->tprincipal_model->getCatalog_Worksites();
        }

        $listComplexes = $this->tprincipal_model->getCatalog_Complexes($findComplex);
        $listProcesses = $this->tprincipal_model->getCatalog_Processes($findProcesses);
        $listLocations = $this->tprincipal_model->getCatalog_Locations($findLocations);







        $data = array(
            'courses_int' => $qty_courses,
            'hours_int' => $qty_hours,
            'capacitation_int' => $qty_participants,
            'english_int' => $qty_english,
            'english_hours_int' => $qty_hours_english,
            'courses_ext' => $qty_coursesExt,
            'hours_ext' => $qty_hoursExt,
            'capacitation_ext' => $qty_participantsExt,
            //'english_ext' => $qty_englishExt,
            'qty_HoursAlchemy' => $qty_HoursAlchemy,
            'qty_HoursUP' => $qty_HoursUP,
            'qty_HoursOthers' => $qty_HoursOthers,
            'qty_HoursAlchemyExt' => $qty_HoursAlchemyExt,
            'qty_HoursUPExt' => $qty_HoursUPExt,
            'qty_HoursOthersExt' => $qty_HoursOthersExt,
            'qty_ParticipantAlchemy' => $qty_ParticipantAlchemy,
            'qty_ParticipantUP' => $qty_ParticipantUP,
            'qty_ParticipantOthers' => $qty_ParticipantOthers,
            'qty_ParticipantAlchemyExt' => $qty_ParticipantAlchemyExt,
            'qty_ParticipantUPExt' => $qty_ParticipantUPExt,
            'qty_ParticipantOthersExt' => $qty_ParticipantOthersExt,
            'qty_CourseAlchemy' => $qty_CourseAlchemy,
            'qty_CourseUP' => $qty_CourseUP,
            'qty_CourseOthers' => $qty_CourseOthers,
            'qty_CourseAlchemyExt' => $qty_CourseAlchemyExt,
            'qty_CourseUPExt' => $qty_CourseUPExt,
            'qty_CourseOthersExt' => $qty_CourseOthersExt,
            //  'qty_HoursEnglish' => $qty_HoursEnglish,
            'catalogWorksites' => $listWorksites,
            'catalogComplexes' => $listComplexes,
            'catalogProcesses' => $listProcesses,
            'catalogLocations' => $listLocations,
            's_worksite' => $filterWorksite,
            's_complex' => $filterComplex,
            's_process' => $filterProcess,
            's_location' => $filterLocation,
            's_finicial' => $filterFinitial,
            's_ffinal' => $filterFfinal,
        );


        $this->load->view('tprincipal', $data);
    }

}
