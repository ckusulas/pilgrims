<?php

/**
 * Controlador para mostrar Admon de Areas.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Admon_areas extends CI_Controller {

    public $list;

    public function __construct() {
        parent::__construct();
        $this->load->model('admon_areas_model');

        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }
    }

    public function index() {



        $listAreas = $this->admon_areas_model->getlist(0);


        $data = array();



        foreach ($listAreas->result() as $areas) {

            //$complex->ID despues un ahref para editar.
            $data[] = array(
                'id' => $areas->id,
                'name' => $areas->name,
            );
        }


        $output = array(
            "dataTables" => $data,
            "subTitle" => "Areas",
            "subElementSingular" => "Area",
            "name_controller" => "admon_areas/",
        );

        $this->load->view('admon_areas', $output);
    }

    public function ajax_deleteElement($idElement) {


        $result = $this->admon_areas_model->delete($idElement);


        $msg = "";
        $status = $result;
        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getElement($find_elem = "") {

        $elem_area = $this->admon_areas_model->get($find_elem);

        foreach ($elem_area->result() as $row) {
            $id = $row->id;
            $name_area = $row->name;
        }




        $data = array(
            'id' => $id,
            'name_area' => $name_area
        );

        echo json_encode($data);
    }

    public function saveElement() {


        $nameComplex = $this->input->post('nameAreaTXT');
        $idComplex = $this->input->post('idElement');





        $result = $this->admon_areas_model->update($idComplex, $nameComplex);
        if ($result) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function createElement() {


        $nameArea = $this->input->post('nameAreaTXT');


        $result = $this->admon_areas_model->create($nameArea);

        if ($result == "insertado exitosamente") {
            $status = true;
            $msg = '';
        }

        if ($result == "ya existia area") {
            $status = false;
            $msg = $result;
        }

        if ($result == "error insertado") {
            $status = false;
            $msg = "Error al insertar, favor consultar con Soporte.";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getList($find_elem = "") {

        $findCourse = urldecode($find_elem);
        $listComplex = $this->crud_course_model->getlist($findCourse);

        $data = array(
            'listComplex' => $listComplex
        );



        echo json_encode($data);
    }

}
