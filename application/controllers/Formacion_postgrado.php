<?php

/**
 * CRUD para administrar listado de Postgrados
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Formacion_postgrado extends CI_Controller {
                   
    public $list;

    public function __construct() {
        parent::__construct();
        $this->load->model('admon_course_model');
        $this->load->model('tprincipal_model');

        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if($profile == 3  ){
            redirect(base_url().'index.php');
        }

        if($this->session->userdata('id') == "" ){
            redirect(base_url().'index.php');
        }
 
        //Category Postgrado
        $this->list = "4";
    }

    public function index() {


        //'listParticipantes' => $listParticipantesPlane,
        $listCourses = $this->admon_course_model->get_datatables($this->list);


        $data = array();
        $atachmentFiles = array();

        foreach ($listCourses->result() as $course) {

          //Obteniendo lista archivos
            $this->db->where('fk_trainingRecords', $course->IDCourseTraining);
            $this->db->where('active', 1);
            $this->db->order_by('id','DESC');
            $query = $this->db->get('Atachments');
            $cad_atachment = "";
            $qtyAtachment = $query->num_rows();

            foreach ($query->result_array() as $row) {


                $file = array(
                    "id" => $row['id'],
                    "name" => $row['name'],
                    "reformat" => $row['typeExtHTML'],
                    "url" => $row['url']
                );

                $urlf = base_url() . $row['url']. $row['name'];

                $cad_atachment.="<a href='".$urlf."' title='".$row['name']."' >".$row['typeExtHTML']. "</a>,";
            }

             $this->db->set('qtyAtachments', $qtyAtachment);              
            $this->db->where('id', $course->IDCourseTraining);
            $this->db->update('TrainingRecords');

              $this->db->set('qtyAtachments', $qtyAtachment);              
            $this->db->where('cloneFrom', $course->IDCourseTraining);
            $this->db->update('TrainingRecords');
 
 


            //$course->ID despues un ahref para editar.
            $data[] = array(
                'id' => $course->id,
                'courseName' => $course->courseName,
                'complejo' => $course->complejo,
                'area' => $course->Area,
                'subarea' => '', //subArea
                'localidad' => $course->Localidad,
                'tipo' => $course->Tipo,
                'categoria' => $course->Categoria,
                'modalidad' => $course->Modalidad,
                'trainingStart' => $course->trainingStart,
                'trainingEnd' => $course->trainingEnd,
                'noAsistentes' => $course->NoAsistentes,
                'duration' => $course->NoHrs,
                'capacitador' => $course->capacitador,
                'idCourse' => $course->IDCourseTraining,
                'atachment' => $cad_atachment,
            );
        }




        $output = array(
            "dataTables" => $data,
       
        );

        $this->load->view('formacion_postgrado', $output);
    }

    public function ajax_list($find_elem = "") {


        // Datatables Variables
        $draw = intval($this->input->post("draw"));



        $listCourses = $this->admon_course_model->get_datatables($this->list);
        $listTotal = $this->admon_course_model->getlist();

        $data = array();

        foreach ($listCourses->result() as $course) {

            //$course->ID despues un ahref para editar.
            $data[] = array(
                $course->courseName,
                $course->complejo,
                $course->Area,
                '', //subArea
                $course->Localidad,
                $course->Tipo,
                $course->Categoria,
                $course->Modalidad,
                $course->trainingStart,
                $course->trainingEnd,
                $course->NoAsistentes,
                $course->NoHrs,
                $course->capacitador,
                '<h2>adjunto</h2>',
                "<button type='button' onClick='loadNewCourse({$course->IDCourseTraining})' class='btn btn-xs btn-primary waves-effect w-md waves-light m-b-15' data-toggle='modal' data-target='.modal-nuevo'><i class='fa fa-pencil'></i> Detalles</button>
                <button type='button' onClick='deleteCourse({$course->IDCourseTraining})' class='btn btn-xs btn-danger waves-effect w-md waves-light m-b-15' id='sa-warning'><i class='fa fa-trash'></i> Eliminar</button>",
            );
        }
        //$course->IDCourse
        // print_r($data);

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $listCourses->num_rows(),
            "recordsFiltered" => $listTotal->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
    }

    public function ajax_deleteCourse($idCourse) {

        $data = array(
            "idCourse" => $idCourse,
            "type_course" => "Posgrado"

        );
 
        
        $processDelete = $this->admon_course_model->delete($data);

        
      
        if($processDelete){
                $msg = "todo bien";
                $status = true;
                $datos = array(
                    'status' => $status,
                    'msg' => $msg,
                );
        }else{
                $msg = "Error";
                $status = false;
                $datos = array(
                    'status' => $status,
                    'msg' => $msg,
                );
        }


        //  echo json_encode($datos);
        echo json_encode(array($datos));

        //return true;
    }



    public function getInfo_CourseTraining($find_elem = "") {



        $listComplexes = $this->tprincipal_model->getCatalog_Complexes();
        $listLocations = $this->tprincipal_model->getCatalog_Locations();
        $listProcesses = $this->tprincipal_model->getCatalog_Processes();
        $listCategory = $this->admon_course_model->getCatalog_Category($this->list);
        $listInstructors = $this->admon_course_model->getCatalog_Instructor("");


        $listCourses = "";
        $atachmentFiles = array();



        //extract InitialDate Course and Format
        if ($find_elem != "") {
            $find_elem = intval($find_elem);
            $listCourses = $this->admon_course_model->getCourseTraining($find_elem);


            //extract endDate Course and Format
            $trainingStart = "";
            if ($listCourses[0]->trainingStart != "") {
                $initialDate = explode(" ", $listCourses[0]->trainingStart);
                $extractInitial = explode("-", $initialDate[0]);
                $trainingStart = $extractInitial[2] . "/" . $extractInitial[1] . "/" . $extractInitial[0];
            }

            $trainingEnd = "";
            if ($listCourses[0]->trainingEnd != "") {
                $endDate = explode(" ", $listCourses[0]->trainingEnd);
                $extractEnd = explode("-", $endDate[0]);
                $trainingEnd = $extractEnd[2] . "/" . $extractEnd[1] . "/" . $extractEnd[0];
            }


            //Obteniendo lista archivos
            $this->db->where('fk_trainingRecords', $find_elem);
            $this->db->order_by('id','DESC');
            $query = $this->db->get('Atachments');

            foreach ($query->result_array() as $row) {


                $file = array(
                    "id" => $row['id'],
                    "name" => $row['name'],
                    "reformat" => $row['typeExtHTML'],
                    "url" => $row['url']
                );

                $atachmentFiles[] = $file;
            }




            //find idCourse
            $query = $this->db->query('select "fk_course" from "TrainingRecords" where "id"=\'' . $find_elem . '\'');
            $this->db->where('id', $find_elem);
            $query = $this->db->get('TrainingRecords');
            $ret = $query->row();
            $idCourse = $ret->fk_course;
            $refCourses = $listCourses[0];
        } else {
            $idCourse = "others00001";
            $trainingStart = "";
            $trainingEnd = "";
            $refCourses = '';
        }



        $data = array(
            'idCurso' => $idCourse,
            'IDCourseTraining' => $find_elem,
            'listCourses' => $refCourses,
            'catalogComplexes' => $listComplexes,
            'catalogAreas' => $listProcesses,
            'catalogLocations' => $listLocations,
            'catalogCategories' => $listCategory,
            'catalogInstructors' => $listInstructors,
            'trainingStart' => $trainingStart,
            'trainingEnd' => $trainingEnd,
            'atachmentFiles' => $atachmentFiles,
        );

        echo json_encode($data);
    }

    public function ajax_saveCourse() {

        $debug = 0;

        $codeCourse = $this->input->post('codeCourseTXT');
        $idRelationCourse = $this->input->post('IDCourseTraining');
        $courseName = $this->input->post('nameCourseTXT');
        $complexSelect = $this->input->post('complexSelect');
        $areaSelect = $this->input->post('areaSelect');
        $locationSelect = $this->input->post('locationSelect');
        $typeSelect = $this->input->post('typeSelect');
        $categorySelect = $this->input->post('categorySelect');
        $modalitySelect = $this->input->post('modalitySelect');
        $listParticipantsForm = $this->input->post('assistantsList');
        $noParticipants = intval($this->input->post('noAsistentesTXT'));
        $duration = intval($this->input->post('durationCourseTXT'));
        $instructor = $this->input->post('capacitorSelect');
        $methodForm = $this->input->post('methodForm');
        $dateStart = $this->input->post('trainingStartTXT');
        $dateEnd = $this->input->post('trainingEndTXT');
        $teamName = $this->input->post('teamNameTXT');
        $idTrainingTMP = $this->input->post('idAtachmentTMP2');
        $qtyAtachment = $this->input->post('qtyTrainingTMP2');

       




        if ($methodForm == "newForm") {

            //Registrando log
            $this->db->set('action', "Alta Nuevo Curso");
            $this->db->set('fk_user', $this->session->userdata('id'));
            $this->db->insert('Imports');

            $fk_imports = $this->db->insert_id();


            //Preparando obtencion id para nuevoCurso.
            $this->db->like('courseCode', 'others_');
            $this->db->order_by('courseCode', 'DESC');
            $this->db->limit(1);
            $query = $this->db->get('Courses');
            

            if($query->num_rows() > 0) {
                $ret = $query->row();
                $last_courseCode = $ret->courseCode;

            }else{

                $last_courseCode = "others_00000";

            }


            $aElem = explode("_", $last_courseCode);
            $elemento = intval($aElem[1]);
            $elemento++;
            $subNumeric = str_pad($elemento, 5, "0", STR_PAD_LEFT);
            $codeCourse = "others_" . $subNumeric;

            //Preparando nuevo terreno
            //Insertando nuevo curso
            $this->db->set('source', "Otros");
            $this->db->set('courseCode', $codeCourse);
            $this->db->set('fk_import', $fk_imports);
            $this->db->insert('Courses');

            //Insertando nuevo trainingRecord
            $this->db->set('fk_course', $codeCourse); 
            $this->db->insert('TrainingRecords');

            $idRelationCourse = $this->db->insert_id();

           

            $url = "upload/atachment/".$idTrainingTMP;
            rename($url, "upload/atachment/".$idRelationCourse);

            $this->db->set('url', "upload/atachment/".$idRelationCourse."/");
            $this->db->set('fk_trainingRecords', $idRelationCourse);
            $this->db->set('fk_import', $fk_imports);
            $this->db->where('fk_trainingRecords', $idTrainingTMP);
            $this->db->update('Atachments');
        }else{

            //Registrando log
            $this->db->set('action', "Edicion de Curso");
            $this->db->set('fk_user', $this->session->userdata('id'));
            $this->db->insert('Imports');

            $fk_imports = $this->db->insert_id();



        }



        $bandMatchQtyParticipant = 0;

        $listParticipantsClean = explode(",", $listParticipantsForm);

        if ($listParticipantsForm == "") {
            $cantidadParticipantes = 0;
        } else {
            $cantidadParticipantes = count($listParticipantsClean);
        }

        if ($cantidadParticipantes == $noParticipants) {
            $bandMatchQtyParticipant = 1;
        }




        //find listParticipantsDB from TrainingRecords
        $this->db->select('listAsistants');
        $this->db->where('id', $idRelationCourse);
        $query = $this->db->get('TrainingRecords');
        $ret = $query->row();
        $listP = $ret->listAsistants;

        $listP_DB = $listP;
        $listP = "," . $listP;

        if ($bandMatchQtyParticipant) {
            $this->updateTraininCourse($idRelationCourse, $codeCourse, $typeSelect, $modalitySelect, $courseName, $complexSelect, $areaSelect, $locationSelect, $categorySelect, $noParticipants, $listParticipantsForm, $duration, $instructor, $dateStart, $dateEnd, $teamName, $fk_imports);
        }



        if ($listParticipantsForm != "") {
            $listParticipantsClean = explode(",", $listParticipantsForm);


            for ($i = 0; $i < count($listParticipantsClean); $i++) {

                $idSelected = $listParticipantsClean[$i];

 
                    
                    $this->db->select('idParticipant');
                    $this->db->where('id', $idSelected);
                    $query = $this->db->get('Participants');
                    $ret = $query->row();
                    $idParticipant = $ret->idParticipant;

                    $this->db->select('id');
                    $this->db->where('fk_participant', $idParticipant);
                    $this->db->where('cloneFrom', $idRelationCourse);
                    $query = $this->db->get('TrainingRecords');

                    foreach ($query->result() as $row) {
                        $idCloned = $row->id;
                    }





                    if ($query->num_rows() > 0) {

                        if ($bandMatchQtyParticipant) {
                            $this->updateTraininCourse($idCloned, $codeCourse, $typeSelect, $modalitySelect, $courseName, $complexSelect, $areaSelect, $locationSelect, $categorySelect, $noParticipants, $listParticipantsForm, $duration, $instructor, $dateStart, $dateEnd, $teamName, $fk_imports);
                        }


                        //find idParticipant from Pilgrim from id selected
                      /*  $this->db->select('idParticipant');
                        $this->db->where('id', $idSelected);
                        $query = $this->db->get('Participants');

                        $ret = $query->row();
                        $idParticipant = $ret->idParticipant;

                        $this->db->set('fk_participant', $idParticipant);
                        $this->db->where('id', $idRelationCourse);
                        $this->db->update('TrainingRecords');*/
                    } else {



                        $lastidCloned = $this->DuplicateMySQLRecord("TrainingRecords", "id", $idRelationCourse);



                        $this->db->set('cloneFrom', $idRelationCourse);
                        $this->db->where('id', $lastidCloned);
                        $this->db->update('TrainingRecords');

                        $this->db->select('idParticipant');
                        $this->db->where('id', $idSelected);
                        $query = $this->db->get('Participants');
                        $ret = $query->row();
                        $idParticipant = $ret->idParticipant;

                        $this->db->set('fk_participant', $idParticipant);
                        $this->db->where('id', $lastidCloned);
                        $this->db->update('TrainingRecords');
                    }
               
            }
        }


       if ($methodForm != "newForm") {
            if (trim($listP_DB) !== trim($listParticipantsForm) && $bandMatchQtyParticipant == 1) {


                $aListParticipantsDB = explode(",", $listP_DB);
                $listParticipantsForm = "," . $listParticipantsForm . ",";

                foreach ($aListParticipantsDB as $participantDB) {


                    $pos = strpos($listParticipantsForm, $participantDB);
                    if ($pos === FALSE) {
                   

                        $this->db->select('idParticipant');
                        $this->db->where('id', $participantDB);
                        $query = $this->db->get('Participants');
                        $ret = $query->row();
                        $idParticipant = $ret->idParticipant;

                        $this->db->where('fk_participant', $idParticipant);
                        $this->db->where('cloneFrom', $idRelationCourse);
                        $this->db->delete('TrainingRecords');

 
                    }
                }
            }
        }



        $status = true;
        $msg = "";



        if ($bandMatchQtyParticipant == 0) {
            $status = false;
            $msg = "No coincide la cantidad de Asistentes.";
        }



        $datos = array(
            'nameCourse' => $this->input->post('nameCourseTXT'),
            'complejo' => $this->input->post('complexSelect'),
            'areaSelect' => $this->input->post('areaSelect'),
            'locationSelect' => $this->input->post('locationSelect'),
            'typeSelect' => $this->input->post('typeSelect'),
            'trainingStart' => $this->input->post('trainingStartTXT'),
            'trainingEnd' => $this->input->post('trainingEndTXT'),
            'duration' => $this->input->post('durationCourseTXT'),
            'teamName' => $this->input->post('teamNameTXT'),
            'participants' => $this->input->post('assistantsList'),
            'idRelationCourse' => $this->input->post('IDCourseTraining'),
            'idCourse' => $this->input->post('codeCourse'),
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function updateTraininCourse($idRelationCourse, $codeCourse, $typeSelect, $modalitySelect, $courseName, $complexSelect, $areaSelect, $locationSelect, $categorySelect, $noParticipants, $listParticipants, $duration, $instructor, $dateStart, $dateEnd, $teamName, $fk_imports="") {

 
        

        //Updating CourseName
        $this->db->set('fk_import', $fk_imports);
        $this->db->set('courseName', $courseName);
        $this->db->set('type', strtolower($typeSelect));
        $this->db->set('duration', $duration);
        $this->db->where('courseCode', $codeCourse);
        $this->db->update('Courses');
        //echo $this->db->last_query();
        //find idComplex selected
        $this->db->where('name', $complexSelect);
        $query = $this->db->get('Complexes');
        $ret = $query->row();
        $idComplex = $ret->id;

        //find idArea selected
        $this->db->where('name', $areaSelect);
        $query = $this->db->get('Processes');
        $ret = $query->row();
        $idArea = $ret->id;

        //find idLocation selected
        $this->db->where('name', $locationSelect);
        $query = $this->db->get('Locations');
        $ret = $query->row();
        $idLocation = $ret->id;

        //find idCategory selectedselect
        $this->db->where('name', $categorySelect);
        $query = $this->db->get('Category');
        $ret = $query->row();
        $idCategory = $ret->id;


        $this->db->set('fk_import', $fk_imports);
        $this->db->set('fk_complex', $idComplex);
        $this->db->set('fk_area', $idArea);
        $this->db->set('fk_location', $idLocation);
        $this->db->set('fk_category', $idCategory);
        $this->db->set('fk_instructor', $instructor);
        $this->db->set('teamName', $teamName);

        $formatDataStart = "";
        if ($dateStart != "") {
            $aDataStart = explode("/", $dateStart);
            $formatDataStart = $aDataStart[2] . "-" . $aDataStart[1] . "-" . $aDataStart[0];
        }

        $this->db->set('trainingStart', $formatDataStart);



        $formatDataEnd = "";
        if ($dateEnd != "") {
            $aDataEnd = explode("/", $dateEnd);
            $formatDataEnd = $aDataEnd[2] . "-" . $aDataEnd[1] . "-" . $aDataEnd[0];
        }

        $this->db->set('trainingEnd', $formatDataEnd);


        $this->db->set('delivery', strtolower($modalitySelect));

        $this->db->set('calculatedDuration', $duration);
        $this->db->set('listAsistants', $listParticipants);
        $this->db->set('quantityParticipants', intval($noParticipants));
        $this->db->where('id', $idRelationCourse);
        $this->db->update('TrainingRecords');


        //  echo $this->db->last_query();
    }

    public function getList($find_elem = "") {

        $findCourse = urldecode($find_elem);
        $listCourses = $this->admon_course_model->getlist($findCourse);

        $data = array(
            'listCourses' => $listCourses
        );



        echo json_encode($data);
    }

    public function create($title) {

        $this->admon_course_model->create($title);
    }

    public function editWorksite($id, $title) {

        $this->admon_course_model->update($id, $title);
    }

    public function deleteWorksite($id) {

        $this->admon_course_model->delete($id);
    }

    public function getListParticpants() {

        $q = strtoupper(pg_escape_string($_GET["q"]));

        $listParticipants = $this->admon_course_model->getListParticipantsName($q);


        $listPersons = array();

        foreach ($listParticipants as $row) {

            $participant = array(
                "id" => $row['id'],
                "name" => $row['nombrecompleto']
            );

            $listPersons[] = $participant;
        }




        echo json_encode($listPersons);
    }

    public function getListParticpants_specific($IDCourseTraining) {

        //If is newCourse, then show ALL Pleople: 
        if ($IDCourseTraining == 'none') {

            $listPersons = array();
            echo (json_encode($listPersons));
            die();
        } else {

            //find idParticipant from Pilgrim from id selected
            $this->db->select('listAsistants');
            $this->db->where('id', $IDCourseTraining);
            $query = $this->db->get('TrainingRecords');
            $ret = $query->row();
            $listParticipants = $ret->listAsistants;
        }

        $listParticipants = $this->admon_course_model->getListParticipantsName("0", $listParticipants);


        $listPersons = array();

        foreach ($listParticipants as $row) {

            $participant = array(
                "id" => $row['id'],
                "name" => $row['nombrecompleto']
            );

            $listPersons[] = $participant;
        }




        echo json_encode($listPersons);
    }

    function extractNumberParticipant($nameFull) {
        $firstA = explode("[", $nameFull);
        $firstB = explode("]", $firstA[1]);
        return $firstB[0];
    }

    function DuplicateMySQLRecord($table, $primary_key_field, $primary_key_val) {
        /* generate the select query */
        $this->db->where($primary_key_field, $primary_key_val);
        $query = $this->db->get($table);

        foreach ($query->result() as $row) {
            foreach ($row as $key => $val) {
                if ($key != $primary_key_field) {
                    /* $this->db->set can be used instead of passing a data array directly to the insert or update functions */
                    $this->db->set($key, $val);
                }//endif
            }//endforeach
        }//endforeach

        /* insert the new record into table */
        $this->db->insert($table);
        $lastid = $this->db->insert_id();

        return($lastid);
    }

    function upload_files() {

        $idCourseRecord = $this->input->post("IDCourseTraining");

        $idAtachmentTMP = $this->input->post('idAtachmentTMP');
        //echo "content:" . $idAtachmentTMP;



        if($idCourseRecord==""){
            
            if($idAtachmentTMP!="" && $idAtachmentTMP!="undefined"){

                $idCourseRecord = $idAtachmentTMP;

            }else{

                $idCourseRecord = mt_rand(80000000, 90000000);

            }

        }
        //echo "valor:" . $idCourseRecord;

        $saveTo = 'upload/atachment/' . $idCourseRecord . "/";

        if (isset($_FILES['files']) && !empty($_FILES['files'])) {
            $no_files = count($_FILES["files"]['name']);
            for ($i = 0; $i < $no_files; $i++) {
                if ($_FILES["files"]["error"][$i] > 0) {
                    echo "Error: " . $_FILES["files"]["error"][$i] . "<br>";
                } else {

                    if (!file_exists($saveTo)) {
                        mkdir($saveTo, 0777, true);
                    }


                    if (file_exists($saveTo . $_FILES["files"]["name"][$i])) {
                   
                        echo 'El archivo ya existia: <b>' . $_FILES["files"]["name"][$i] . "</b>";
                    } else {
                        move_uploaded_file($_FILES["files"]["tmp_name"][$i], $saveTo . $_FILES["files"]["name"][$i]);
                        echo 'Archivo exitosamente guardado...';


                        //get Extension
                        $name = $_FILES["files"]["name"][$i];
                        $temp = explode(".", $name);
                        $ext = "";
                        if (count($temp) > 1) {
                            $ext = $temp[1];
                        }

                        $typeExt = "";
                        switch (strtolower($ext)) {
                            case "pdf":
                                $typeExt = "<i class='fa fa-file-pdf-o' aria-hidden='true'></i>";
                                break;

                            case "doc":
                            case "docx":
                                $typeExt = "<i class='fa fa-file-word-o' aria-hidden='true'></i>";
                                break;

                            case "xls":
                            case "csv":
                            case "xlsx":
                                $typeExt = "<i class='fa fa-table' aria-hidden='true'></i>";
                                break;

                            case "zip":
                            case "rar":
                                $typeExt = "<i class='fa fa-file-archive-o' aria-hidden='true'></i>";
                                break;

                            case "avi":
                            case "mp4":
                                $typeExt = "<i class='fa fa-file-video-o' aria-hidden='true'></i>";
                                break;

                            case "jpg":
                            case "bmp":
                            case "png":
                                $typeExt = "<i class='fa fa-file-image-o' aria-hidden='true'></i>";
                                break;



                            default:
                                $typeExt = "<i class='fa fa-file-o' aria-hidden='true'></i>";
                                break;
                        }

                         //Registrando log
                        $this->db->set('action', "Carga de Adjunto");
                        $this->db->set('fk_user', $this->session->userdata('id'));
                        $this->db->insert('Imports');

                        $fk_imports = $this->db->insert_id();

                        $this->db->set('fk_import', $fk_imports);
                        $this->db->set('fk_trainingRecords', $idCourseRecord);
                        $this->db->set('fk_trainingRecords', $idCourseRecord);
                        $this->db->set('url', $saveTo);
                        $this->db->set('extension', strtolower($ext));
                        $this->db->set('typeExtHTML', $typeExt);
                        $this->db->set('name', $_FILES['files']['name'][$i]);
                        $this->db->insert('Atachments');
                    }
                    $qtyAtachment = 0;
                    $this->db->where('fk_trainingRecords', $idCourseRecord);
                    $this->db->where('active', 1);
                    $this->db->order_by('id', 'DESC');
                    $query = $this->db->get('Atachments');

                    $qtyAtachment = $query->num_rows();
                      echo '<input type="hidden" id="idTrainingTMP" name="idTrainingTMP" value="'.$idCourseRecord.'">' ;
                      echo "<br>";
                    
                    foreach ($query->result_array() as $row) {
                       
                        echo "<div class='atachment' id='atachment_".$row['id']."'>";
                        echo $row['typeExtHTML'] . "&nbsp;<a href='" . base_url() . $row['url'] . $row['name'] . "'>" . $row['name'] . "</a>&nbsp;<a href='#' onClick='deleteAtachment(".$row['id'].",". $idCourseRecord.")' border='0'><img width='14px' height='14px' src='../assets/images/delete-icon.png'></a></div>";
                    }
                    

                     echo '<input type="hidden"  name="qtyAtachment" id="qtyAtachment" value="'.$qtyAtachment.'">';
                      $this->db->set('qtyAtachments', $qtyAtachment);              
                        $this->db->where('id', $idCourseRecord);
                        $this->db->update('TrainingRecords');

                        $this->db->set('qtyAtachments', $qtyAtachment);              
                        $this->db->where('cloneFrom',  $idCourseRecord);
                        $this->db->update('TrainingRecords');
                }
            }
        } else {
            echo 'Porfavor, seleccione al menos un archivo.';
        }
    }


    public function ajax_deleteAtachment($idAtachment) {


        //Registrando log
        $this->db->set('action', "Eliminacion de Adjunto");
        $this->db->set('fk_user', $this->session->userdata('id'));
        $this->db->insert('Imports');

        $fk_imports = $this->db->insert_id();


        /*$this->db->where('id', $idAtachment);
        $query = $this->db->get('Atachments');

        foreach ($query->result_array() as $row) {
            $url =  $row['url'];
            $file = $row['name']; 
        }
        //echo "eliminando:".$url.$file;

        $url = substr($url,0,strlen($url)-1);
        @unlink($url.$file);*/
        


        //Eliminado Adjunto
        $this->db->set('fk_import', $fk_imports);
        $this->db->set('active', 0);
        $this->db->where('id', $idAtachment);
        $this->db->update('Atachments');
 

        //$this->db->delete('Atachments', array('id' => $idAtachment)); 



        //echo "idCourse:".$idCourse;
        $msg = $idAtachment;
        $status = true;
        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );

        //  echo json_encode($datos);
        echo json_encode(array($datos));

        //return true;
    }

    

}
