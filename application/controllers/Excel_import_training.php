<?php

/**
 * Importador para Cursos UP
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_import_training extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        //$this->load->library('PHPExcel');
    }

    public function index() {

        $this->load->view('excel_import_training');
    }

    public function import_data() {
        $config = array(
            'upload_path' => FCPATH . 'upload/',
            'allowed_types' => 'xls'
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file')) {
            $data = $this->upload->data();

            @chmod($data['full_path'], 0777);

            $this->load->library('Spreadsheet_Excel_Reader');
            $this->spreadsheet_excel_reader->setOutputEncoding('CP1251');


            $this->spreadsheet_excel_reader->read($data['full_path']);


            //Procesando Sheet: "Sheet1"
            $this->processTrainingImport($this->spreadsheet_excel_reader->sheets[0]);



            echo "<br><a href='javascript:history.back()'>Regresar.</a>";



            //redirect('excel-import-sap');
        } else {
            echo "Error: No se pudo cargar el archivo..";
        }
    }

    public function processTrainingImport($dataInput) {


        /*
          1.FirstName
          2.LastName
          3.UserID
          4.DOH
          5.DOT
          6.CourseCode
          7.ParentName
          8.CourseName
          9.TrainingStart
          10.TrainingEnd
          11.Calculated Duration (min)
          12.Est. Duration (min)
          13.CreditHours
          14.Worksite
          15.Status
          16.SistemContentVersion
          17.Facilitator
          18.Score
          19.Base Rate (per hour)
          20.Delivery


         */

        $sheets = $dataInput;

        //error_reporting(0);
        error_reporting(E_ALL);




        $data_excel = array();
        $cantidadUpdates = 0;
        $cantidadInsert = 0;
        $totalCounter = 0;


        //Registrando importacion
        $this->db->set('action', "Importacion Cursos UP");
        $this->db->set('fk_user', $this->session->userdata('id'));
        $this->db->insert('Imports');

        $fk_imports = $this->db->insert_id();




        //Primera columna debemos poner i=1, para validar las columnas
        //for ($i = 1; $i <= $sheets['numRows']; $i++) {
        for ($i = 2; $i <= $sheets['numRows']; $i++) {

            $matricula = $position = $lastName = $secondName = $firstName = "";
            $ingreso = $costCenter = $costText = $organizational = $organizationalText = "";
            $personelArea = $personalAreaText = $localidad = $status = $facilitator = "";

            if (isset($sheets['cells'][$i][3]))
                $matricula = utf8_encode($sheets['cells'][$i][3]);

            if (isset($sheets['cells'][$i][1]))
                $firstName = utf8_encode($sheets['cells'][$i][1]);

            if (isset($sheets['cells'][$i][2]))
                $lastName = utf8_encode($sheets['cells'][$i][2]);

            if (isset($sheets['cells'][$i][6]))
                $courseCode = utf8_encode($sheets['cells'][$i][6]);

            if (isset($sheets['cells'][$i][8]))
                $courseName = utf8_encode($sheets['cells'][$i][8]);

            if (isset($sheets['cells'][$i][9]))
                $trainingStart = utf8_encode($sheets['cells'][$i][9]);

            if (isset($sheets['cells'][$i][10]))
                $trainingEnd = utf8_encode($sheets['cells'][$i][10]);

            if (isset($sheets['cells'][$i][11]))
                $calculatedDuration = utf8_encode($sheets['cells'][$i][11]);

            if (isset($sheets['cells'][$i][12]))
                $estimatedDuration = ""; //utf8_encode($sheets['cells'][$i][12]);

            if (isset($sheets['cells'][$i][13]))
                $creditHours = utf8_encode($sheets['cells'][$i][13]);

            if (isset($sheets['cells'][$i][14]))
                $worksiteSede = utf8_encode($sheets['cells'][$i][14]);

            if (isset($sheets['cells'][$i][15]))
                $statusCourse = utf8_encode($sheets['cells'][$i][15]);

            if (isset($sheets['cells'][$i][17]))
                $facilitator = utf8_encode($sheets['cells'][$i][17]);

            if (isset($sheets['cells'][$i][18]))
                $score = utf8_encode($sheets['cells'][$i][18]);

            if (isset($sheets['cells'][$i][20]))
                $type_capac = utf8_encode($sheets['cells'][$i][20]);

            if (isset($sheets['cells'][$i][21]))
                $worksiteUser = utf8_encode($sheets['cells'][$i][21]);

            if (isset($sheets['cells'][$i][22]))
                $type = utf8_encode($sheets['cells'][$i][22]);











            //USUARIO
            /* $this->db->where('idParticipant', $matricula);
              $q = $this->db->get('Participants');


              $this->db->set('position', $position);


              $this->db->set('lastName', $lastName);
              $this->db->set('firstName', $firstName);

              $this->db->set('worksite', $worksiteUser);



              $totalCounter++;

              if ($q->num_rows() > 0) {
              //UPDATE Particpante

              $this->db->where('idParticipant', $matricula);
              $this->db->update('Participants');

              $cantidadUpdates++;
              } else {
              //INSERT Participante

              $this->db->set('idParticipant', $matricula);
              $this->db->insert('Participants');

              $cantidadInsert++;
              } */


            //Course
            $this->db->where('courseCode', $courseCode);
            //$this->db->like('LOWER(courseCode)', strtolower($courseCode));
            $q = $this->db->get('Courses');

            $source = "Alchemy";
            if ($type == "interno")
                $source = "UP";


            $this->db->set('courseName', $courseName);
            $this->db->set('capac_type', $type_capac);
            $this->db->set('duration', $estimatedDuration);
            $this->db->set('type', $type);
            $this->db->set('source', $source);
            $this->db->set('fk_import', $fk_imports);





            $totalCounter++;

            if ($q->num_rows() > 0) {

                //UPDATE Course
                $this->db->where('courseCode', $courseCode);
                $this->db->update('Courses');

                $cantidadUpdates++;
            } else {

                //INSERT Course
                $this->db->set('courseCode', $courseCode);
                $this->db->insert('Courses');

                $cantidadInsert++;
            }


            //TrainingCourse
            $this->db->where('fk_participant', $matricula);
            $this->db->where('fk_course', $courseCode);
            $this->db->where('trainingStart', $trainingStart);
            //$this->db->like('LOWER(fk_participant)', strtolower($matricula));
            //$this->db->like('LOWER(fk_course)', strtolower($courseCode));
            $q = $this->db->get('TrainingRecords');

            $facilitatorA = explode(",", $facilitator);
            $facilitador = "Otro";
            if (isset($facilitatorA[1])) {
                $facilitator = $facilitatorA[1];
            }
            $this->db->set('fk_instructor', $facilitator);

            //$this->db->set('courseName', $courseName);
            //$this->db->set('source', $source);

            $this->db->set('trainingStart', $trainingStart);
            $this->db->set('trainingEnd', $trainingEnd);

            if ($calculatedDuration == "over 7 days")
                $calculatedDuration = "10080";

            $this->db->set('calculatedDuration', $calculatedDuration);

            $this->db->set('worksiteSede', $worksiteSede);
            $this->db->set('score', $score);
            $this->db->set('delivery', $type);
            $this->db->set('status', $statusCourse);




            $totalCounter++;

            if ($q->num_rows() > 0) {
                //UPDATE TrainingRecords
                $this->db->where('fk_participant', $matricula);
                $this->db->where('fk_course', $courseCode);
                $this->db->where('trainingStart', $trainingStart);

                //$this->db->like('LOWER(courseCode)', strtolower($courseCode));
                $this->db->update('TrainingRecords');

                $cantidadUpdates++;
            } else {
                //INSERT TrainingRecords

                $this->db->set('fk_course', $courseCode);
                $this->db->set('fk_participant', $matricula);

                $this->db->insert('TrainingRecords');

                $cantidadInsert++;
            }
        }


        echo "cantidad de renglones:" . $totalCounter . "<br>";
        echo "cantidad de updates:" . $cantidadUpdates . "<br>";
        echo "cantidad de inserts:" . $cantidadInsert . "<br>";
    }

}
