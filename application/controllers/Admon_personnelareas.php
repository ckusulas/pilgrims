<?php

/**
 * Crud para mostrar Admon de Personal Areas.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Admon_personnelareas extends CI_Controller {

    public $list;

    public function __construct() {
        parent::__construct();
        $this->load->model('admon_personnelareas_model');

        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }
    }

    public function index() {


        $listpersonnelareas = $this->admon_personnelareas_model->getlist(0);


        $data = array();



        foreach ($listpersonnelareas->result() as $personnelareas) {

            //$complex->ID despues un ahref para editar.
            $data[] = array(
                'id' => $personnelareas->id,
                'name' => $personnelareas->name,
            );
        }


        $output = array(
            "dataTables" => $data,
            "subTitle" => "personnelareas",
            "subElementSingular" => "PersonnelArea",
            "name_controller" => "admon_personnelareas/",
        );

        $this->load->view('admon_personnelareas', $output);
    }

    public function ajax_deleteElement($idElement) {


        $result = $this->admon_personnelareas_model->delete($idElement);


        $msg = "";
        $status = $result;
        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getElement($find_elem = "") {

        $elem_area = $this->admon_personnelareas_model->get($find_elem);

        foreach ($elem_area->result() as $row) {
            $id = $row->id;
            $name_personnelarea = $row->name;
        }




        $data = array(
            'id' => $id,
            'name_personnelarea' => $name_personnelarea
        );

        echo json_encode($data);
    }

    public function saveElement() {

        $namePersonnelarea = $this->input->post('namePersonnelAreaTXT');
        $idPersonnelarea = $this->input->post('idElement');


        $result = $this->admon_personnelareas_model->update($idPersonnelarea, $namePersonnelarea);
        if ($result) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function createElement() {


        $namePersonnelarea = $this->input->post('namePersonnelAreaTXT');




        $result = $this->admon_personnelareas_model->create($namePersonnelarea);

        if ($result == "insertado exitosamente") {
            $status = true;
            $msg = '';
        }

        if ($result == "ya existia Personnelarea") {
            $status = false;
            $msg = $result;
        }

        if ($result == "error insertado") {
            $status = false;
            $msg = "Error al insertar, favor consultar con Soporte.";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getList($find_elem = "") {

        $findCourse = urldecode($find_elem);
        $listComplex = $this->crud_course_model->getlist($findCourse);

        $data = array(
            'listComplex' => $listComplex
        );



        echo json_encode($data);
    }

}
