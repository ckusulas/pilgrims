<?php

/**
 * Crud para mostrar Admon de Unidad Organizacional.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Admon_organizationalunits extends CI_Controller {

    public $list;

    public function __construct() {
        parent::__construct();
        $this->load->model('admon_organizationalunits_model');

        $profile = $this->session->userdata('profile');
        $token = $this->session->userdata('token');

        if ($profile != 1) {
            redirect(base_url() . 'index.php');
        }
    }

    public function index() {



        $listorganizationalunits = $this->admon_organizationalunits_model->getlist(0);


        $data = array();



        foreach ($listorganizationalunits->result() as $organizationalunits) {

            //$complex->ID despues un ahref para editar.
            $data[] = array(
                'id' => $organizationalunits->id,
                'name' => $organizationalunits->name,
            );
        }


        $output = array(
            "dataTables" => $data,
            "subTitle" => "Grupo Organizacional",
            "subElementSingular" => "GrupoOrganizacional",
            "name_controller" => "admon_organizationalunits/",
        );

        $this->load->view('admon_organizationalunits', $output);
    }

    public function ajax_deleteElement($idElement) {


        $result = $this->admon_organizationalunits_model->delete($idElement);


        $msg = "";
        $status = $result;
        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getElement($find_elem = "") {

        $elem_area = $this->admon_organizationalunits_model->get($find_elem);

        foreach ($elem_area->result() as $row) {
            $id = $row->id;
            $name_organizationalunit = $row->name;
        }




        $data = array(
            'id' => $id,
            'name_organizationalunit' => $name_organizationalunit
        );

        echo json_encode($data);
    }

    public function saveElement() {


        $nameGrupoOrganizacional = $this->input->post('nameGrupoOrganizacionalTXT');
        $idGrupoOrganizacional = $this->input->post('idElement');





        $result = $this->admon_organizationalunits_model->update($idGrupoOrganizacional, $nameGrupoOrganizacional);
        if ($result) {
            $status = true;
            $msg = "Ok";
        } else {
            $status = false;
            $msg = "Error";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function createElement() {


        $nameOrganizUnit = $this->input->post('nameGrupoOrganizacionalTXT');


        $result = $this->admon_organizationalunits_model->create($nameOrganizUnit);

        if ($result == "insertado exitosamente") {
            $status = true;
            $msg = '';
        }

        if ($result == "ya existia Grupo Organizacional") {
            $status = false;
            $msg = $result;
        }

        if ($result == "error insertado") {
            $status = false;
            $msg = "Error al insertar, favor consultar con Soporte.";
        }



        $datos = array(
            'status' => $status,
            'msg' => $msg,
        );


        echo json_encode(array($datos));
    }

    public function getList($find_elem = "") {

        $findCourse = urldecode($find_elem);
        $listComplex = $this->crud_course_model->getlist($findCourse);

        $data = array(
            'listComplex' => $listComplex
        );



        echo json_encode($data);
    }

}
