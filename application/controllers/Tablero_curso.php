<?php

/**
 * Controlador para mostrar Panel de Cursos.
 *
 * @author Constantino Kusulas V
 * @company Bussem Consulting
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Tablero_curso extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('tprincipal_model');
        $this->load->model('tcurso_model');
    }

    public function index() {

        //$this->output->delete_cache();
        $data = "";


        $searchCourse = "";
        $searchWorksite = "";
        $searchComplex = "";
        $searchProcess = "";
        $searchLocation = "";
        $searchDateFinal = "";
        $searchWorkSideSede = "";
        $filterWorksite = "";
        $filterComplex = "";
        $filterProcess = "";
        $filterLocation = "";
        $filterFinitial = "";
        $filterFfinal = "";
        $filterCourse = "";

        $flagWorkSideSede = 0;



        if ($this->input->post('f_submited')) {

            $filterCourse = $this->input->post('f_course');

            $filterWorksite = $this->input->post('f_worksite');
            $filterComplex = $this->input->post('f_complex');
            $filterProcess = $this->input->post('f_process');
            $filterLocation = $this->input->post('f_location');
            $filterFinitial = $this->input->post('f_finitial');
            $filterFfinal = $this->input->post('f_ffinal');
            //echo "fechiInicial:" . $filterFfinal;

            if ($filterCourse == "Seleccione Curso" || $filterCourse == "Todos") {
                $filterCourse = "Todos los cursos";
            } else {

                $searchCourse = ' and "courseName" like \'' . $filterCourse . '\' ';
            }

            if ($filterComplex != "Seleccione Complejo" && $filterComplex != "Todas") {
                $searchComplex = " and worksite like '%" . $filterComplex . "%'";
            }



            if ($filterWorksite != "Seleccione Worksite" && $filterWorksite != "Todos") {
                $searchWorksite = " and worksite='$filterWorksite'";

                if ($filterWorksite == "CORPORATIVO QUERETARO") {
                    $flagWorkSideSede = 1;
                }
            }



            if ($filterProcess != "Seleccione Area" && $filterProcess != "Todos") {
                $searchProcess = " and worksite like '%" . $filterProcess . "%'";

                if ($filterProcess == "CORPORATIVO") {
                    $flagWorkSideSede = 1;
                }
            }

            if ($filterLocation != "Seleccione Localidad" && $filterLocation != "Todas") {
                $searchLocation = ' and "nameLocation"=\'' . $filterLocation . '\'';
            }

            //subquery Especial para cuando Worksite="CORPORATIVO QUERETARO" OR Proceso = "CORPORATIVO"
            if ($flagWorkSideSede == 1) {

                $searchWorkSideSede = ""; //" OR ( LOWER('worksiteSede') like '%corporativo%' AND  LOWER('worksite') not like '%corporativo%' )";
            }





            if ($filterFinitial != "" && $filterFfinal != "") {

                $dateInitial = explode("/", $filterFinitial);
                $filterFinitial = $dateInitial[2] . "/" . $dateInitial[1] . "/" . $dateInitial[0];

                $dateFinal = explode("/", $filterFfinal);
                $filterFfinal = $dateFinal[2] . "/" . $dateFinal[1] . "/" . $dateFinal[0];


                $searchDateFinal = ' and "status"=\'Complete\' and "trainingEnd" BETWEEN \'' . $filterFinitial . '\' AND \'' . $filterFfinal . '\' ';
            }

            //Reacomodando fechas
            if ($filterFinitial != "") {

                $l_date = explode("/", $filterFinitial);
                $filterFinitial = $l_date[2] . "/" . $l_date[1] . "/" . $l_date[0];
                $l_date = explode("/", $filterFfinal);
                $filterFfinal = $l_date[2] . "/" . $l_date[1] . "/" . $l_date[0];
            }
        }

        $subQueryGeneral = "$searchCourse $searchWorksite $searchComplex $searchProcess $searchLocation $searchDateFinal $searchWorkSideSede";


        $qty_hours = $this->tcurso_model->get_qtyHours($subQueryGeneral);
        $qty_hours = number_format($qty_hours, 2);
        $qty_participants = number_format($this->tcurso_model->get_qtyPaticipants($subQueryGeneral));
        $times_course = number_format($this->tcurso_model->get_timesCourses($subQueryGeneral));
        $listInstructors = $this->tcurso_model->getListInstructors($subQueryGeneral);
        $qtyInstructors = $this->tcurso_model->get_qtyInstructors($subQueryGeneral);



        //Listado de Catalogos
        $listCourses = $this->tcurso_model->getCatalog_Courses();
        $listComplexes = $this->tprincipal_model->getCatalog_Complexes();
        $listProcesses = $this->tprincipal_model->getCatalog_Processes();
        $listLocations = $this->tprincipal_model->getCatalog_Locations();
        $listWorksites = $this->tprincipal_model->getCatalog_Worksites();





        $data = array(
            //'listParticipantes' => $listParticipantesPlane,
            'catalogCourses' => $listCourses,
            'catalogComplexes' => $listComplexes,
            'catalogProcesses' => $listProcesses,
            'catalogLocations' => $listLocations,
            'catalogWorksites' => $listWorksites,
            'listInstructors' => $listInstructors,
            'hours_int' => $qty_hours,
            'qty_participants' => $qty_participants,
            'times_course' => $times_course,
            'qty_instructors' => $qtyInstructors,
            's_course' => $filterCourse,
            's_worksite' => $filterWorksite,
            's_complex' => $filterComplex,
            's_process' => $filterProcess,
            's_location' => $filterLocation,
            's_finicial' => $filterFinitial,
            's_ffinal' => $filterFfinal,
        );




        $this->load->view('tcurso', $data);
    }

}
