<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tpersonal_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getWorksite_EventView() {

        $sql = 'select distinct("worksite") from "EventViews"  order by "worksite"';



        $query = $this->db->query($sql);
        $listWorksites = $query->result_array();

        return($listWorksites);
    }

    public function getLocation_FilterbyWorksite($find_worksite = "") {

        $searchWorksite = "";

        if ($find_worksite != "" && $find_worksite != "Todos" && $find_worksite != "Seleccione Worksite") {
            $searchWorksite = ' and "worksite"=\'' . $find_worksite . '\'';
        }

        //$sql = 'select "nameLocation" from "EventViews" ' . $searchWorksite . ' group by "nameLocation" order by "nameLocation"';.
        $sql = 'select "Locations"."id", "Locations"."name" from "EventViews", "Locations" where "Locations"."name"="EventViews"."nameLocation" ' . $searchWorksite . '   group by "nameLocation","id","name" order by "nameLocation"';


        // echo "query:" . $sql;
        $query = $this->db->query($sql);
        $listLocations = $query->result_array();

        return($listLocations);
    }

    public function getParticipants_FilterbyLocation($find_location = "", $refWorksite = "") {

        $searchLocation = "";

        /* echo "find_location:" . $find_location . "<br>";
          echo "find_refWorksite:" . $refWorksite . "<br>"; */

        if ($find_location != "" && $find_location != "Otros") {
            $searchLocation = ' where "nameLocation"=\'' . $find_location . '\'';
        } elseif ($find_location == "Otros") {
            $searchLocation = ' where "worksite"=\'' . $refWorksite . '\'';
        }

        $sql = ' select "userID", "lastName", UPPER(concat("lastName",\' \',"secondName",\' \',"firstName")) as nameComplete from "EventViews" ' . $searchLocation . ' group by "userID","lastName","secondName","firstName" order by "lastName" ';

        // echo "query:" . $sql;
        $query = $this->db->query($sql);
        $listParticipants = $query->result_array();

        return($listParticipants);
    }

    public function getHoursParticipant($idUser) {

        $sql = 'select SUM("calculatedDuration") as "minutos" from "EventViews" where "userID"=\'' . $idUser . '\' ';
        $query = $this->db->query($sql);
        $row = $query->row_array();


        $hours = round($row["minutos"] / 60, 2);
        return($hours);
    }

    public function getNameParticipant($idUser) {

        $sql = 'select UPPER("lastName") as "lastName", UPPER("secondName") as "secondName", UPPER("firstName") as "firstName" from "Participants" where "idParticipant"=\'' . $idUser . '\'';
        $query = $this->db->query($sql);
        $nameParticipant = $query->result_array();

        return($nameParticipant);
    }

    public function getListCoursesByUser($idUser) {

        $sql = 'select "trainingID","courseName", "status", "score" from "EventViews" where "userID"=\'' . $idUser . '\' order by  "status","score" ';
        $query = $this->db->query($sql);
        $listCourses = $query->result_array();

        return($listCourses);
    }

}
