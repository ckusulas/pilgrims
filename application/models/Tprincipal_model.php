<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tprincipal_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_qtyHours($parametro) {
        $sql = 'select  SUM("calculatedDuration") as "minutos" from "EventViews" where ' . $parametro;
        //echo $sql;
        $query = $this->db->query($sql);
        $row = $query->row_array();

        $hours = $row["minutos"] / 60;
        return($hours);
    }

    public function get_qtyPaticipants($parametro) {

        $query = $this->db->query('select distinct("userID") from "EventViews" where ' . $parametro);
        $qty = $query->num_rows();

        return($qty);
    }

    public function get_qtyCourses($parametro) {
        $sql = 'select distinct("courseCode") from "EventViews" where ' . $parametro;
        //echo $sql . "<br>";
        $query = $this->db->query($sql);
        $qty = $query->num_rows();

        return($qty);
    }

    public function get_qtyEnglish($parametro) {

        $query = $this->db->query('select "userID" from "EventViews" where LOWER("courseName") like \'%engli%\' or LOWER("courseName") like \'%traini%\'  and ' . $parametro . ' group by "userID" ');
        $qty = $query->num_rows();

        return($qty);
    }

    public function get_qtyHoursEnglish($parametro) {


        $query = $this->db->query('select  SUM("calculatedDuration") as "horas" from "EventViews" where LOWER("courseName") like \'%engli%\' or LOWER("courseName") like \'%traini%\'  and ' . $parametro);
        $row = $query->row_array();

        $hours = $row["horas"] / 60;
        return($hours);
    }

    public function get_QtyHourSource($source, $parametro) {

        $query = $this->db->query('select  SUM("calculatedDuration") as "horas" from "EventViews" where "source"=\'' . $source . '\' and ' . $parametro);
        $row = $query->row_array();

        $hours = $row["horas"] / 60;
        return($hours);
    }

    public function get_QtyParticipantSource($source, $parametro) {

        $query = $this->db->query('select distinct("userID") from "EventViews" where "source"=\'' . $source . '\' and ' . $parametro);
        $qty = $query->num_rows();

        return($qty);
    }

    public function get_QtyCourseSource($source, $parametro) {

        $sql = 'select distinct("courseCode") from "EventViews" where "source"=\'' . $source . '\' and ' . $parametro;

        $query = $this->db->query($sql);
        $qty = $query->num_rows();

        return($qty);
    }

    public function get_QtyHourEnglish($parametro) {
        $query = $this->db->query('select  SUM("calculatedDuration") as "horas" from "EventViews" where LOWER("courseName") like \'%engli%\' or LOWER("courseName") like \'%traini%\' ' . $parametro);
        $row = $query->row_array();

        $hours = $row["horas"] / 60;
        return($hours);
    }

    public function getCatalog_Worksites() {
        $query = $this->db->query('select * from "Worksites" where "status"=1   ORDER BY "name"');
        $listWorksites = $query->result_array();

        return($listWorksites);
    }

    public function getCatalog_Complexes($find = "") {

        if ("" == $find) {
            $query = $this->db->query('select * from "Complexes" where "status"=1   ORDER BY "name"');
        } else {
            $query = $this->db->query('select * from "Complexes" where "status"=1 and "id" in(' . $find . ')  ORDER BY "name"');
        }

        $listComplexes = $query->result_array();

        return($listComplexes);
    }

    public function getCatalog_Processes($find = "") {

        if ("" == $find) {
            $query = $this->db->query('select * from "Processes" where "status"=1   ORDER BY "name"');
        } else {
            $query = $this->db->query('select * from "Processes" where "status"=1 and "id" in(' . $find . ')   ORDER BY "name"');
        }
        $listProcesses = $query->result_array();

        return($listProcesses);
    }

    public function getCatalog_Locations($find = "") {

        if ("" == $find) {
            $sql = 'select * from "Locations" where "status"=1  ORDER BY "name"';
        } else {
            $sql = 'select * from "Locations" where "status"=1  and "name" in(' . $find . ') ORDER BY "name"';
        }
        $query = $this->db->query($sql);
        $listLocations = $query->result_array();

        return($listLocations);
    }

    public function getCatalog_LocationsFromWorksites($parameter = "") {

        $sql = 'select * from "Relations" where "worksite" in(' . $parameter . ') ORDER BY "location"';
        $query = $this->db->query($sql);
        $listLocations = $query->result_array();

        return($listLocations);
    }

    public function getListParticipantsName() {
        $query = $this->db->query('select "lastName", "secondName", "firstName", "idParticipant"  from "Participants" ORDER BY "lastName" limit 10');
        $listParticipants = $query->result_array();

        return($listParticipants);
    }

}
