<?php

defined('BASEPATH') OR exit('No direct script access allowed');

define("TABLE", "Instructors");
define("ELEMENT", "el Instructor");

require_once APPPATH . 'interfaces/CrudModelInterface.php';

class Admon_instructors_model extends CI_Model implements CrudModelInterface {

    public function __construct() {
        parent::__construct();
    }

    public function get($id) {

        $this->db->where('id', $id);
        $query = $this->db->get(TABLE);
        return($query);
    }

    public function create($data) {

        $firstName = trim($data['firstName']);
        $lastName = trim($data['lastName']);
        $secondName = trim($data['secondName']);

        //validar si existe



        if ($firstName != "") {

            $sql = 'select * from "' . TABLE . '" where lower("firstName")=\'' . strtolower($firstName) . '\' and lower("lastName")=\'' . strtolower($lastName) . '\' and lower("secondName")=\'' . strtolower($secondName) . '\'';

            $query = $this->db->query($sql);
            $qty = $query->num_rows();

            if ($qty > 0) {
                //echo "Ya existia este area";
                return "ya existia instructor";
            } else {
                $this->db->set('firstName', $firstName);
                $this->db->set('lastName', $lastName);
                $this->db->set('secondName', $secondName);
                $this->db->insert(TABLE);



                if ($this->db->affected_rows() > 0) {
                    return "insertado exitosamente";
                } else {
                    return "error insertado";
                }
            }
        }
    }

    public function update($id, $data) {


        $this->db->set('firstName', $data["firstName"]);
        $this->db->set('lastName', $data["lastName"]);
        $this->db->set('secondName', $data["secondName"]);
        $this->db->where('id', $id);
        $this->db->update(TABLE);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id) {
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        $this->db->update(TABLE);

        //echo $this->db->last_query();

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getlist($find_course = "") {


        $sql = 'SELECT * FROM "' . TABLE . '" where "status"=1 order by "firstName"';
        $listAreas = $this->db->query($sql);


        return($listAreas);
    }

}
