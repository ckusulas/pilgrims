<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tcurso_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getCatalog_Courses() {
        //$query = $this->db->query(' select "courseCode","courseName" from "Courses" order by "courseName" ');
        $query = $this->db->query(' select "courseCode","courseName" from "EventViews"   group by "courseCode", "courseName" order by "courseName" ');
        $listCourses = $query->result_array();

        return($listCourses);
    }

    public function get_qtyHours($parametro) {

        $query = $this->db->query('select  SUM("calculatedDuration") as "horas" from "EventViews" where 1=1 ' . $parametro);
        $row = $query->row_array();

        //     echo "Query:<b>" . 'select  SUM("calculatedDuration") as "horas" from "EventViews" where ' . $parametro . "</b>";

        $hours = $row["horas"] / 60;
        return($hours);
    }

    public function get_qtyPaticipants($parametro) {
        // echo "query:" . 'select distinct("userID") from "EventViews" where ' . $parametro . "<br>";
        $query = $this->db->query('select distinct("userID") from "EventViews" where 1=1 ' . $parametro);
        $qty = $query->num_rows();

        return($qty);
    }

    public function get_qtyInstructors($parametro) {
        // echo "query:" . 'select distinct("userID") from "EventViews" where ' . $parametro . "<br>";
        $query = $this->db->query('select distinct("fk_instructor") from "EventViews" where 1=1 ' . $parametro);
        $qty = $query->num_rows();

        return($qty);
    }

    public function get_timesCourses($parametro) {
        // echo "query:" . 'select distinct("userID") from "EventViews" where ' . $parametro . "<br>";
        $query = $this->db->query('select  "courseName", "trainingEnd" from "EventViews" where 1=1  ' . $parametro . ' group by "courseName", "trainingEnd"');
        $qty = $query->num_rows();

        return($qty);
    }

    public function getListInstructors($parametro) {

        //  $sql = 'select "fk_instructor", "score" from "EventViews" where '. $parametro .' group by fk_instructor,score limit 5';
        $sql = 'select "fk_instructor", AVG("score") as "prom" from prueba."EventViews" where 1=1  ' . $parametro . ' group by fk_instructor ';

        $query = $this->db->query($sql);

        $listInstructors = $query->result_array();

        return($listInstructors);
    }

}
