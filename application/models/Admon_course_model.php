<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'interfaces/CrudModelInterface.php';

class Admon_course_model extends CI_Model implements CrudModelInterface {

    public function __construct() {
        parent::__construct();
    }

    public function get($id) {
        echo "obtener course";
    }

    public function create($campos) {
        //echo "create course";
        //validate if exist "text" previous, with logical existence.
        // if not then create
        // if yes then show error.
    }

    public function update($id, $title) {
        echo "updating course";
        //find id, and apply update
    }

    public function delete($data) {

        $idCourse = $data["idCourse"];
        $type_course = $data["type_course"];



        $this->db->select('fk_course');
        $this->db->where('id', $idCourse);
        $data = $this->db->get('TrainingRecords');

        foreach ($data->result() as $row) {
            $fk_course = $row->fk_course;
        }


        //Registrando log
        $this->db->set('action', "Eliminacion de " . $type_course);
        $this->db->set('fk_user', $this->session->userdata('id'));
        $this->db->insert('Imports');

        $fk_imports = $this->db->insert_id();

        //Eliminado Curso
        $this->db->set('fk_import', $fk_imports);
        $this->db->set('active', 0);
        $this->db->where('courseCode', $fk_course);
        $this->db->update('Courses');

        //Eliminando Relacion TrainingRecords
        //Cursos de asistentes
        $this->db->set('fk_import', $fk_imports);
        $this->db->set('active', 0);
        $this->db->where('cloneFrom', $idCourse);
        $this->db->update('TrainingRecords');

        //Curso principal
        $this->db->set('fk_import', $fk_imports);
        $this->db->set('active', 0);
        $this->db->where('id', $idCourse);
        $this->db->update('TrainingRecords');



        if ($this->db->affected_rows() > 0) {

            return true;
        } else {

            return false;
        }
    }

    public function getlist($find_course = "") {

        $searchCourse = "";
        if (isset($_POST['search']) && $_POST['search']['value'] != "") {
            $searchCourse = ' and "courseName" like \'%' . $_POST['search']['value'] . '%\'';
        }


        $sql = 'SELECT  "C"."courseName", "B"."name" as "complejo", "P"."name" as "Area", "L"."name" as "Localidad","C"."type" as "Tipo", "Cat"."name" as "Categoria", "T"."delivery" as "Modalidad", "T"."trainingStart","T"."trainingEnd", "T"."quantityParticipants" as "NoAsistentes", "C"."duration" as "NoHrs", "T"."fk_instructor" as "capacitador", "T"."fk_course"  FROM "Courses" as "C" JOIN "TrainingRecords" as "T" ON "C"."courseCode" = "T"."fk_course" LEFt JOIN "Complexes" "B" ON "T"."fk_complex" = "B"."id" LEFt JOIN "Processes" "P" ON "T"."fk_area" = "P"."id" LEFt JOIN "Locations" "L" ON "T"."fk_location" = "L"."id" LEFt JOIN "Category" "Cat" ON "T"."fk_category" = "Cat"."id"  where "C"."source"=\'Otros\' and "T"."fk_category" in (1,2) and "T"."cloneFrom" is NULL ' . $searchCourse;


        $listCourses = $this->db->query($sql);
        $listCourses = $query->result_array();

        return($listCourses);
    }

    public function get_datatables($list) {


        $sql = 'SELECT  "T"."id", "C"."courseName", "B"."name" as "complejo", "P"."name" as "Area", "L"."name" as "Localidad","C"."type" as "Tipo", "Cat"."name" as "Categoria", "T"."delivery" as "Modalidad", "T"."trainingStart","T"."trainingEnd", "T"."quantityParticipants" as "NoAsistentes", "C"."duration" as "NoHrs", "T"."fk_instructor" as "capacitador", "T"."id" as "IDCourseTraining" FROM "Courses" as "C" JOIN "TrainingRecords" as "T" ON "C"."courseCode" = "T"."fk_course"  LEFt JOIN "Complexes" "B" ON "T"."fk_complex" = "B"."id" LEFt JOIN "Processes" "P" ON "T"."fk_area" = "P"."id" LEFt JOIN "Locations" "L" ON "T"."fk_location" = "L"."id" LEFt JOIN "Category" "Cat" ON "T"."fk_category" = "Cat"."id"  where "C"."source"=\'Otros\' and "T"."fk_category" in  (' . $list . ')  and "T"."active"=1 and  "T"."cloneFrom" is NULL order by "T"."id"  desc';




        $listCourses = $this->db->query($sql);

        return($listCourses);
    }

    public function getCourseTraining($idCourse) {

        $sql = 'SELECT  "C"."courseName", "B"."name" as "complejo", "P"."name" as "Area", "L"."name" as "Localidad","C"."type" as "Tipo", "Cat"."name" as "Categoria", "T"."delivery" as "Modalidad",  "T"."trainingStart", "T"."trainingEnd", "T"."quantityParticipants" as "NoAsistentes", "C"."duration" as "NoHrs", "T"."fk_instructor" as "capacitador", "T"."id" as "IDCourseTraining", "T"."listAsistants", "T"."teamName"   FROM "Courses" as "C" JOIN "TrainingRecords" as "T" ON "C"."courseCode" = "T"."fk_course"  LEFt JOIN "Complexes" "B" ON "T"."fk_complex" = "B"."id" LEFt JOIN "Processes" "P" ON "T"."fk_area" = "P"."id" LEFt JOIN "Locations" "L" ON "T"."fk_location" = "L"."id" LEFt JOIN "Category" "Cat" ON "T"."fk_category" = "Cat"."id"  where "T"."id"=' . $idCourse;



        $query = $this->db->query($sql);
        $listCourses = $query->result();
        return($listCourses);
    }

    public function getCatalog_Category($list) {

        $sql = 'select * from "Category" where "id" in(' . $list . ')   ORDER BY "name"';
        $query = $this->db->query($sql);
        $listCategory = $query->result_array();

        return($listCategory);
    }

    public function getCatalog_Instructor() {

        $sql = 'select distinct("fk_instructor") from "TrainingRecords" where "fk_instructor" is not null order by "fk_instructor"';
        $query = $this->db->query($sql);
        $listInstructor = $query->result_array();

        return($listInstructor);
    }

    public function getListParticipantsName($find, $list = "") {

        if ($list == "ALL") {
            $sql = 'select "id",UPPER(CONCAT("lastName",\' \', "secondName",\', \', "firstName")) as "nombrecompleto" from "Participants"   order by "lastName", "secondName","firstName"';
        } else {

            if ($list != "") {
                $sql = 'select "id",UPPER(CONCAT("lastName",\' \', "secondName",\', \', "firstName")) as "nombrecompleto" from "Participants" where "id" in(' . $list . ')  order by "lastName", "secondName","firstName"';
            } else {
                $sql = 'select "id",UPPER(CONCAT("lastName",\' \', "secondName",\', \', "firstName")) as "nombrecompleto" from "Participants" where "lastName" like \'%' . $find . '%\' OR "secondName" like \'%' . $find . '%\' OR "firstName" like \'%' . $find . '%\'  order by "lastName", "secondName","firstName"';
            }
        }
        //echo $sql;
        $query = $this->db->query($sql);

        $participantsList = $query->result_array();


        return($participantsList);
    }

}
