<?php

defined('BASEPATH') OR exit('No direct script access allowed');

define("TABLE", "Processes");
define("ELEMENT", "area");

require_once APPPATH . 'interfaces/CrudModelInterface.php';

class Admon_areas_model extends CI_Model implements CrudModelInterface {

    public function __construct() {
        parent::__construct();
    }

    public function get($id) {

        $this->db->where('id', $id);
        $query = $this->db->get(TABLE);
        return($query);
    }

    public function create($name) {

        //validar si existe

        $name_tmp = trim($name);

        $name = strtolower(trim($name));

        if ($name != "") {

            $sql = 'select * from "' . TABLE . '" where lower("name")=\'' . $name . '\'';

            $query = $this->db->query($sql);
            $qty = $query->num_rows();

            if ($qty > 0) {
                //echo "Ya existia este area";
                return "ya existia " . ELEMENT;
            } else {
                $this->db->set('name', $name_tmp);
                $this->db->insert(TABLE);



                if ($this->db->affected_rows() > 0) {
                    return "insertado exitosamente";
                } else {
                    return "error insertado";
                }
            }
        }
    }

    public function update($id, $name) {

        //Registrando log
        $this->db->set('action', "Edicion de Area");
        $this->db->set('fk_user', $this->session->userdata('id'));
        $this->db->insert('Imports');

        $fk_imports = $this->db->insert_id();

        $this->db->set('fk_import', $fk_imports);
        $this->db->set('name', $name);
        $this->db->where('id', $id);
        $this->db->update(TABLE);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id) {

        //Registrando log
        $this->db->set('action', "Eliminacion de Area");
        $this->db->set('fk_user', $this->session->userdata('id'));
        $this->db->insert('Imports');

        $fk_imports = $this->db->insert_id();

        $this->db->set('fk_import', $fk_imports);
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        $this->db->update(TABLE);

        //echo $this->db->last_query();

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getlist($find_course = "") {


        $sql = 'SELECT * FROM "' . TABLE . '" where "status"=1 order by "name"';
        $listAreas = $this->db->query($sql);

        return($listAreas);
    }

}
