<?php

defined('BASEPATH') OR exit('No direct script access allowed');

define("TABLE", "users");
define("ELEMENT", "localidad");

require_once APPPATH . 'interfaces/CrudModelInterface.php';

class Admon_users_model extends CI_Model implements CrudModelInterface {

    public function __construct() {
        parent::__construct();
    }

    public function get($id_person) {

        $this->db->where('id', $id_person);
        $query = $this->db->get("Participants");
        return($query);
    }

    public function create($name) {

        //validar si existe

        $name_tmp = trim($name);

        $name = strtolower(trim($name));

        if ($name != "") {

            $sql = 'select * from "' . TABLE . '" where lower("name")=\'' . $name . '\'';

            $query = $this->db->query($sql);
            $qty = $query->num_rows();

            if ($qty > 0) {

                return "ya existia " . ELEMENT;
            } else {
                $this->db->set('name', $name_tmp);
                $this->db->insert(TABLE);



                if ($this->db->affected_rows() > 0) {
                    return "insertado exitosamente";
                } else {
                    return "error insertado";
                }
            }
        }
    }

    public function update($id, $name) {
        $this->db->set('name', $name);
        $this->db->where('id', $id);
        $this->db->update(TABLE);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id) {
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        $this->db->update(TABLE);

        //echo $this->db->last_query();

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getlist($find_course = "") {


        $sql = 'select  "idParticipant", UPPER("firstName") as "firstName",UPPER("lastName") as "lastName", "worksite", "fk_perfil", "password" from "Participants" order by "lastName"';

        $listAreas = $this->db->query($sql);


        return($listAreas);
    }

    function Datatables($dt) {

        //Elements finding
        $columnd = $dt['col-display'];
        $count_c = count($columnd)-1; //Quitando la ultima columna "id". para que no lo refleje pero si lo tomaremos como $idUser


        //Preparing Query Find General
        $columns = implode('","', $dt['col-display']) . '", "Participants"."' . $dt['id-table'] . '"';
        $join = $dt['join'];
        $sql = "SELECT \"{$columns} FROM \"{$dt['table']}\" {$join}";

        $searchGeneral = $dt['search']['value'];

        /**
         * Search Generic
         */
        if ($searchGeneral != "") {
            $where = "";

            for ($i = 0; $i < $count_c; $i++) {

                $where .= ' UPPER("' . $columnd[$i] . '") LIKE \'%' . strtoupper($searchGeneral) . '%\' ';
                $where = $where . " OR";
            }
            $where = substr($where, 0, strlen($where) - 3);


            if ($where != '') {
                $sql .= " WHERE " . $where;
            }
        }



        //echo "sql:". $sql;
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();


        // sorting
        $sql .= " ORDER BY \"{$columnd[$dt['order'][0]['column']]}\" {$dt['order'][0]['dir']}";

        // limit
        //echo "longi:" . $dt['length'];
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$length} OFFSET {$start}";

        //echo "sql2:" . $sql;
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        //zecho "columnas:" . print_r($columnd);
        foreach ($list->result() as $row) {
            /**
             * custom gunakan
             * $option['data'][] = array(
             *                       $row->columnd[0],
             *                       $row->columnd[1],
             *                       $row->columnd[2],
             *                       $row->columnd[3],
             *                       $row->columnd[4],
             *                       .....
             *                     );
             */
            $rows = array();
            for ($i = 0; $i < $count_c; $i++) {
                $rows[] = strtoupper($row->{$columnd[$i]});
            }
            $idUser = $row->{$columnd[6]};
            $rows[] = '<button type="button" onClick="loadEditParticipant(\'' . $idUser . '\')" class="btn btn-xs btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal" data-target=".modal-nuevo"><i class="fa fa-pencil"></i> Detalles</button>
                <button type="button" onClick="deleteCourse(\'' . $idUser . '\')" class="btn btn-xs btn-danger waves-effect w-md waves-light m-b-15" id="sa-warning"><i class="fa fa-trash"></i> Eliminar</button>';
            ;

            $option['data'][] = $rows;
        }
        // eksekusi json
        echo json_encode($option);
    }

    public function getCatalog_Profiles() {

        $query = $this->db->query('select * from "Profiles"  ORDER BY "idProfiles"');
        $listProfiles = $query->result_array();

        return($listProfiles);
    }

    public function getListWorksitesGestorSpecific($idUser) {

        $sql = 'select "Worksites"."id","Worksites"."name", "fk_complex", "fk_area" from "Worksites" inner join "ProfilesGestorWorksites" ON ("Worksites"."id" = "fk_worksite") where "fk_participant" = (select "id" from "Participants" where "id"=\'' . $idUser . '\')';

        //echo $sql;
        $query = $this->db->query($sql);
        $worksitesGestorList = $query->result_array();


        return($worksitesGestorList);
    }

    public function getListWorksites() {

        $sql = 'select "Worksites"."id","Worksites"."name" from "Worksites" where  "status"=1 order by "name"  ';

        $query = $this->db->query($sql);
        $worksitesList = $query->result_array();


        return($worksitesList);
    }

}
