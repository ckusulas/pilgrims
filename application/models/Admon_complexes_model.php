<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'interfaces/CrudModelInterface.php';

class Admon_complexes_model extends CI_Model implements CrudModelInterface {

    public function __construct() {
        parent::__construct();
    }

    public function get($id) {

        $this->db->where('id', $id);
        $query = $this->db->get('Complexes');
        return($query);
    }

    public function create($name) {

        //validar si existe

        $name_tmp = trim($name);

        $name = strtolower(trim($name));

        if ($name != "") {

            $sql = 'select * from "Complexes" where lower("name")=\'' . $name . '\'';

            $query = $this->db->query($sql);
            $qty = $query->num_rows();

            if ($qty > 0) {
                return "ya existia complejo";
                
            } else {
                $this->db->set('name', $name_tmp);
                $this->db->insert('Complexes');



                if ($this->db->affected_rows() > 0) {
                    return "insertado exitosamente";
                } else {
                    return "error insertado";
                }
            }
        }
    }

    public function update($id, $name) {
        //Registrando log
        $this->db->set('action', "Edicion de Complejo");
        $this->db->set('fk_user', $this->session->userdata('id'));
        $this->db->insert('Imports');

        $fk_imports = $this->db->insert_id();

        $this->db->set('fk_import', $fk_imports);
        $this->db->set('name', $name);
        $this->db->where('id', $id);
        $this->db->update('Complexes');

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id) {


        //Registrando log
        $this->db->set('action', "Eliminacion de Complejo");
        $this->db->set('fk_user', $this->session->userdata('id'));
        $this->db->insert('Imports');

        $fk_imports = $this->db->insert_id();


        $this->db->set('fk_import', $fk_imports);
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        $this->db->update('Complexes');

        //echo $this->db->last_query();

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getlist($find_course = "") {



        $sql = 'SELECT * FROM "Complexes" where "status"=1 order by "name"';
        $listComplexes = $this->db->query($sql);

        return($listComplexes);
    }

}
