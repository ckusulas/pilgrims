<tbody>
<?php

foreach($dataTables as $row){

 ?>
      <tr>
          <td><?=$row['id']?></td>
          <td><?=$row['name']?></td>
          <td>
              <button type="button" class="btn btn-xs btn-primary waves-effect w-md waves-light m-b-15"  onClick='loadElement(<?php echo $row['id'] ?>)' data-toggle="modal" data-target=".modal-nuevo"><i class="fa fa-pencil"></i> Detalles</button>
              <button type="button" class="btn btn-xs btn-danger waves-effect w-md waves-light m-b-15"  onClick='deleteElement(<?php echo $row['id'] ?>)' id="sa-warningxxx"><i class="fa fa-trash"></i> Eliminar</button>
          </td>
      </tr>
<?php
 }

?>
</tbody>
