<tbody>
<?php

foreach($dataTables as $row){

 

 ?>
      <tr>
          <td><?=$row['id']?></td>
          <td><?=$row['courseName']?></td>
          <td><?=$row['complejo']?></td>
          <td><?=$row['area']?></td>
          <td>Sub-area</td>
          <td><?=$row['localidad']?></td>
          <td><?=$row['tipo']?></td>
          <td><?=$row['categoria']?></td>
          <td><?=$row['modalidad']?></td>
          <td><?=$row['trainingStart']?></td>
          <td><?=$row['trainingEnd']?></td>
          <td><?=$row['noAsistentes']?></td>
          <td><?=$row['duration']?></td>
          <td><?=$row['capacitador']?></td>
          <td><?=$row['atachment']?></td>
          <td>
              <button type="button" class="btn btn-xs btn-primary waves-effect w-md waves-light m-b-15"  onClick='loadEditCourse(<?=$row['idCourse']?>)' data-toggle="modal" data-target=".modal-nuevo"><i class="fa fa-pencil"></i> Detalles</button>

              
              <?php 
                if ($row['noAsistentes']>0) {?>
                  <button type="button" class="btn btn-xs btn-success waves-effect w-md waves-light m-b-15"  onClick='evaluationCourse(<?=$row['idCourse']?>)' data-toggle="modal" data-target=".modal-evaluation" ><i class="fa fa-list-ul"></i> Calificacion</button>
              <?php
                }
                else{
              ?>
                   <button type="button" class="btn btn-xs btn-sample waves-effect w-md waves-light m-b-15"  onClick='evaluationCourse(<?=$row['idCourse']?>)'  " ><i class="fa fa-list-ul"></i> Calificacion</button>

              <?php
              }
              ?>

              
               <button type="button" class="btn btn-xs btn-danger waves-effect w-md waves-light m-b-15"  onClick='deleteCourse(<?=$row['idCourse']?>)'  ><i class="fa fa-trash"></i> Eliminar</button>


          </td>
      </tr>
<?php
 }

?>
</tbody>
