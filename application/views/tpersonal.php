<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

        <title>Universidad Pilgrims | Tablero Personal</title>

        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/menu.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css">

        <!-- Plugin CSS -->
        <link href="../plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="../plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <link href="../assets/css/pilgrims.css" rel="stylesheet" type="text/css">

        <script src="../assets/js/modernizr.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <?php $this->view('subviewLogo.php'); ?>
              

                <!-- Navbar -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->view('subviewLeft.php'); ?>
            <!-- Left Sidebar End --> 



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <ol class="breadcrumb pull-right">
                                        <li><a href="#">Tableros</a></li>
                                        <li class="active">Personal</li>
                                    </ol>
                                    <h4 class="page-title">Bienvenida(o)</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-20">
                                                <form action="#">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2 m-b-10">Filtro de fecha</label>
                                                        <div class="col-sm-10 m-b-10">
                                                            <div class="input-daterange input-group" id="date-range">
                                                                <span class="input-group-addon bg-primary b-0 text-white">De</span>
                                                                <input type="text" class="form-control" name="start" />
                                                                <span class="input-group-addon bg-primary b-0 text-white">al</span>
                                                                <input type="text" class="form-control" name="end" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Selección de participante</label>
                                                        <div class="col-sm-3">
                                                            <select id="catalogWorksites" class="form-control">
                                                                <option>Seleccione Worksite</option>
                                                                <option>Todos</option>
                                                                <?php
                                                                foreach ($catalogWorksites as $row) {
                                                                    $selected = "";

                                                                    /* if ($s_worksite == $row['name']) {
                                                                      $selected = " selected";
                                                                      } */
                                                                    echo "<option value='" . $row['worksite'] . "' $selected>" . $row['worksite'] . "</option>\n";
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <select  id="catalogLocations"  class="form-control">
                                                                <option>Seleccione Localidad</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <select  id="catalogParticipant"  class="form-control">
                                                                <option>Seleccione Participante</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-sm-12 m-t-15">
                                                            <button type="button" class="btn btn-xs btn-block waves-effect waves-light btn-primary"><i class="fa fa-eye"></i> Consultar indicadores de la selección</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4">
                                <div id="profile" style="display: none" class="text-center card-box">
                                    <div class="member-card">
                                        <div class="thumb-xl member-thumb m-b-10 center-block">
                                            <img src="../assets/images/users/avatar-1.jpg" class="img-circle img-thumbnail" alt="profile-image">
                                        </div>

                                        <div class="row">
                                            <div class="m-b-5" id="nameUser">Nombre y Apellido</div>
                                            <p id="userID" class="text-muted">ID: </p>
                                            <p id="qtyHours" class="text-muted">Horas cursadas: <b>530</b></p>
                                            <div class="col-sm-6">
                                                <div id="labelCompleted"><h2 class="naranja"></h2></div>
                                                <p>Cursos terminados</p>
                                            </div>
                                            <div class="col-sm-6">
                                                <div id="labelProgress"><h2 class="naranja"></h2></div>
                                                <p>Cursos no terminados</p>
                                            </div>
                                        </div>

                                    </div>

                                </div> <!-- end card-box -->

                            </div> <!-- end col -->

                            <div class="col-lg-9 col-md-8">
                                <!--Widget de Curso-->
                                <div id="listCourses"></div>





                            </div>

                        </div>
                        <!-- end row -->

                    </div>
                    <!-- end container -->
                </div>
                <!-- end content -->

                <footer class="footer text-right">
                    2017 © Pilgrim
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->



        <script>
            var resizefunc = [];



        </script>

        <!-- Plugins  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/wow.min.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../plugins/switchery/switchery.min.js"></script>

        <script src="../plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

        <!-- peity charts -->
        <script src="../plugins/peity/jquery.peity.min.js"></script>
        <script src="../assets/pages/jquery.peity.init.js"></script>

        <!-- DatePicker -->
        <script src="../plugins/moment/moment.js"></script>
        <script src="../plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

        <!-- Page js  -->
        <script src="../assets/pages/jquery.dashboard.js"></script>

        <!-- Custom main Js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>


        <script type="text/javascript">

            $(document).ready(function () {

                $("#catalogWorksites").change(function () {
                    var worksiteSelected = $("#catalogWorksites option:selected").text();
                    //$("#profile").hide("slow");
                    // $("#listCourses").hide("slow");

                    populate_location(worksiteSelected);

                });

                function populate_location(findWorksite) {
                    var cantElement = 0;
                    $('#catalogLocations').empty();
                    $('#catalogLocations').append("<option>Loading ....</option>");
                    $.ajax({
                        type: "GET",
                        url: "<?php echo site_url('tablero_personal/populate_locations') ?>/" + findWorksite,
                        contentType: "application/json;charset=utf-8",
                        dataType: 'json',
                        success: function (data) {
                            $('#catalogLocations').empty();
                            $('#catalogLocations').append("<option>Todos</option>");


                            $.each(data, function (i, name) {
                                $('#catalogLocations').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                                cantElement++;
                            });

                            if (cantElement == 0) {
                                $('#catalogLocations').append('<option value="' + findWorksite + '">Otros</option>');
                            }




                        }
                    });
                }


                $("#catalogLocations").change(function () {
                    var locationSelected = $("#catalogLocations option:selected").text();
                    var locationVal = $("#catalogLocations option:selected").val();
                    populate_participant(locationSelected, locationVal);

                });

                function populate_participant(findLocation, valueFind) {
                    $('#catalogParticipant').empty();
                    $('#catalogParticipant').append("<option>Loading ....</option>");
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('tablero_personal/populate_participants') ?>/" + findLocation + "/" + valueFind,
                        contentType: "application/json;charset=utf-8",
                        dataType: 'json',
                        success: function (data) {
                            $('#catalogParticipant').empty();
                            $('#catalogParticipant').append("<option>Todos</option>");
                            $.each(data, function (i, name) {
                                $('#catalogParticipant').append('<option value="' + data[i].userID + '">' + data[i].namecomplete + '</option>');
                            });
                        }
                    });
                }




                $("#catalogParticipant").change(function () {
                    $("#profile").fadeOut();
                    $("#listCourses").fadeOut();

                    var participantSelected = $("#catalogParticipant option:selected").val();
                    //populate_participant(locationSelected);
                    //$("#profile").fadeOut();
                    getInfoParticipant(participantSelected);
                    /*$("#nameUser").html("<h4 class='m-b-5' id='nameUser'>Constantino Kusulas</h4>");
                     $("#profile").fadeIn(2000);*/

                });



                function getInfoParticipant(idParticipant) {

                    var cadCurso = "";
                    // e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('tablero_personal/getHoursParticipant') ?>/" + idParticipant,
                        contentType: "application/json;charset=utf-8",
                        dataType: 'json',
                        success: function (data) {

                            $("#profile").fadeIn(500, function () {

                                $("#nameUser").html("<h4 class='m-b-5' id='nameUser'>" + data.nameParticipant + "</h4>");
                                $("#userID").html("<p id='userID' class='text-muted'>ID: <b>" + data.IDparticipant + "</b></p>");
                                $("#qtyHours").html("<p id='qtyHours' class='text-muted'>Horas cursadas: <b>" + data.qtyHours + "</b></p>");
                                $("#labelCompleted").html("<div id='labelCompleted'><h2 class='naranja'>" + data.qtyCompleted + "</h2></div>");
                                $("#labelProgress").html("<div id='labelProgress'><h2 class='naranja'>" + data.qtyProgress + "</h2></div>");

                                $("#listCourses").html("<div id='listCourses'></div>");
                                $("#profile").show("slow");


                                $.each(data.listCourses, function (i, listCourses) {
                                    // $('#listCourses').append('<input type="text" id="star" value="'+listCourses.courseName+'" />');

                                    cadCurso = "<div class='col-lg-6 col-md-6'>";
                                    cadCurso += "          <div class='card-box'>";
                                    cadCurso += "               <div class='row'>";
                                    cadCurso += "                   <div class='col-lg-12'>";
                                    cadCurso += "                        <h4>" + listCourses.courseName + "</h4>";
                                    cadCurso += "                       <div class='col-md-6'>";
                                    if (listCourses.status == "Complete") {
                                        cadCurso += "                           <h5 class='text-success m-t-0 m-b-0'><i class='md md-school'></i> Complete</h5>";
                                    } else {
                                        cadCurso += "                           <h5 class='text-warning m-t-0 m-b-0'><i class='md md-school'></i> Cursando</h5>";

                                    }
                                    cadCurso += "                       </div>";
                                    cadCurso += "                       <div class='col-md-6 text-right'>";
                                    cadCurso += "                           <button type='button' class='btn btn-xs btn-block waves-effect waves-light btn-primary'><i class='fa fa-file-text'></i> Imprimir Diploma</button>";
                                    cadCurso += "                       </div>";
                                    cadCurso += "                      <div class='col-md-6 text-center m-t-15'>";
                                    //cadCurso += "                        <div id='graph-" + listCourses.trainingID + "'></div><br>";
                                    if (listCourses.status != "Complete") {
                                        cadCurso += "<i class='fa fa-star text-warning fa-2x'></i><i class='fa fa-star text-warning fa-2x'></i><i class='fa fa-star-half-o text-warning fa-2x'></i><i class='fa fa-star-o text-warning fa-2x'></i><i class='fa fa-star-o text-warning fa-2x'></i><br>";
                                    } else {
                                        cadCurso += "<i class='fa fa-star text-warning fa-2x'></i><i class='fa fa-star text-warning fa-2x'></i><i class='fa fa-star text-warning fa-2x'></i><i class='fa fa-star text-warning fa-2x'></i><i class='fa fa-star text-warning fa-2x'></i>";
                                    }
                                    if (listCourses.status == "Complete") {
                                        cadCurso += "                       <h4>Avance: <b class='naranja'>100%</b></h4>";
                                    } else {
                                        cadCurso += "                       <h4>Avance: <b class='naranja'>50%</b></h4>";
                                    }
                                    cadCurso += "                 </div>";
                                    cadCurso += "                <div class='col-md-6 text-center m-t-15'>";
                                    if (parseInt(listCourses.score) == "100") {
                                        cadCurso += "               <i class='fa fa-trophy text-warning fa-2x'></i><br>";
                                    }
                                    if (parseInt(listCourses.score) <= "80") {
                                        cadCurso += "               <i class='fa fa-flag  text-danger fa-2x'></i><br>";
                                    }
                                    if (listCourses.score >= "81" && listCourses.score <= "99") {
                                        cadCurso += "               <i class='fa fa-check text-success fa-2x'></i><br>";
                                    }
                                    cadCurso += "           <h4>Calificación: <b class='naranja'>" + listCourses.score + "</b></h4>";
                                    cadCurso += "    </div>";
                                    cadCurso += "   <div class='col-md-12 text-right m-t-10'>";
                                    cadCurso += "   <h5 class=' m-t-0 m-b-0'><i class='md md-group'></i> Nombre del grupo</h5>";
                                    cadCurso += "  </div>";
                                    cadCurso += " </div>";
                                    cadCurso += "  </div>";
                                    cadCurso += " </div>";
                                    cadCurso += "  </div>";






                                    $('#listCourses').append(cadCurso);
                                    // $('#graph-'+listCourses.trainingID).html("<span data-plugin='peity-donut-alt' data-peity='{ 'fill': ['#084073', '#ebeff2'],  'innerRadius': 18, 'radius': 28 }'>30/100</span><br>");

                                    $("#listCourses").fadeIn();
                                });






                            }


                            );



                        }
                    });
                }

            })

 




            // Date Picker
            jQuery('#datepicker').datepicker();
            jQuery('#datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            jQuery('#datepicker-inline').datepicker();
            jQuery('#datepicker-multiple-date').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: "mm/dd/yyyy",
                clearBtn: true,
                multidate: true,
                multidateSeparator: ","
            });
            jQuery('#date-range').datepicker({
                toggleActive: true
            });

            //Date range picker
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-default',
                cancelClass: 'btn-primary'
            });
            $('.input-daterange-timepicker').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mm A',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-default',
                cancelClass: 'btn-primary'
            });
            $('.input-limit-datepicker').daterangepicker({
                format: 'MM/DD/YYYY',
                minDate: '06/01/2016',
                maxDate: '06/30/2016',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-default',
                cancelClass: 'btn-primary',
                dateLimit: {
                    days: 6
                }
            });

            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#reportrange').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2016',
                maxDate: '12/31/2016',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });

        </script>

    </body>
</html>