<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="Tablero principal" content="Tablero principal de indicadores">
        <meta name="BUSEM" content="Tablero Pilgrims">

        <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

        <title>Universidad Pilgrims | Tablero Principal</title>

        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/menu.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css">

        <!-- Plugin CSS -->
        <link href="../plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="../plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <link href="../assets/css/pilgrims.css" rel="stylesheet" type="text/css">

        <script src="../assets/js/modernizr.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->


    </head>


    <body class="fixed-left">
        <!--Highcharts-->
        <script src="../plugins/jquery/js/jquery-3.1.1.min.js"></script>
        <script src="../plugins/highcharts/code/highcharts.js"></script>
        <script src="../plugins/highcharts/code/highcharts-3d.js"></script>
        <script src="../plugins/highcharts/code/modules/exporting.js"></script>
        <!--Highcharts END-->

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                 <?php $this->view('subviewLogo.php'); ?>
              
                <!-- Navbar -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->view('subviewLeft.php'); ?>
            <!-- Left Sidebar End --> 



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <ol class="breadcrumb pull-right">
                                        <li><a href="#">Tableros</a></li>
                                        <li class="active">Principal</li>
                                    </ol>
                                    <h4 class="page-title">Bienvenida(o)</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="p-20">
                                                <form id="formProcess"    method="post" action="tablero_principal">
                                                    <div class="form-group">
                                                        <div class="col-md-4 m-b-10">
                                                            <select id="catalogWorksites" name="catalogWorksites" class="form-control" onChange="selectWorksite()">
                                                                <option>Seleccione Worksite</option>
                                                                <option>Todos</option>
                                                                <?php
                                                                foreach ($catalogWorksites as $row) {
                                                                    $selected = "";

                                                                    if ($s_worksite == $row['name']) {
                                                                        $selected = " selected";
                                                                    }
                                                                    echo "<option value='" . $row['id'] . "' $selected>" . $row['name'] . "</option>\n";
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-8 m-b-10">
                                                            <div class="col-md-3"><label>Cursos que hayan finalizado:</label></div>
                                                            <div class="col-md-9">
                                                                <div class="input-daterange input-group" id="date-range">
                                                                    <span class="input-group-addon bg-primary b-0 text-white">Entre</span>
                                                                    <input type="text" id="start_date" class="form-control" name="start" value="<?= $s_finicial ?>"/>
                                                                    <span class="input-group-addon bg-primary b-0 text-white">y</span>
                                                                    <input type="text" id="end_date" class="form-control" name="end" value="<?= $s_ffinal ?>" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-1">
                                                            <b>Filtro Avanzado</b>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select id="catalogComplexes" class="form-control col-md-2" onChange="selectFilterAdvanced()">
                                                                <option>Seleccione Complejo</option>
                                                                <option>Todas</option>
                                                                <?php
                                                                foreach ($catalogComplexes as $row) {
                                                                    $selected = "";

                                                                    if ($s_complex == $row['name']) {
                                                                        $selected = " selected";
                                                                    }

                                                                    echo "<option value='" . $row['id'] . "' $selected>" . $row['name'] . "</option>\n";
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select id="catalogProcesses" class="form-control col-md-2" onChange="selectFilterAdvanced()" >
                                                                <option>Seleccione Area</option>
                                                                <option>Todas</option>
<?php
foreach ($catalogProcesses as $row) {
    $selected = "";

    if ($s_process == $row['name']) {
        $selected = " selected";
    }

    echo "<option value='" . $row['id'] . "' $selected>" . $row['name'] . "</option>\n";
}
?>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <select id="catalogLocations"  class="form-control" onChange="selectFilterAdvanced()">
                                                                <option>Seleccione Localidad</option>
                                                                <option>Todas</option>
<?php
foreach ($catalogLocations as $row) {
    $selected = "";

    if ($s_location == $row['name']) {
        $selected = " selected";
    }
    echo "<option value='" . $row['id'] . "' $selected>" . $row['name'] . "</option>\n";
}
?>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button type="button" class="btn btn-block waves-effect waves-light btn-primary" onclick="resetForm()"><i class="fa fa-rotate-left"></i> Resetear</button>
                                                        </div>
                                                        <div class="col-sm-12 m-t-15">
                                                            <button type="button" class="btn btn-xs btn-block waves-effect waves-light btn-primary" onclick="processForm()"><i class="fa fa-eye"></i> Consultar indicadores de la selección</button>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" id="f_worksite" name="f_worksite">
                                                    <input type="hidden" id="f_complex" name="f_complex">
                                                    <input type="hidden" id="f_process" name="f_process">
                                                    <input type="hidden" id="f_location" name="f_location">
                                                    <input type="hidden" id="f_finitial" name="f_finitial">
                                                    <input type="hidden" id="f_ffinal" name="f_ffinal">
                                                    <input type="hidden" id="f_submited" value="" name="f_submited">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="col-lg-12 col-md-12">
                                    <div class="card-box">
                                        <h4 class="text-dark  header-title m-t-0 m-b-30">Cursos Internos</h4>
                                        <div class="row">
                                            <div class="col-lg-4 text-center">
                                                <h2><i class="md md-timelapse naranja"></i></h2>
                                                <div class="wid-icon-info">
                                                    <h2 class="m-t-0 m-b-5 counter naranja"><?= $hours_int ?></h2>
                                                    <p class="text-muted m-b-5 font-13 text-uppercase">Horas</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 text-center">
                                                <h2><i class="md md-people naranja"></i></h2>
                                                <div class="wid-icon-info">
                                                    <h2 class="m-t-0 m-b-5 counter naranja"><?= $capacitation_int ?></h2>
                                                    <p class="text-muted m-b-5 font-13 text-uppercase">Personas Cap.</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 text-center">
                                                <h2><i class="md md-book naranja"></i></h2>
                                                <div class="wid-icon-info">
                                                    <h2 class="m-t-0 m-b-5 counter naranja"><?= $courses_int ?></h2>
                                                    <p class="text-muted m-b-5 font-13 text-uppercase">Cursos</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-center">
                                                <h2><i class="md md-chat naranja"></i></h2>
                                                <div class="wid-icon-info">
                                                    <h2 class="m-t-0 m-b-5 counter naranja"><?= $english_hours_int ?></h2>
                                                    <p class="text-muted m-b-5 font-13 text-uppercase">Horas en Inglés</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-center">
                                                <h2><i class="md md-people naranja"></i></h2>
                                                <div class="wid-icon-info">
                                                    <h2 class="m-t-0 m-b-5 counter naranja"><?= $english_int ?></h2>
                                                    <p class="text-muted m-b-5 font-13 text-uppercase">Personas en Inglés</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="text-dark  header-title m-t-0 m-b-30">Horas Impartidas - Cursos Internos</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="grafica1" style="min-width: 100%; height: 500px; max-width: 100%; margin: 0 auto">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="text-dark  header-title m-t-0 m-b-30">Personas capacitadas - Cursos Internos</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="grafica3" style="min-width: 100%; height: 500px; max-width: 100%; margin: 0 auto">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="text-dark  header-title m-t-0 m-b-30">Cursos Impartidos - Cursos Internos</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="grafica5" style="min-width: 100%; height: 500px; max-width: 100%; margin: 0 auto">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="text-dark  header-title m-t-0 m-b-30">Horas de Inglés - Cursos Internos</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="grafica7" style="min-width: 100%; height: 500px; max-width: 100%; margin: 0 auto">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="col-lg-12 col-md-12">
                                    <div class="card-box">
                                        <h4 class="text-dark  header-title m-t-0 m-b-30">Cursos Externos</h4>
                                        <div class="row">
                                            <div class="col-lg-6 text-center">
                                                <h2><i class="md md-timelapse naranja"></i></h2>
                                                <div class="wid-icon-info">
                                                    <h2 class="m-t-0 m-b-5 counter naranja"><?= $hours_ext ?></h2>
                                                    <p class="text-muted m-b-5 font-13 text-uppercase">Horas</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-center">
                                                <h2><i class="md md-people naranja"></i></h2>
                                                <div class="wid-icon-info">
                                                    <h2 class="m-t-0 m-b-5 counter naranja"><?= $capacitation_ext ?></h2>
                                                    <p class="text-muted m-b-5 font-13 text-uppercase">Personas Cap.</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 text-center">
                                                <h2><i class="md md-book naranja"></i></h2>
                                                <div class="wid-icon-info">
                                                    <h2 class="m-t-0 m-b-5 counter naranja"><?= $courses_ext ?></h2>
                                                    <p class="text-muted m-b-5 font-13 text-uppercase">Cursos</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="text-dark  header-title m-t-0 m-b-30">Horas Impartidas - Cursos Externos</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="grafica2" style="min-width: 100%; height: 500px; max-width: 100%; margin: 0 auto">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="text-dark  header-title m-t-0 m-b-30">Personas capacitadas - Cursos Externos</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="grafica4" style="min-width: 100%; height: 500px; max-width: 100%; margin: 0 auto">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="text-dark  header-title m-t-0 m-b-30">Cursos Impartidos - Cursos Externos</h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="grafica6" style="min-width: 100%; height: 500px; max-width: 100%; margin: 0 auto">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 text-center">
                                    <img src="../assets/images/logo.png" style="width: 100%;">
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div>
                    <!-- end container -->
                </div>
                <!-- end content -->

                <footer class="footer text-right">
                    2017 © Pilgrim
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <!--Grafica1-->
        <script type="text/javascript">



            Highcharts.chart('grafica1', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y} equivalente a {point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            distance: 10,
                            style: {
                                fontWeight: 'bold',
                                fontSize: 12
                            },
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                        type: 'pie',
                        name: 'Número de cursos',
                        data: [
                            ['UP', <?= $qty_HoursUP ?>],
                            {
                                name: 'Alchemy',
                                y: <?= $qty_HoursAlchemy ?>,
                                sliced: true,
                                selected: true
                            }
                        ]
                    }]
            });
        </script>

        <!--Grafica2-->
        <script type="text/javascript">

            Highcharts.chart('grafica2', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y} equivalente a {point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            distance: 10,
                            style: {
                                fontWeight: 'bold',
                                fontSize: 12
                            },
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                        type: 'pie',
                        name: 'Número de cursos',
                        data: [
                            {
                                name: 'Otros',
                                y: <?= $qty_HoursOthersExt ?>,
                                sliced: true,
                                selected: true
                            }
                        ]
                    }]
            });
        </script>

        <!--Grafica3-->
        <script type="text/javascript">

            Highcharts.chart('grafica3', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y} equivalente a {point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            distance: 10,
                            style: {
                                fontWeight: 'bold',
                                fontSize: 12
                            },
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                        type: 'pie',
                        name: 'Número de personas capacitadas',
                        data: [
                            ['UP', <?= $qty_ParticipantUP ?>],
                            {
                                name: 'Alchemy',
                                y: <?= $qty_ParticipantAlchemy ?>,
                                sliced: true,
                                selected: true
                            }
                        ]
                    }]
            });
        </script>

        <!--Grafica4-->
        <script type="text/javascript">

            Highcharts.chart('grafica4', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y} equivalente a {point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            distance: 10,
                            style: {
                                fontWeight: 'bold',
                                fontSize: 12
                            },
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                        type: 'pie',
                        name: 'Número de cursos',
                        data: [
                            {
                                name: 'Otros',
                                y: <?= $qty_ParticipantOthersExt ?>,
                                sliced: true,
                                selected: true
                            }
                        ]
                    }]
            });
        </script>

        <!--Grafica5-->
        <script type="text/javascript">

            Highcharts.chart('grafica5', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y} equivalente a {point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            distance: 10,
                            style: {
                                fontWeight: 'bold',
                                fontSize: 12
                            },
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                        type: 'pie',
                        name: 'Número de cursos',
                        data: [
                            ['UP', <?= $qty_CourseUP ?>],
                            {
                                name: 'Alchemy',
                                y: <?= $qty_CourseAlchemy ?>,
                                sliced: true,
                                selected: true
                            }
                        ]
                    }]
            });
        </script>

        <!--Grafica6-->
        <script type="text/javascript">

            Highcharts.chart('grafica6', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y} equivalente a {point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            distance: 10,
                            style: {
                                fontWeight: 'bold',
                                fontSize: 12
                            },
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                        type: 'pie',
                        name: 'Número de cursos',
                        data: [
                            {
                                name: 'Otros',
                                y: <?= $qty_CourseOthersExt ?>,
                                sliced: true,
                                selected: true
                            }
                        ]
                    }]
            });
        </script>

        <!--Grafica7-->
        <script type="text/javascript">

            Highcharts.chart('grafica7', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y} equivalente a {point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            distance: 10,
                            style: {
                                fontWeight: 'bold',
                                fontSize: 12
                            },
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                        type: 'pie',
                        name: 'Número de Horas',
                        data: [
                            {
                                name: 'Horas de Inlgés',
                                y: <?= $english_hours_int ?>,
                                sliced: true,
                                selected: true
                            }
                        ]
                    }]
            });
        </script>

        <script>
            var resizefunc = [];
        </script>

        <script type="text/javascript">



            function selectWorksite(){

                $('#catalogComplexes').val('Seleccione Complejo');
                $('#catalogLocations').val('Seleccione Localidad');
                $('#catalogProcesses').val('Seleccione Area');
                
                $("#catalogComplexes").attr('disabled', 'disabled');
                $("#catalogLocations").attr('disabled', 'disabled');
                $("#catalogProcesses").attr('disabled', 'disabled');

                 
                
                                                             
            }


            function selectFilterAdvanced(){

                $('#catalogWorksites').val('Seleccione Worksite');
                $("#catalogWorksites").attr('disabled', 'disabled');
                  
                                                             
            }

            

      

            function resetForm() { 

                $("#catalogComplexes").removeAttr('disabled');
                $("#catalogLocations").removeAttr('disabled');
                $("#catalogProcesses").removeAttr('disabled');
                $("#catalogWorksites").removeAttr('disabled');
                
 
                $("#catalogComplexes")[0].selectedIndex = 0;
                $("#catalogLocations")[0].selectedIndex = 0;
                $("#catalogProcesses")[0].selectedIndex = 0;
                $("#catalogWorksites")[0].selectedIndex = 0;

                //$("#formProcess")[0].reset();
            }

            function processForm() {


                worksiteSelected = $("#catalogWorksites option:selected").text();
                complexSelected = $("#catalogComplexes option:selected").text();
                processSelected = $("#catalogProcesses option:selected").text();
                locationsSelected = $("#catalogLocations option:selected").text();
                finitial = $("#start_date").val();
                ffinal = $("#end_date").val();

                $("#f_worksite").val(worksiteSelected);
                $("#f_complex").val(complexSelected);
                $("#f_process").val(processSelected);
                $("#f_location").val(locationsSelected);
                $("#f_finitial").val(finitial);
                $("#f_ffinal").val(ffinal);
                $("#f_submited").val(true);


                $("#formProcess").submit();

                /*
                 
                 $.ajax({
                 url : "tprincipal/index",
                 type : "POST",
                 dataType : "json",
                 data : {"s_worksite" : worksiteSelected, "s_complex" : complexSelected, "s_process" : processSelected, "s_location" : locationsSelected,"finicial" : finitial, "ffinal" : ffinal },
                 success : function(data) {
                 //alert("todo bien");
                 },
                 error : function(data) {
                 //  alert("todo mal");
                 }
                 });*/



            }
        </script>

        <!-- Plugins  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/wow.min.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../plugins/switchery/switchery.min.js"></script>

        <script src="../plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

        <!-- DatePicker -->
        <script src="../plugins/moment/moment.js"></script>
        <script src="../plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

        <!-- Page js  -->
        <script src="../assets/pages/jquery.dashboard.js"></script>

        <!-- Custom main Js -->
        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>


        <script type="text/javascript">
            // Date Picker
            jQuery('#datepicker').datepicker();
            jQuery('#datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            jQuery('#datepicker-inline').datepicker();
            jQuery('#datepicker-multiple-date').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: "mm/dd/yyyy",
                clearBtn: true,
                multidate: true,
                multidateSeparator: ","
            });
            jQuery('#date-range').datepicker({
                toggleActive: true
            });

            //Date range picker
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-default',
                cancelClass: 'btn-primary'
            });
            $('.input-daterange-timepicker').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mm A',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-default',
                cancelClass: 'btn-primary'
            });
            $('.input-limit-datepicker').daterangepicker({
                format: 'MM/DD/YYYY',
                minDate: '06/01/2016',
                maxDate: '06/30/2016',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-default',
                cancelClass: 'btn-primary',
                dateLimit: {
                    days: 6
                }
            });

            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#reportrange').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2016',
                maxDate: '12/31/2016',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-success',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });

        </script>

    </body>
</html>
