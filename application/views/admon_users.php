<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

        <title>Universidad Pilgrims | Administracion | <?=$subTitle?></title>

        <!-- DataTables -->
        <link href="../plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- Sweet Alert css -->
        <link href="../plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />


        <link href="../plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/menu.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css">

        <link href="../assets/css/pilgrims.css" rel="stylesheet" type="text/css">

        <script src="../assets/js/modernizr.min.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <?php $this->view('subviewLogo.php'); ?>
              

                <!-- Navbar -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->view('subviewLeft.php'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <ol class="breadcrumb pull-right">
                                        <li><a href="#">Administración</a></li>
                                        <li class="active"><?=$subTitle?></li>
                                    </ol>
                                    <h4 class="page-title">Bienvenida(o)</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                 
                                <div class="panel panel-color panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Lista de Usuarios </h3>
                                        <p class="panel-sub-title font-13 text-muted">Administración de Usuarios del Sistema</p>
                                    </div>
                             
                                     <div class="panel-body">
                                       <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                          <thead>
                                              <tr>
                                                    <th>matricula</th>
                                                    <th>Nombre</th>                                                    
                                                    <th>Apellidos</th>                                                                               
                                                    <th>Worksite</th>   
                                                    <th>Localidad</th>                                                        
                                                    <th>Perfil</th>     
                                                    <th>Acciones</th>     
                                              </tr>
                                          </thead>
                                          <tfoot>
                                              <tr>
                                               
                                              </tr>
                                          </tfoot>
                                    </table>
                                </div>
                                </div>
                            </div>

                        </div>
                        <!-- end row -->

                    </div>
                    <!-- end container -->
                </div>
                <!-- end content -->

                <footer class="footer text-right">
                    2017 © Pilgrim
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <!--  Modal nuevo-->
        <div id="modalNew" class="modal fade modal-nuevo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">Detalles</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form  action="#" id="form_course" class="form-horizontal"   >
 
                                <div class="form-group" id="displayId">
                                    <label class="col-md-2 control-label">Nombre Completo:</label>
                                    <div class="col-md-6">
                                        <input id="idNombre" name="idNombre" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Matricula:</label>
                                   <div class="col-md-6">
                                        <input  type="text" id="matriculaTXT" class="form-control"  >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Password:</label>
                                   <div class="col-md-6">
                                        <input id="passwordTXT" name="passwordTXT" type="text" class="form-control"  >
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Worksite:</label>
                                   <div class="col-md-6">
                                        <input id="worksiteTXT" name="worksiteTXT" type="text" class="form-control"  >
                                     </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo de Perfil:</label>

                                     <div class="col-md-6">
                                    <div id="selectProfile" name="selectProfile"  >

                                    </div>
                                    </div>
                                </div>

                                 <div class="form-group" id="displayListW">
                                    <label class="col-md-2 control-label">Workistes a Gestionar:</label>
                                    <div class="col-md-6">
                                        <div id="div_worksiteLists"></div>
                                    </div>
                                </div>
                                <input type="hidden" id="idUser" name="idUser">
                                <input type="hidden" id="matricula" name="matricula">
                                <input type="hidden" id="profiSelect" name="profiSelect">
                                 
                                <div class="form-group">
                                 
                            </form>
                           
                            <div align="right" class="col-md-12">
                                <button type="button"  onClick="saveElement()" class="btn btn-success waves-effect waves-light">Guardar</button>
                            </div>

                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
   



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>




        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/wow.min.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../plugins/switchery/switchery.min.js"></script>

        <!-- Sweet Alert js -->
        <script src="../plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="../assets/pages/jquery.sweet-alert.init.js"></script>


        <!-- Datatables-->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap.min.js"></script>

        <!-- Datatable init js -->
        <script src="../assets/pages/datatables.init.js"></script>

        <!--tokeninput-->
        <script type="text/javascript" src="../plugins/tokeninput/src/jquery.tokeninput.js"></script>
        <link rel="stylesheet" type="text/css" href="../plugins/tokeninput/styles/token-input.css" />
        <link rel="stylesheet" type="text/css" href="../plugins/tokeninput/styles/token-input-facebook.css" />



        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>






        <script type="text/javascript">



             function loadTokenAutocomplete(idUser) {
               
                $.ajax({
                    url: "<?php echo site_url('admon_users/getListWorksites_specific') ?>/" + idUser,
                    dataType: 'json',
                    success: function (data) {
                    

                        $('#worksiteLists').tokenInput(
                                "admon_users/getListWorksites",
                                {
                                   // zindex: 6000,
                                    prePopulate: data,
                                    theme: 'facebook',
                                    preventDuplicates: true,
                                    allowFreeTagging: true
                                }
                        );
                    }
                });
            }

    
              $(document).ready(function() {         
                  
                // DataTable
                var table = $('#example').DataTable({
                                "processing": true,
                                "serverSide": true,
                                 "pageLength": 10,
                               // "lengthChange": false,
                                "ajax": {
                                    "url": "<?php echo site_url('admon_users/datatables_ajax');?>",
                                    "type": "POST"
                                }
                            });

                //  TableManageButtons.init();
             
                // Apply the search
                table.columns().every( function () {
                    var that = this;
             
                    $( 'input', this.footer() ).on( 'keyup change', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
            } );


           function loadEditParticipant(idUser){

                   $('#displayListW').hide();

               $.ajax({
                type: "POST",
                url: "<?php echo site_url('admon_users/getElement') ?>/" + idUser,
                contentType: "application/json;charset=utf-8",
                dataType: 'json',
                success: function (data) {
     
              
                        var cadProfile = "";
                        var selectProfile = "";




                        cadProfile = "<select id='selectProfile' class='form-control' onChange='updateSelectProfile()' >";

                        $.each(data.catalogProfiles, function (i, catalogProfiles) {

                            selectProfile = "";
                            if (data.profile == catalogProfiles.idProfiles) {
                                selectProfile = "selected";
                            }

                            cadProfile += "<option " + selectProfile + " value='"+ catalogProfiles.idProfiles +"' >" + catalogProfiles.type + "</option>";


                        });
                        cadProfile += "</select>";

                        $('#selectProfile').html(cadProfile);

                        $('#idUser').val(data.idUser);
                        $('#matricula').val(data.matricula);
                        $('#matriculaTXT').val(data.matricula);
                        $('#idNombre').val(data.name);
                        $('#passwordTXT').val(data.password);
                        $('#worksiteTXT').val(data.worksite);
                         
                        $("#div_worksiteLists").html('');
                        if(2 == data.profile){

                             $('#displayListW').show();
                         $("#div_worksiteLists").html("<textarea id='worksiteLists' name='worksiteLists' class='example' rows='1' style='width: 650px;'></textarea>");

                            //loadingTokenAutocomplete
                            loadTokenAutocomplete(idUser);
                        }




                        }
                    });

           }





            function saveElement() {

            
                var complexSelected;
                var areaSelected;
                var locationSelected;
                var typeSelected;
                var categorySelected;
                var capacitorSelected;

 
                profileSelected = $('#selectProfile option:selected').val();
                
              


                $("#profiSelect").val(profileSelected);
 


             


                $.ajax({
                    url: "<?php echo site_url('admon_users/saveElement') ?>",
                    type: "POST",
                    data: $('#form_course').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                        if (status == true)
                        {
                            $('#noAsistentes').css('background', 'white');
                            $('#assistantsList').css('background', 'white');
                            $('#nameCourseTXT').css('background', 'white');
                            $('#trainingEndTXT').css('background', 'white');
                            $('#trainingStartTXT').css('background', 'white');

                            //if success close modal and reload ajax table
                            $('#modalNew').modal('hide');

                            swal({
                                title: "Bien hecho",
                                text: "Los datos del perfil, ha sido guardado!",
                                type: "success"
                            },
                                 
                                    );
                        } else {
                            //$('#modalNew').modal('hide');
                            //reload_table();

                            $('#noAsistentes').css('background', 'yellow');
                            $('#assistantsList').css('background', 'yellow');

                            $('#noAsistentes').focus();



                            swal(
                                    'Oops...',
                                    message,
                                    'error'
                                    )
                        }
                    }
                });

            }


            function updateSelectProfile(){
              profileSel =  $('#selectProfile option:selected').val();
              if(profileSel!=2){
                 $('#displayListW').hide();
              }else{
                $('#displayListW').show();
              }
            }
 
              

        </script>




    </body>
</html>
