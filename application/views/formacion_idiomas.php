<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

        <title>Universidad Pilgrims | Formación | Idiomas</title>

        <!-- DataTables -->
        <link href="../plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- Sweet Alert css -->
        <link href="../plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />


        <link href="../plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/menu.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css">

        <link href="../assets/css/pilgrims.css" rel="stylesheet" type="text/css">

        <script src="../assets/js/modernizr.min.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                 <?php $this->view('subviewLogo.php'); ?>
              

                <!-- Navbar -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->view('subviewLeft.php'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <ol class="breadcrumb pull-right">
                                        <li><a href="#">Formación</a></li>
                                        <li class="active">Idiomas</li>
                                    </ol>
                                    <h4 class="page-title">Bienvenida(o)</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal" data-target=".modal-nuevo" onClick='loadNewCourse()'><i class="fa fa-plus-square"></i> Inscribir</button>
                               <!-- <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal" data-target=".modal-nuevoanexo"><i class="fa fa-file"></i> Subir Anexo</button>-->
                                <div class="panel panel-color panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Lista de Idiomas</h3>
                                        <p class="panel-sub-title font-13 text-muted">La siguiente es una lista de Idiomas que se han impartido.</p>
                                    </div>
                                    <div class="panel-body">
                                        <table id="datatable-buttons" class="table table-striped table-bordered ">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Nombre</th>
                                                    <th>Complejo</th>
                                                    <th>Area</th>
                                                    <th>Sub-area</th>
                                                    <th>Localidad</th>
                                                    <th>Tipo</th>
                                                    <th>Categoría</th>
                                                    <th>Modalidad</th>
                                                    <th>Inicio</th>
                                                    <th>Fin</th>
                                                    <th>No Asist.</th>
                                                    <th>No Mins</th>
                                                    <th>Instructor</th>
                                                    <th>Adjunto</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>

                                            <?php
                                            $this->view('subview_tables_course.php');
                                            ?>

                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- end row -->

                    </div>
                    <!-- end container -->
                </div>
                <!-- end content -->

                <footer class="footer text-right">
                    2017 © Pilgrim
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <!--  Modal nuevo-->
        <div id="modalNew" class="modal fade modal-nuevo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <div id="subTitle"><h4 class="modal-title">Detalles</h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form  action="#" id="form_course" class="form-horizontal"   >


                                <div class="form-group">
                                    <label class="col-md-2 control-label">Nombre del curso</label>
                                    <div class="col-md-10">
                                        <input id="nameCourseTXT" name="nameCourseTXT" type="text" class="form-control"  >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Complejo</label>
                                    <div id="selectComplex" name="selectComplex" class="col-sm-10">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Area</label>
                                    <div id="selectArea" name="selectArea" class="col-sm-10">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sub-Area</label>
                                    <div class="col-sm-10">
                                        <select class="form-control">
                                            <option>Opcion 1</option>
                                            <option>Opcion 2</option>
                                            <option>Opcion 3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Localidad</label>
                                    <div id="selectLocation" name="selectLocation" class="col-sm-10">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo</label>
                                    <div  id="selectType" name="selectType" class="col-sm-10">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Categoría</label>
                                    <div id="selectCategory" name="selectCategory" class="col-sm-10">

                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Modalidad</label>
                                    <div  id="selectModality" name="selectModality" class="col-sm-10">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Fecha de Inicio</label>
                                    <div class="col-md-10">
                                        <input id="trainingStartTXT" name="trainingStartTXT" type="text"  placeholder="dd/mm/YYYY" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Fecha de Fin</label>
                                    <div class="col-md-10">
                                        <input id="trainingEndTXT" name="trainingEndTXT" type="text" placeholder="dd/mm/YYYY" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Duración en minutos</label>
                                    <div class="col-md-10">
                                        <input id="durationCourseTXT" name="durationCourseTXT" type="text" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">No. de asistentes</label>
                                    <div class="col-md-10">
                                        <input type="text" name="noAsistentesTXT" id="noAsistentes" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-group">                                    
                                    <label class="col-sm-2 control-label">Capacitador</label>
                                    <div id="selectInstructor" name="selectInstructor" class="col-sm-10"></div>
                                </div>
                                <!--ASISTENTES-->
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Nombre del grupo</label>
                                    <div class="col-md-10">
                                        <input id="teamNameTXT" name="teamNameTXT" type="text" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Lista de asistentes</label>
                                    <div class="col-md-10">
                                        <div id="div_assistantsList"></div>
                                    </div>
                                </div>
                                <!--ANEXOS-->


                                <input type="hidden" name="complexSelect" id="complexSelect">
                                <input type="hidden" name="areaSelect" id="areaSelect">
                                <input type="hidden" name="locationSelect" id="locationSelect">
                                <input type="hidden" name="capacitorSelect" id="capacitorSelect">
                                <input type="hidden" name="typeSelect" id="typeSelect">
                                <input type="hidden" name="categorySelect" id="categorySelect">
                                <input type="hidden" name="modalitySelect" id="modalitySelect">
                                <input type="hidden" name="IDCourseTraining" id="IDCourseTraining">
                                <input type="hidden" name="idAtachmentTMP2" id="idAtachmentTMP2">
                                <input type="hidden" name="qtyTrainingTMP2" id="qtyTrainingTMP2">
                                 
                              
                               
                                <input type="hidden" name="methodForm" id="methodForm">
                                <input id="codeCourseTXT"  name="codeCourseTXT"  type="hidden">
                            </form>
                            <div id="atachment">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Lista de anexos</label>
                                    <div class="col-md-10">
                                        <p id="msg"></p>
                                        <o id="listFiles"></p>
                                    </div>


                                </div>
                                <div class="form-group">

                                    <div class="col-md-10">

                                        <input type="file" id="multiFiles" name="files[]" multiple="multiple" style="display: none;"/>          

                                        <input type="button" value="Seleccionar Archivo..." onclick="document.getElementById('multiFiles').click();" />
                                        <button class="col-md-2 control-label .btn-info" id="upload">Anexar</button>    

                                    </div>
                                </div>
                            </div>

                            <div align="right">
                                <button type="button"  onClick="saveCourse()" class="btn btn-success waves-effect waves-light">Guardar</button>
                            </div>

                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!--  Modal nuevo anexo-->
        <div class="modal fade modal-nuevoanexo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">Nuevo Anexo</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label">Seleccione el curso al que se agregará el anexo</label>
                                    <div class="col-sm-6">
                                        <select class="form-control">
                                            <option>Nombre del curso 2</option>
                                            <option>Nombre del curso 1</option>
                                            <option>Nombre del curso 3</option>
                                            <option>Nombre del curso 4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Seleccione Anexo</label>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-success waves-effect waves-light">Examinar</button>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <button type="button" class="btn btn-success waves-effect waves-light">Subir</button>
                            </form>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>




        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/wow.min.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../plugins/switchery/switchery.min.js"></script>

        <!-- Sweet Alert js -->
        <script src="../plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="../assets/pages/jquery.sweet-alert.init.js"></script>


        <!-- Datatables-->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap.min.js"></script>

        <!-- Datatable init js -->
        <script src="../assets/pages/datatables.init.js"></script>

        <!--tokeninput-->
        <script type="text/javascript" src="../plugins/tokeninput/src/jquery.tokeninput.js"></script>
        <link rel="stylesheet" type="text/css" href="../plugins/tokeninput/styles/token-input.css" />
        <link rel="stylesheet" type="text/css" href="../plugins/tokeninput/styles/token-input-facebook.css" />



        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>






        <script type="text/javascript">

            var table;

            //upload file
            $(document).ready(function (e) {
                $('#upload').on('click', function () {

                      $('#msg').html('');

                    var form_data = new FormData();
                    var ins = document.getElementById('multiFiles').files.length;

                    form_data.set("files", "");
                    form_data.set("IDCourseTraining", "");
                    form_data.set("idAtachmentTMP", "");
                    form_data.set("qtyTrainingTMP", "");

                    for (var x = 0; x < ins; x++) {
                        form_data.append("files[]", document.getElementById('multiFiles').files[x]);
                        form_data.append('IDCourseTraining', $("#IDCourseTraining").val());
                        form_data.append('idAtachmentTMP', $("#idTrainingTMP").val());
                        form_data.append('qtyTrainingTMP', $("#qtyAtachment").val());
                    }

             
                    $.ajax({
                        url: "<?php echo site_url('formacion_idiomas/upload_files') ?>",
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
                              $('#msg').html('');
                            $('#listFiles').html(response); // display success response from the server
                           
                            $('#qtyTrainingTMP').val($("#idTrainingTMP").val());
                            $('#qtyAtachment').val($("#qtyAtachment").val());
                             $("#idAtachmentTMP2").val($("#idTrainingTMP").val());
                             $("#qtyTrainingTMP2").val($("#qtyAtachment").val());

                             
                        },
                        error: function (response) {
                              $('#msg').html('');
                            $('#listFiles').html(response); // display error response from the server
                        }
                    });
                });
            });




            //$(document).ready(function(){

            function loadTokenAutocomplete(IDCourseTraining) {
                //var id = $('#dbId').val();
                // Get all the tags for this photo
                $.ajax({
                    url: "<?php echo site_url('formacion_idiomas/getListParticpants_specific') ?>/" + IDCourseTraining,
                    dataType: 'json',
                    success: function (data) {


                        $('#assistantsList').tokenInput(
                                "formacion_idiomas/getListParticpants",
                                {
                                    zindex: 6000,
                                    prePopulate: data,
                                    theme: 'facebook',
                                    preventDuplicates: true,
                                    allowFreeTagging: true
                                }
                        );
                    }
                });
            }
            //  });






             $(document).ready(function () {
                $('#datatable').DataTable();
            });
            TableManageButtons.init();


 





            function deleteCourse(idCourse) {
                swal({
                    title: "Estas seguro de querer borrar?",
                    text: "(No sera posible revertir el borrado).",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, estoy seguro!',
                    cancelButtonText: "No, cancelar!.",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                        function (isConfirm) {

                            if (isConfirm) {

                                $.ajax({
                                    url: "<?php echo site_url('formacion_idiomas/ajax_deleteCourse') ?>/" + idCourse,
                                    type: "POST",
                                    data: $('#form_course').serialize(),
                                    dataType: "JSON",
                                    success: function (data)
                                    {
                                        var status, message;
                                        $.each(data, function (index, obj) {
                                            status = obj.status;
                                            message = obj.msg;
                                        });

                                        if (status == true)
                                        {

                                            swal({
                                                title: "Eliminado!",
                                                text: "El curso de idiomas se eliminó.",
                                                type: "success"
                                            },
                                                    function () {
                                                        reload_table();
                                                    });
                                        } else {


                                            swal(
                                                    'Oops...',
                                                    'Error al eliminar. Consultar con Soporte Tecnico',
                                                    'error'
                                                    )
                                        }
                                    }
                                });




                            } else {
                                swal("Cancelado", "El curso no fué eliminado.", "error");
                            }
                        });

            }



            function loadNewCourse(idCourse) {


                var searchCurso = "";
                var htmlCurso = "";

                $("#form_course")[0].reset();
                $('#noAsistentes').css('background', 'white');
                $('#assistantsList').css('background', 'white');
                $('#nameCourseTXT').css('background', 'white');
                $('#trainingEndTXT').css('background', 'white');
                $('#trainingStartTXT').css('background', 'white');

                $("#methodForm").val("newForm");
                //$("#atachment").hide();
                $("#subTitle").html('<h4 class="modal-title">Alta Nuevo Curso</h4>');

                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('formacion_idiomas/getInfo_CourseTraining') ?>/",
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {

                        //id del Curso
                        //$("#codeCourseTXT").val(data.idCurso);

                        //RelacionCurso
                        $('#IDCourseTraining').val(data.IDCourseTraining);

                        //inicializando
                        $("#div_assistantsList").html("<textarea id='assistantsList' name='assistantsList' class='example' rows='1' style='width: 650px;'></textarea>");

                        idCourse = "none";

                        //loadingTokenAutocomplete
                        loadTokenAutocomplete(idCourse);

                        //Nombre del curso
                        $("#nameCourseTXT").val(data.listCourses.courseName);


                        //Complejo
                        var cadComplex = "";
                        var selectComplex = "";

                        cadComplex = "<select id='selectComplex' class='form-control'>";

                        $.each(data.catalogComplexes, function (i, catalogComplexes) {

                            selectComplex = "";
                            if (data.listCourses.complejo == catalogComplexes.name) {
                                selectComplex = "selected";
                            }

                            cadComplex += "<option " + selectComplex + " >" + catalogComplexes.name + "</option>";


                        });
                        cadComplex += "</select>";

                        $('#selectComplex').html(cadComplex);



                        //Area
                        var cadArea = "";
                        var selectArea = "";

                        cadArea = "<select id='selectArea' class='form-control'>";

                        $.each(data.catalogAreas, function (i, catalogAreas) {


                            cadArea += "<option " + selectArea + " >" + catalogAreas.name + "</option>";


                        });
                        cadArea += "</select>";

                        $('#selectArea').html(cadArea);







                        //Localidad
                        var cadLocation = "";
                        var selectLocation = "";

                        cadLocation = "<select id='selectLocation' class='form-control'>";

                        $.each(data.catalogLocations, function (i, catalogLocations) {


                            cadLocation += "<option " + selectLocation + " >" + catalogLocations.name + "</option>";


                        });
                        cadLocation += "</select>";

                        $('#selectLocation').html(cadLocation);




                        //Tipo
                        var cadTipo = "";
                        var selectTipo = "";
                        var strTipo = data.listCourses.Tipo;


                        cadTipo = "<select id='selectType' class='form-control'>";
                        cadTipo += "     <option>Externo</option>";
                        cadTipo += "     <option>Interno</option>";
                        cadTipo += "</select>";

                        $('#selectType').html(cadTipo);





                        //Categoria
                        var cadCategory = "";
                        var selectCategory = "";

                        cadCategory = "<select id='selectCategory' class='form-control'>";

                        $.each(data.catalogCategories, function (i, catalogCategories) {

                            cadCategory += "<option " + selectCategory + " >" + catalogCategories.name + "</option>";

                        });
                        cadCategory += "</select>";

                        $('#selectCategory').html(cadCategory);



                        //Instructor
                        var cadInstructor = "";
                        var selectInstructor = "";

                        cadInstructor = "<select id='selectInstructor' class='form-control'>";

                        $.each(data.catalogInstructors, function (i, catalogInstructors) {

                            selectInstructor = "";


                            cadInstructor += "<option " + selectInstructor + " >" + catalogInstructors.fk_instructor + "</option>";


                        });
                        cadInstructor += "</select>";

                        $('#selectInstructor').html(cadInstructor);







                        //Duracion
                        $("#durationCourseTXT").val(data.listCourses.NoHrs);


                        //Modalidad

                        var cadTipo = "";
                        var selectModalidad = "";
                        var strModalidad = data.listCourses.Modalidad;


                        cadModalidad = "<select id='selectModality' class='form-control'>";
                        cadModalidad += "     <option>Presencial</option>";
                        cadModalidad += "     <option>Virtual</option>";
                        cadModalidad += "     <option>Mixto</option>";
                        cadModalidad += "</select>";

                        $('#selectModality').html(cadModalidad);


                        //Fecha Inicio del Curso
                        $("#trainingStartTXT").val(data.trainingStart);

                        //Fecha final del Curso
                        $("#trainingEndTXT").val(data.trainingEnd);


                        //Nombre del Equipo
                        $("#teamNameTXT").val(data.listCourses.teamName);





                    }
                });
            }


            function deleteAtachment(idAtachment, idCourse){
                //alert("Eleiminado:" +idElement);

                      swal({
                    title: "Estas seguro de querer borrar el archivo?",
                    text: "(No sera posible revertir el borrado).",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, estoy seguro!',
                    cancelButtonText: "No, cancelar!.",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, 
                       function (isConfirm) {

                            if (isConfirm) {

                                $.ajax({
                                    url: "<?php echo site_url('formacion_idiomas/ajax_deleteAtachment') ?>/" + idAtachment,
                                    type: "POST",
                                    data: $('#form_course').serialize(),
                                    dataType: "JSON",
                                    success: function (data)
                                    {
                                        var status, message;
                                        $.each(data, function (index, obj) {
                                            status = obj.status;
                                            message = obj.msg;
                                        });

                                        if (status == true)
                                        {

                                            swal({
                                                title: "Eliminado!",
                                                text: "El adjunto se eliminó.",
                                                type: "success"
                                            },
                                                    function () {
                                                        quitAtachment(message);
                                                             var qtyAtach = $("#qtyAtachment").val();
                                                             qtyAtach--;
                                                             $("#qtyAtachment").val(qtyAtach);

                                                    });
                                        } else {


                                            swal(
                                                    'Oops...',
                                                    'Error al eliminar. Consultar con Soporte Tecnico',
                                                    'error'
                                                    )
                                        }
                                    }
                                });




                            } else {
                                swal("Cancelado", "El archivo no fué eliminado.", "error");

                            }
                        });
               
            }

            function quitAtachment(idAtachment){
                var elementQuit = "atachment_"+idAtachment;                 
                $("#"+elementQuit).remove();

            }


            function loadEditCourse(idCourse) {


                var searchCurso = "";
                var htmlCurso = "";

                $("#subTitle").html('<h4 class="modal-title">Detalles</h4>');

                $("#methodForm").val("editForm");

                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('formacion_idiomas/getInfo_CourseTraining') ?>/" + idCourse,
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {

                            $('#noAsistentes').css('background', 'white');
                            $('#assistantsList').css('background', 'white');
                            $('#nameCourseTXT').css('background', 'white');
                            $('#trainingEndTXT').css('background', 'white');
                            $('#trainingStartTXT').css('background', 'white');
                             $('#msg').html('');

                        //id del Curso
                        $("#codeCourseTXT").val(data.idCurso);

                        //RelacionCurso
                        $('#IDCourseTraining').val(data.IDCourseTraining);

                        //inicializando
                        $("#div_assistantsList").html("<textarea id='assistantsList' name='assistantsList' class='example' rows='1' style='width: 650px;'></textarea>");



                        //loadingTokenAutocomplete
                        loadTokenAutocomplete(idCourse);

                        //Nombre del curso
                        $("#nameCourseTXT").val(data.listCourses.courseName);


                        //Complejo
                        var cadComplex = "";
                        var selectComplex = "";

                        cadComplex = "<select id='selectComplex' class='form-control'>";

                        $.each(data.catalogComplexes, function (i, catalogComplexes) {

                            selectComplex = "";
                            if (data.listCourses.complejo == catalogComplexes.name) {
                                selectComplex = "selected";
                            }

                            cadComplex += "<option " + selectComplex + " >" + catalogComplexes.name + "</option>";


                        });
                        cadComplex += "</select>";

                        $('#selectComplex').html(cadComplex);



                        //Area
                        var cadArea = "";
                        var selectArea = "";

                        cadArea = "<select id='selectArea' class='form-control'>";

                        $.each(data.catalogAreas, function (i, catalogAreas) {

                            selectArea = "";
                            if (data.listCourses.Area == catalogAreas.name) {
                                selectArea = "selected";
                            }

                            cadArea += "<option " + selectArea + " >" + catalogAreas.name + "</option>";


                        });
                        cadArea += "</select>";

                        $('#selectArea').html(cadArea);





                        //Localidad
                        var cadLocation = "";
                        var selectLocation = "";

                        cadLocation = "<select id='selectLocation' class='form-control'>";

                        $.each(data.catalogLocations, function (i, catalogLocations) {

                            selectLocation = "";
                            if (data.listCourses.Localidad == catalogLocations.name) {
                                selectLocation = "selected";
                            }

                            cadLocation += "<option " + selectLocation + " >" + catalogLocations.name + "</option>";


                        });
                        cadLocation += "</select>";

                        $('#selectLocation').html(cadLocation);




                        //Tipo
                        var cadTipo = "";
                        var selectTipo = "";
                        var strTipo = data.listCourses.Tipo;


                        cadTipo = "<select id='selectType' class='form-control'>";
                        if (strTipo.toLowerCase() == "externo")
                            cadTipo += "     <option selected>Externo</option>";
                        else
                            cadTipo += "     <option>Externo</option>";
                        if (strTipo.toLowerCase() == "interno")
                            cadTipo += "     <option selected>Interno</option>";
                        else
                            cadTipo += "     <option>Interno</option>";
                        cadTipo += "</select>";

                        $('#selectType').html(cadTipo);


                        //npAsistentes
                        $("#noAsistentes").val(data.listCourses.NoAsistentes);



                        //Categoria
                        var cadCategory = "";
                        var selectCategory = "";

                        cadCategory = "<select id='selectCategory' class='form-control'>";

                        $.each(data.catalogCategories, function (i, catalogCategories) {

                            selectCategory = "";
                            if (data.listCourses.Categoria == catalogCategories.name) {
                                selectCategory = "selected";
                            }

                            cadCategory += "<option " + selectCategory + " >" + catalogCategories.name + "</option>";


                        });
                        cadCategory += "</select>";

                        $('#selectCategory').html(cadCategory);



                        //Instructor
                        var cadInstructor = "";
                        var selectInstructor = "";

                        cadInstructor = "<select id='selectInstructor' class='form-control'>";

                        $.each(data.catalogInstructors, function (i, catalogInstructors) {

                            selectInstructor = "";
                            if (data.listCourses.capacitador == catalogInstructors.fk_instructor) {
                                selectInstructor = "selected";
                            }

                            cadInstructor += "<option " + selectInstructor + " >" + catalogInstructors.fk_instructor + "</option>";


                        });
                        cadInstructor += "</select>";

                        $('#selectInstructor').html(cadInstructor);



                        //Get ListFiles
                        var cadFiles = "";
                        var base = "<?php echo base_url() ?>";
                        var toFile = "";

                        var qtyAtachment = 0;


                        $.each(data.atachmentFiles, function (i, atachmentFiles) {
                            toFile = atachmentFiles.url + "/" + atachmentFiles.name;
                            cadFiles += "<div class='atachment' id='atachment_" + atachmentFiles.id + "'>";
                            cadFiles += atachmentFiles.reformat + "&nbsp;<a href='" + base + toFile + "'>" + atachmentFiles.name + "</a>&nbsp;<a href='#' onClick='deleteAtachment("+   atachmentFiles.id+","+idCourse+")' border='0'><img width='14px' height='14px' src='../assets/images/delete-icon.png'></a></div>";

                            qtyAtachment++;

                        });

                        cadFiles += "<input type='hidden' name='qtyAtachment' value='" + qtyAtachment + "'>";

                   






                        //alert("cantidad:" +  qtyAtachment);
                        $("#qtyAtachment").val(qtyAtachment);
                        $("#qtyTrainingTMP").val(qtyAtachment);


                        $('#listFiles').html(cadFiles);




                        //Duracion
                        $("#durationCourseTXT").val(data.listCourses.NoHrs);


                        //Modalidad
                        var modalidad = data.listCourses.Modalidad;

                        var cadTipo = "";
                        var selectModalidad = "";
                        var strModalidad = data.listCourses.Modalidad;



                        cadModalidad = "<select id='selectModality' class='form-control'>";
                        cadModalidad += "     <option>Presencial</option>";
                        cadModalidad += "     <option>Virtual</option>";
                        cadModalidad += "     <option>Mixto</option>";
                        cadModalidad += "</select>";

                        cadModalidad = "<select id='selectModality' class='form-control'>";
                        if (strModalidad.toLowerCase() == "presencial")
                            cadModalidad += "     <option selected>Presencial</option>";
                        else
                            cadModalidad += "     <option>Presencial</option>";
                        if (strModalidad.toLowerCase() == "virtual")
                            cadModalidad += "     <option selected>Virtual</option>";
                        else
                            cadModalidad += "     <option>Virtual</option>";
                         if (strModalidad.toLowerCase() == "mixto")
                            cadModalidad += "     <option selected>Mixto</option>";
                        else
                            cadModalidad += "     <option>Mixto</option>";
                        cadModalidad += "</select>";

                        $('#selectModality').html(cadModalidad);


                        //Fecha Inicio del Curso
                        $("#trainingStartTXT").val(data.trainingStart);

                        //Fecha final del Curso
                        $("#trainingEndTXT").val(data.trainingEnd);


                        //Nombre del Equipo
                        $("#teamNameTXT").val(data.listCourses.teamName);





                    }
                });
            }



            function reload_table()
            {
                //table.ajax.reload(null, false); //reload datatable ajax
                location.reload();

            }


            function validateDate(date) {
                var reg = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
                //var re = /^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
                return reg.test(date);
            }


            function validateCampos(){
                    //Validando que se asigne al menos un Titulo.
                if($("#nameCourseTXT").val() == ""){
                      $('#nameCourseTXT').css('background', 'yellow');
                       $('#nameCourseTXT').focus();
                      message = 'No puede quedar vacio el Titulo, favor de poner un Titulo.';

                       swal(
                                    'Oops...',
                                    message,
                                    'error'
                                    )
                       return false;
                }

                //Validando las Fechas
                //Fecha Inicial
               if($("#trainingStartTXT").val()!=""){
                    if(!validateDate($("#trainingStartTXT").val())){
                         $('#trainingStartTXT').css('background', 'yellow');                         
                           $('#trainingStartTXT').focus();

                          message = 'Fecha Inicial invalida, el formato de fecha debe ser dd/mm/YYYY.';

                           swal(
                                        'Oops...',
                                        message,
                                        'error'
                                        )
                           return false;

                    }
                }

                //Fecha Final
               if($("#trainingEndTXT").val()!=""){
                    if(!validateDate($("#trainingEndTXT").val())){
                         $('#trainingEndTXT').css('background', 'yellow');
                         $('#trainingEndTXT').focus();

                          message = 'Fecha Final invalida, el formato de fecha debe ser dd/mm/YYYY.';

                           swal(
                                        'Oops...',
                                        message,
                                        'error'
                                        )
                           return false;

                    }
                }

            }


            function saveCourse() { 



            
                var complexSelected;
                var areaSelected;
                var locationSelected;
                var typeSelected;
                var categorySelected;
                var capacitorSelected;


 

                complexSelected = $("#selectComplex option:selected").text();
                areaSelected = $("#selectArea option:selected").text();
                locationSelected = $("#selectLocation option:selected").text();
                typeSelected = $("#selectType option:selected").text();
                categorySelected = $("#selectCategory option:selected").text();
                modalitySelected = $("#selectModality option:selected").text();
                capacitorSelected = $("#selectInstructor option:selected").text();



                $("#complexSelect").val(complexSelected);
                $("#areaSelect").val(areaSelected);
                $("#locationSelect").val(locationSelected);
                $("#typeSelect").val(typeSelected);
                $("#categorySelect").val(categorySelected);
                $("#modalitySelect").val(modalitySelected);
                $("#capacitorSelect").val(capacitorSelected);
               

                if(validateCampos()==false){
                    return;
                }

               //Validando cantidad documentos

               var countAtachm = 0;
                $('.atachment').each(function() {
                    countAtachm++;
                }); 
                
                    if(countAtachm == 0){
                         message = 'Debe existir al menos un anexo (documento que respalde el curso).';

                                swal(
                                    'Oops...',
                                    message,
                                    'error'
                                    )
                               return false;

                    }
            //}



               



                $.ajax({
                    url: "<?php echo site_url('formacion_idiomas/ajax_saveCourse') ?>",
                    type: "POST",
                    data: $('#form_course').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                        if (status == true)
                        {
                            $('#noAsistentes').css('background', 'white');
                            $('#assistantsList').css('background', 'white');
                            $('#nameCourseTXT').css('background', 'white');
                            $('#trainingEndTXT').css('background', 'white');
                            $('#trainingStartTXT').css('background', 'white');

                            //if success close modal and reload ajax table
                            $('#modalNew').modal('hide');

                            swal({
                                title: "Bien hecho",
                                text: "El curso de idiomas ha sido guardado!",
                                type: "success"
                            },
                                    function () {
                                        reload_table();
                                    });
                        } else {
                            //$('#modalNew').modal('hide');
                            //reload_table();

                            $('#noAsistentes').css('background', 'yellow');
                            $('#assistantsList').css('background', 'yellow');

                            $('#noAsistentes').focus();



                            swal(
                                    'Oops...',
                                    message,
                                    'error'
                                    )
                        }
                    }
                });

            }




        </script>




    </body>
</html>
