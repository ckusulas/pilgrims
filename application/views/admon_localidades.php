<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

        <title>Universidad Pilgrims | Administracion | <?=$subTitle?></title>

        <!-- DataTables -->
        <link href="../plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- Sweet Alert css -->
        <link href="../plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />


        <link href="../plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/pages.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/menu.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css">

        <link href="../assets/css/pilgrims.css" rel="stylesheet" type="text/css">

        <script src="../assets/js/modernizr.min.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->


    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                 <?php $this->view('subviewLogo.php'); ?>
              

                <!-- Navbar -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->view('subviewLeft.php'); ?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <ol class="breadcrumb pull-right">
                                        <li><a href="#">Administración</a></li>
                                        <li class="active"><?=$subTitle?></li>
                                    </ol>
                                    <h4 class="page-title">Bienvenida(o)</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-15" data-toggle="modal" data-target=".modal-nuevo" onClick='loadNewElement()'><i class="fa fa-plus-square"></i> Nuevo <?=$subElementSingular?></button>
                              
                                <div class="panel panel-color panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Lista de <?=$subTitle?></h3>
                                        <p class="panel-sub-title font-13 text-muted">Administración de <?=$subTitle?></p>
                                    </div>
                                    <div class="panel-body">
                                        <table id="datatable-buttons" width="45%" class="table table-striped table-bordered ">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th><?=$subTitle?></th>                                                    
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>

                                            <?php
                                            $this->view('subview_admon_tables.php');
                                            ?>

                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- end row -->

                    </div>
                    <!-- end container -->
                </div>
                <!-- end content -->

                <footer class="footer text-right">
                    2017 © Pilgrim
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <!--  Modal nuevo-->
        <div id="modalNew" class="modal fade modal-nuevo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">Detalles</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form  action="#" id="form_course" class="form-horizontal"   >


                                <div class="form-group" id="displayId">
                                    <label class="col-md-2 control-label">Id</label>
                                    <div class="col-md-10">
                                        <input id="id<?=$subElementSingular?>" name="id<?=$subElementSingular?>" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nombre <?=$subElementSingular?>:</label>
                                   <div class="col-md-10">
                                        <input id="name<?=$subElementSingular?>TXT" name="name<?=$subElementSingular?>TXT" type="text" class="form-control"  >
                                    </div>
                                </div>
                                <input type="hidden" id="idElement" name="idElement">
                                <input type="hidden" id="methodForm" name="methodForm">
                               
                            </form>
                           
                            <div align="right">
                                <button type="button"  onClick="saveElement()" class="btn btn-success waves-effect waves-light">Guardar</button>
                            </div>

                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
   



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>




        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/detect.js"></script>
        <script src="../assets/js/fastclick.js"></script>
        <script src="../assets/js/jquery.slimscroll.js"></script>
        <script src="../assets/js/jquery.blockUI.js"></script>
        <script src="../assets/js/waves.js"></script>
        <script src="../assets/js/wow.min.js"></script>
        <script src="../assets/js/jquery.nicescroll.js"></script>
        <script src="../assets/js/jquery.scrollTo.min.js"></script>
        <script src="../plugins/switchery/switchery.min.js"></script>

        <!-- Sweet Alert js -->
        <script src="../plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="../assets/pages/jquery.sweet-alert.init.js"></script>


        <!-- Datatables-->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap.min.js"></script>

        <!-- Datatable init js -->
        <script src="../assets/pages/datatables.init.js"></script>

        <!--tokeninput-->
        <script type="text/javascript" src="../plugins/tokeninput/src/jquery.tokeninput.js"></script>
        <link rel="stylesheet" type="text/css" href="../plugins/tokeninput/styles/token-input.css" />
        <link rel="stylesheet" type="text/css" href="../plugins/tokeninput/styles/token-input-facebook.css" />



        <script src="../assets/js/jquery.core.js"></script>
        <script src="../assets/js/jquery.app.js"></script>






        <script type="text/javascript">

            var table;

  

            $(document).ready(function () {
                $('#datatable').DataTable();
            });
            TableManageButtons.init();








            function deleteElement(idElement) {
                swal({
                    title: "Estas seguro de querer borrar?",
                    text: "(No sera posible revertir el borrado).",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, estoy seguro!',
                    cancelButtonText: "No, cancelar!.",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                        function (isConfirm) {

                            if (isConfirm) {

                                $.ajax({

                                    url: "<?php echo site_url($name_controller.'ajax_deleteElement') ?>/" + idElement,
                                    type: "POST",
                                    //data: $('#form_course').serialize(),
                                    dataType: "JSON",
                                    success: function (data)
                                    {
                                        var status, message;
                                        $.each(data, function (index, obj) {
                                            status = obj.status;
                                            message = obj.msg;
                                        });

                                        if (status == true)
                                        {

                                            swal({
                                                title: "Eliminado!",
                                                text: "La localidad se eliminó.",
                                                type: "success"
                                            },
                                                    function () {
                                                        reload_table();
                                                    });
                                        } else {


                                            swal(
                                                    'Oops...',
                                                    'Error al eliminar. Consultar con Soporte Tecnico',
                                                    'error'
                                                    )
                                        }
                                    }
                                });




                            } else {
                                swal("Cancelado", "El elemento no fue eliminado.", "error");
                            }
                        });

            }



            function loadNewElement(idCourse) {


                $("#displayId").hide();
                $("#myLargeModalLabel").html("<h4>Alta</h4>")



                var searchCurso = "";
                var htmlCurso = "";

                $("#form_course")[0].reset();
                $('#name<?=$subElementSingular?>TXT').css('background', 'white');
                
                $("#methodForm").val("newForm");
               
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url($name_controller.'createElement') ?>/",
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
 




                    }
                });
            }


 


            function loadElement(idLocalidad){               
 

                var searchCurso = "";
                var htmlCurso = "";
                
                //url: nameController+"/getElement/" + id<?=$subElementSingular?>,
                 $("#methodForm").val("editForm");
                    
 
                $.ajax({
                    type: "POST",
                     url: "<?php echo site_url($name_controller.'getElement') ?>/" + idLocalidad,
                    
                    contentType: "application/json;charset=utf-8",
                    dataType: 'json',
                    success: function (data) {

                        $('#name<?=$subElementSingular?>TXT').css('background', 'white');
                            
                        //id complex
                        $("#id<?=$subElementSingular?>").val(idLocalidad);
                        $("#idElement").val(idLocalidad);
                       
                      
                        //name complex
                        $('#nameLocalidadTXT').val(data.name_location);
                
                         

        
                    }
                });
            }



            function reload_table()
            {
                //table.ajax.reload(null, false); //reload datatable ajax
                location.reload();

            }


            function validateDate(date) {
                var reg = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
                //var re = /^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
                return reg.test(date);
            }


            function validateCampos(){
            
                //Validando que se asigne al menos un nombre.
                if($("#name<?=$subElementSingular?>TXT").val() == ""){
                      $('#name<?=$subElementSingular?>TXT').css('background', 'yellow');
                       $('#name<?=$subElementSingular?>TXT').focus();
                      message = 'No puede quedar vacio el nombre de <?=$subElementSingular?>.';

                       swal(
                                    'Oops...',
                                    message,
                                    'error'
                                    )
                       return false;
                }

               

            }


            function saveElement() {
                

                var action;
                var nameController = "<?=$name_controller?>";


                if(validateCampos()==false){
                    return;
                }


                action = "saveElement";
                if($("#methodForm").val()=="newForm"){
                    action = "createElement";
                }

 
                $.ajax({                      
                    url: "<?php echo site_url($name_controller) ?>"+action,
                    type: "POST",
                    data: $('#form_course').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        var status, message;
                        $.each(data, function (index, obj) {
                            status = obj.status;
                            message = obj.msg;
                        });

                        if (status == true)
                        {
                            $('#noAsistentes').css('background', 'white');
                            $('#assistantsList').css('background', 'white');
                  

                            //if success close modal and reload ajax table
                            $('#modalNew').modal('hide');

                            swal({
                                title: "Bien hecho",
                                text: "El <?=$subElementSingular?> ha sido guardado!",
                                type: "success"
                            },
                                    function () {
                                        reload_table();
                                    });
                        } else {
                            //$('#modalNew').modal('hide');
                            //reload_table();
 



                            swal(
                                    'Oops...',
                                    message,
                                    'error'
                                    )
                        }
                    }
                });

            }




        </script>




    </body>
</html>
