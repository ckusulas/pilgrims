    <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <div id="sidebar-menu">
                        <ul>
                            <li class="menu-title">Menú</li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect waves-primary"><i class="fa fa-dashboard"></i> <span> Tableros </span>
                                 <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <?php 
                                     $profile = $this->session->userdata('profile');
                                     ?>

                                    <?php if($profile == 1 || $profile == 2){ ?>
                                        <li><a href="tablero_principal?srt=<?php echo $this->session->userdata('token') ?>">T. Principal</a></li>
                                        <li><a href="tablero_curso?srt=<?php echo $this->session->userdata('token') ?>">T. Curso</a></li>
                                    <?php
                                    }
                                    ?>
                                    <li><a href="tablero_personal?srt=<?php echo $this->session->userdata('token') ?>">T. Personal</a></li>
                                </ul>
                            </li>

                            <?php if($profile == 1 || $profile == 2) { ?>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect waves-primary"><i class="fa fa-book"></i>
                                    <span> Formación </span><span class="menu-arrow"></span></a></a>
                                <ul class="list-unstyled">
                                    <li><a href="formacion_curso?srt=<?php echo $this->session->userdata('token') ?>">Curso o Taller</a></li>
                                    <li><a href="formacion_diplomado?srt=<?php echo $this->session->userdata('token') ?>">Diplomado</a></li>
                                    <li><a href="formacion_postgrado?srt=<?php echo $this->session->userdata('token') ?>">Posgrado</a></li>
                                    <li><a href="formacion_idiomas?srt=<?php echo $this->session->userdata('token') ?>">Idiomas</a></li>
                                    <li><a href="admon_instructors?srt=<?php echo $this->session->userdata('token') ?>">Admon. Instructores</a></li>
                                </ul>
                            </li>
                            <?php
                                } 
                            ?>
  
                            <?php if($profile == 1 ) { ?>
                            

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect waves-primary"><i class="fa fa-gear"></i>
                                    <span> Administración </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="admon_complejos?srt=<?php echo $this->session->userdata('token') ?>">Complejos</a></li>
                                    <li><a href="admon_areas?srt=<?php echo $this->session->userdata('token') ?>">Areas</a></li>
                                    <li><a href="subareas.html">SubAreas</a></li>
                                    <li><a href="admon_localidades?srt=<?php echo $this->session->userdata('token') ?>">Localidades</a></li>
                                    <li><a href="admon_worksites?srt=<?php echo $this->session->userdata('token') ?>">Worksites</a></li>
                                    <li><a href="admon_users?srt=<?php echo $this->session->userdata('token') ?>">Usuarios del sistema</a></li>
                                    <li><a href="admon_personnelareas?srt=<?php echo $this->session->userdata('token') ?>">Personnel Areas</a></li>
                                    <li><a href="admon_organizationalunits?srt=<?php echo $this->session->userdata('token') ?>">Grupo Org</a></li>                  
                                    <li><a href="admon_import_data?srt=<?php echo $this->session->userdata('token') ?>">Importar datos</a></li> 
                                
                                </ul>
                            </li>
                            <?php 
                            }
                            ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="user-detail">
                    <div class="dropup">
                        <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
                            <img  src="../assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle">
                            <span class="user-info-span">
                                <h5 class="m-t-0 m-b-0"><?php echo $this->session->userdata('name') ?></h5>
                                <p class="text-muted m-b-0">
                                    <small><i class="fa fa-circle text-success"></i> <span>En línea</span></small>
                                </p>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!--<li><a href="javascript:void(0)"><i class="md md-face-unlock"></i> Profile</a></li>
                            <li><a href="javascript:void(0)"><i class="md md-settings"></i> Settings</a></li>
                            <li><a href="javascript:void(0)"><i class="md md-lock"></i> Lock screen</a></li>-->
                            <li><a href="javascript:logout()"><i class="md md-settings-power"></i> Logout</a></li>
                        </ul>

                    </div>
                </div>
            </div>



            <script language="javascript">
            function logout(){
               
         
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('login/logout') ?>/" ,
                        contentType: "application/json;charset=utf-8",
                        dataType: 'json',
                        success: function (data) {
                         
                            window.location.replace('../index.php');



                        }
                    });
                
            }

            </script>