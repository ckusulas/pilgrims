<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon_1.ico">

        <title>Universidad Pilgrims | Login Control de cursos</title>

        <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/pages.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/menu.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/responsive.css" rel="stylesheet" type="text/css">

        <script src="<?=base_url()?>/assets/js/modernizr.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->


    </head>
    <body>


        <div class="wrapper-page">

            <div class="text-center">
                <a href="#" class="logo-lg"><img src="<?=base_url()?>/assets/images/logo.png" style="width: 80%"></a>
            </div>

            <form class="form-horizontal m-t-20" action="<?=site_url()?>/login" method="post">

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="login" id="login" required="" placeholder="ID">
                        <i class="md md-account-circle form-control-feedback l-h-34"></i>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" id="password" required="" placeholder="Contraseña">
                        <i class="md md-vpn-key form-control-feedback l-h-34"></i>
                    </div>
                </div>

                <div class="form-group text-right m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-custom w-md waves-effect waves-light" type="submit">Entrar
                        </button>
                    </div>
                </div>
            </form>
        </div>


    	<script>
            var resizefunc = [];
        </script>

        <!-- Main  -->
        <script src="<?=base_url()?>/assets/js/jquery.min.js"></script>
        <script src="<?=base_url()?>/assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>/assets/js/detect.js"></script>
        <script src="<?=base_url()?>/assets/js/fastclick.js"></script>
        <script src="<?=base_url()?>/assets/js/jquery.slimscroll.js"></script>
        <script src="<?=base_url()?>/assets/js/jquery.blockUI.js"></script>
        <script src="<?=base_url()?>/assets/js/waves.js"></script>
        <script src="<?=base_url()?>/assets/js/wow.min.js"></script>
        <script src="<?=base_url()?>/assets/js/jquery.nicescroll.js"></script>
        <script src="<?=base_url()?>/assets/js/jquery.scrollTo.min.js"></script>

        <!-- Custom main Js -->
        <script src="<?=base_url()?>/assets/js/jquery.core.js"></script>
        <script src="<?=base_url()?>/assets/js/jquery.app.js"></script>

	</body>
</html>
